<?php

return [

    'name' => 'BeLead CMS',
    'version' => '3.6',
    'front_template' => 'default',

    'prefix_backend' => 'admin',

    'google2FA' => false,
    'google2FA_window' => 4,
    'google2FA_cookie_lifespan' => 60,

];
