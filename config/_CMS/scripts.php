<?php

return [
    'plugins/bower_components/jquery/dist/jquery.min.js',
    'bootstrap/dist/js/bootstrap.min.js',
    'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
    'js/jquery.slimscroll.js',
    'js/waves.js',
    'plugins/bower_components/owl.carousel/owl.carousel.min.js',
    'plugins/bower_components/owl.carousel/owl.custom.js',
    'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
    'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
    'plugins/bower_components/sweetalert/sweetalert.min.js',
    'plugins/bower_components/toast-master/js/jquery.toast.js',
    'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',
    'plugins/bower_components/dropzone-master/dist/dropzone.js',
    'plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
    'plugins/bower_components/morrisjs/morris.min.js',
    'plugins/bower_components/raphael/raphael-min.js',
    'plugins/bower_components/multiselect/js/jquery.multi-select.js',
    'js/widget.js',
    'js/custom.js',
    'js/cbpFWTabs.js',
];
