<?php

return [
    'css/animate.css',
    'bootstrap/dist/css/bootstrap.min.css',
    'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
    'plugins/bower_components/owl.carousel/owl.carousel.min.css',
    'plugins/bower_components/owl.carousel/owl.theme.default.css',
    'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',
    'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
    'plugins/bower_components/sweetalert/sweetalert.css',
    'plugins/bower_components/toast-master/css/jquery.toast.css',
    'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css',
    'plugins/bower_components/dropzone-master/dist/dropzone.css',
    'plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
    'plugins/bower_components/css-chart/css-chart.css',
    'plugins/bower_components/morrisjs/morris.css',
    'plugins/bower_components/multiselect/css/multi-select.css',
    'css/style.css',
    'css/colors/default.css'
];
