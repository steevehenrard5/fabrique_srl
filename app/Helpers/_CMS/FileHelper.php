<?php namespace App\Helpers\_CMS;

class FileHelper {

    public static function getFormattedSize($bytes) {
        if($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = 0;
        }

        return $bytes;
    }

    public static function getIcon($file_type){

        if(!empty($file_type)) {
            if (strpos($file_type,'.') !== false) {
                $arrayFile = explode('.', $file_type);
                $file_type = end($arrayFile);
            }
        }

        switch(strtolower($file_type)) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'gif':
                return 'file-image-o';
                break;
            case 'pdf':
                return 'file-pdf-o';
                break;
            case 'doc':
            case 'docx':
                return 'file-word-o';
                break;
            case 'xls':
            case 'xlsx':
                return 'file-excel-o';
                break;
            default:
                return 'file-o';
                break;
        }
    }

}