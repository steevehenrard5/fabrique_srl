<?php namespace App\Helpers\_CMS;

use App\Models\_CMS\Menu;

class MenuHelper {

    public static function build($reference, $view_variables, $li_class, $li_active_class) {
        $l_elements = '';

        if(!is_null($view_variables) && isset($view_variables['__data'])) {
            $vars = $view_variables['__data'];

            $found = false;
            if(isset($vars['global_menus'])) {
                foreach($vars['global_menus'] as $g_menu) {
                    if($g_menu->reference == $reference) {
                        $found = true;
                        $l_elements .= self::build_l_elements($g_menu, $vars, $li_class, $li_active_class);
                        break;
                    }
                }
            }

            if(!$found && isset($vars['page'])) {
                if($vars['page']->menus->count() > 0) {
                    foreach($vars['page']->menus as $p_menu) {
                        if($p_menu->menu->reference == $reference) {
                            $found = true;
                            $l_elements .= self::build_l_elements($p_menu->menu, $vars, $li_class, $li_active_class);
                            break;
                        }
                    }
                }
            }

            if(!$found) {
                $menu = self::findMenu($reference);
                if($menu) $l_elements .= self::build_l_elements($menu, $vars, $li_class, $li_active_class);
            }
        }

        return $l_elements;
    }

    private static function findMenu($reference) {
        return Menu::with('f_items.page.f_attributes', 'f_items.sub_menu.menu.f_items.page.f_attributes')
            ->where('reference', $reference)
            ->where('site_id', session('switched_site.id'))
            ->first();
    }

    private static function build_l_elements($menu, $vars, $li_class, $li_active_class) {
        $l_elements = '';

        foreach($menu->f_items as $item) {
            if(!$item->page && !$item->is_void && !$item->custom_url) continue;
            if($item->page && !$item->page->is_active) continue;
            if(!$item->is_void && $item->page && $item->page->is_guest && guard_user_check()) continue;
            if(!$item->is_void && $item->page && $item->page->is_secured && !guard_user_check()) continue;

            $active_class = ' ';
            if(isset($vars['attributes'])) {
                if($item->page) {
                    if($item->page->f_attributes->url == $vars['attributes']->url)
                        $active_class .= $li_active_class;
                } else {
                    if($item->isActiveDueToChildItem($vars['attributes']->url))
                        $active_class .= $li_active_class;
                }
            }

            $l_elements .= '<li class="' . $li_class . $active_class . '">';
                $link = href_none();
                if($item->page && !$item->is_void) {
                    $link = route('front.page', $item->page->f_attributes->url);
                } elseif($item->custom_url) {
                    $link = $item->custom_url;
                }
                if($item->page_anchor) $link .= '#' . $item->page_anchor;
                $l_elements .= '<a href="' . $link . '">';
                    $l_elements .= $item->title;
                $l_elements .= '</a>';
                if($item->sub_menu) {
                    $l_elements .= '<ul>';
                        $l_elements .= self::build_l_elements($item->sub_menu->menu, $vars, $li_class, $li_active_class);
                    $l_elements .= '</ul>';
                }
            $l_elements .= '</li>';
        }

        return $l_elements;
    }

    public static function buildBackendNestable($menu, $pages) {
        $html = '';

        if(!isset($menu->id)) return $html;

        foreach($menu->items as $m_item) {
            $item = [
                'id' => $m_item->id,
                'label' => $m_item->title,
                'page-id' => $m_item->page_id,
                'page_anchor' => $m_item->page_anchor,
                'custom_url' => $m_item->custom_url,
                'is_void' => $m_item->is_void
            ];

            $html .= '<li class="dd-item _itemId' . $m_item->id . '" data-id="' . $m_item->id . '">';
            $html .= '<div class="dd-handle">' . $m_item->title;
            $html .= '<div class="pull-right">';
            $html .= '<a class="_editButton dd-nodrag btn btn-success btn-circle waves-effect waves-light" href="#" data-edit-id="' . $m_item->id . '"><i class="fa fa-pencil"></i></a>';
            $html .= '<a class="_deleteButton dd-nodrag btn btn-danger btn-circle waves-effect waves-light" href="#" data-edit-id="' . $m_item->id . '"><i class="fa fa-trash"></i></a>';
            $html .= '</div></div>';
            $html .= '<div class="_editBox _' . $m_item->id . '"> ' . view('admin.sections.menus.blocks.nestable_item', compact('item', 'pages')) . ' </div>';

            if($m_item->sub_menu) {
                $html .= '<ol class="dd-list">';
                $html .= self::buildBackendNestable($m_item->sub_menu->menu, $pages);
                $html .= '</ol>';
            }

            $html .= '</li>';
        }

        return $html;
    }

}
