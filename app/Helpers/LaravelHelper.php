<?php

if(!function_exists('redirect_success')) {
    function redirect_success($message) {
        return redirect()->back()->with('success', $message);
    }
}

if(!function_exists('redirect_error')) {
    function redirect_error($message) {
        return redirect()->back()->with('error', $message);
    }
}

if(!function_exists('redirect_error_form')) {
    function redirect_error_form($validation, $message = 'The form has invalid inputs.') {
        return redirect()->back()->withErrors($validation)->withInput()->with('error', $message);
    }
}

if(!function_exists('guard_user')) {
    function guard_user() {
        return Auth::guard('user')->user();
    }
}

if(!function_exists('guard_admin')) {
    function guard_admin() {
        return Auth::guard('admin')->user();
    }
}

if(!function_exists('tinymce_key')) {
    function tinymce_key() {
        return env('TINYMCE_KEY');
    }
}

if(!function_exists('href_none')) {
    function href_none() {
        return 'javascript:void(0)';
    }
}

if(!function_exists('menu')) {
    function menu($reference, $view_variables = null, $li_class = '', $li_active_class = 'active') {
        return \App\Helpers\_CMS\MenuHelper::build($reference, $view_variables, $li_class, $li_active_class);
    }
}
