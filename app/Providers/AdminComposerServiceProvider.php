<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminComposerServiceProvider extends ServiceProvider {

    public function boot() {
        view()->composer(
            'admin.blocks.top_nav', 'App\Http\ViewComposers\AdminNavComposer'
        );
        view()->composer(
            'admin.blocks.sidebar', 'App\Http\ViewComposers\AdminSidebarComposer'
        );
    }


    public function register() {

    }

}