<?php namespace App\Providers;

use App\Models\_CMS\Language;
use App\Models\_CMS\Site;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Session;
use App;
use Artisan;
use Schema;

class AppServiceProvider extends ServiceProvider {

    public function boot() {
        Schema::defaultStringLength(191);
        $this->bladeExtend();

        if(!App::runningInConsole()) {
            if(!session('switched_language')) {
                $language = Language::where('default_backend', 1)->first();
                Session::put('switched_language', $language->code);
            }
            if(!session('switched_site')) {
                $site = Site::where('is_default', 1)->first();
                Session::put('switched_site', $site);
            }
        }
    }

    public function register() {
        //...
    }

    private function bladeExtend() {
        // -- Add php switch directive
        Blade::extend(function($value, $compiler) {
            $value = preg_replace('/(\s*)@switch\((.*)\)(?=\s)/', '$1<?php switch($2):', $value);
            $value = preg_replace('/(\s*)@endswitch(?=\s)/', '$1endswitch; ?>', $value);
            $value = preg_replace('/(\s*)@case\((.*)\)(?=\s)/', '$1case $2: ?>', $value);
            $value = preg_replace('/(?<=\s)@default(?=\s)/', 'default: ?>', $value);
            $value = preg_replace('/(?<=\s)@breakswitch(?=\s)/', '<?php break;', $value);
            return $value;
        });
    }

}
