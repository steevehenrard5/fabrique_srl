<?php namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class FrontComposerServiceProvider extends ServiceProvider {

    public function boot() {
        view()->composer(
            'front._master', 'App\Http\ViewComposers\FrontComposer'
        );
    }

    public function register() {}

}
