<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class CRUDController extends Controller {

    protected $soft_delete = false;

    public function add() {
        return $this->getForm();
    }

    public function edit($id) {
        return $this->getForm($id);
    }

    public function create(Request $request) {
        return $this->saveForm($request);
    }

    public function update(Request $request, $id) {
        return $this->saveForm($request, $id);
    }

    public function delete($id) {
        $class = 'App\Models\\' . $this->model;

        $can = $this->beforeDelete($id);

        if($can) {
            if(!$this->soft_delete) {
                $class::find($id)->delete();
            } else {
                $element = $class::find($id);
                $element->is_deleted = 1;
                $element->save();
            }
        }

        return redirect()->back()->with('success', str_replace('_Modules\\', '', str_replace('_CMS\\', '', $this->model) . ' deleted with success.'));
    }

    public function beforeDelete($id) {
        return true;
    }

    public function beforeRestore($id) {
        return true;
    }

    public function restore($id) {
        $class = 'App\Models\\' . $this->model;
        $can = $this->beforeRestore($id);

        if($can) {
            $element = $class::find($id);
            $element->is_deleted = 0;
            $element->save();
        }

        return redirect()->back()->with('success', str_replace('_Modules\\', '', str_replace('_CMS\\', '', $this->model) . ' restored with success.'));
    }

    public function destroy($id) {
        $class = 'App\Models\\' . $this->model;
        $element = $class::find($id);
        $element->is_destroyed = 1;
        $element->save();
        return redirect()->back()->with('success', str_replace('_Modules\\', '', str_replace('_CMS\\', '', $this->model) . ' destroyed.'));
    }

}
