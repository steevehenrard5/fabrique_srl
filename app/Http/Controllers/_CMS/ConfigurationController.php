<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Models\_CMS\Site;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

class ConfigurationController extends Controller {

    public function index() {
        $dash_active = 'configuration';
        $site = Site::with('configuration')->where('is_default', 1)->first();
        if(!$site->configuration)
            return redirect_error('Configuration row missing in database.');

        return view('admin.sections.configuration.form', compact('dash_active', 'site'));
    }

    public function saveForm(Request $request) {
        $rules = [
            'name' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $site = Site::with('configuration')->where('is_default', 1)->first();
            $site->name = $request->input('name');
            $site->save();
            $configuration = $site->configuration;
            if(!$configuration) return redirect_error('Configuration row missing in database.');
            $configuration->youtube = $request->input('youtube');
            $configuration->facebook = $request->input('facebook');
            $configuration->twitter = $request->input('twitter');
            $configuration->instagram = $request->input('instagram');
            $configuration->linkedin = $request->input('linkedin');
            $configuration->footer_copyright = $request->input('footer_copyright');
            $configuration->email_contact = $request->input('email_contact');
            $configuration->phone_contact = $request->input('phone_contact');
            $configuration->address_contact = $request->input('address_contact');

            $directory = public_path() . '/assets/front/img/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);
            if($request->hasFile('logo_website_default')) {
                $file = 'logo_website_default.' . $request->file('logo_website_default')->getClientOriginalExtension();
                $request->file('logo_website_default')->move($directory, $file);
                $configuration->logo_website_default = $file;
            }
            if($request->hasFile('logo_website_header')) {
                $file = 'logo_website_header.' . $request->file('logo_website_header')->getClientOriginalExtension();
                $request->file('logo_website_header')->move($directory, $file);
                $configuration->logo_website_header = $file;
            }
            if($request->hasFile('logo_website_footer')) {
                $file = 'logo_website_footer.' . $request->file('logo_website_footer')->getClientOriginalExtension();
                $request->file('logo_website_footer')->move($directory, $file);
                $configuration->logo_website_footer = $file;
            }

            $configuration->save();
            return redirect_success('Configuration updated.');
        }
    }

    public function removeLogo($reference) {
        $site = Site::with('configuration')->where('is_default', 1)->first();

        $configuration = $site->configuration;
        if(!$configuration)
            return redirect_error('Configuration row is missing from database.');

        $image = $configuration->$reference;
        $configuration->$reference = null;
        $configuration->save();

        $directory = public_path() . '/assets/front/img/';
        if(file_exists($directory . $image))
            unlink($directory . $image);

        return redirect_success('Logo removed.');
    }

}