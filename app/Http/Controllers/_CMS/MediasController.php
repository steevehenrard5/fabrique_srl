<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Models\_CMS\Media;
use Illuminate\Http\Request;

class MediasController extends Controller {

    public function index() {
        $dash_active = 'medias';
        $medias = Media::with('user')->orderBy('created_at', 'DESC')->paginate(12);

        $total_size = Media::sum('file_size');

        return view('admin.sections.medias.index', compact('dash_active', 'medias', 'total_size'));
    }

    public function upload(Request $request) {
        if($request->hasFile('file')) {
            $directory = public_path() . '/uploads/medias/';
            if(!file_exists($directory)) mkdir($directory, 0775, true);

            $medias = collect();
            foreach($request->file('file') as $file) {
                $size = $file->getSize();
                $media = new Media();
                    $name = strtolower(uniqid() . '.' . strtolower($file->getClientOriginalExtension()));
                    $file->move($directory, $name);
                    $media->file_extension = strtolower($file->getClientOriginalExtension());
                    $media->file_size = $size;
                    $media->file_name = $name;
                    $media->user_id = guard_admin()->id;
                $media->save();

                $medias->push(view('admin.sections.medias.row', compact('media'))->render());
            }

            return json_encode($medias);
        } else
            return 'error';
    }

    public function delete($id) {
        $media = Media::find($id);

        $file = public_path() . '/uploads/medias/' . $media->file_name;
        if(file_exists($file)) unlink($file);

        $media->delete();
        return redirect_success('Media deleted.');
    }

}
