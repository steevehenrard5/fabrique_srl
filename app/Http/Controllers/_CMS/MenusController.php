<?php namespace App\Http\Controllers\_CMS;

use App\Helpers\_CMS\MenuHelper;
use App\Models\_CMS\Menu;
use App\Models\_CMS\MenuItem;
use App\Models\_CMS\MenuSubMenu;
use App\Models\_CMS\Page;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class MenusController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Menu';

    public function index() {
        $dash_active = 'menus';
        $menus = Menu::where('site_id', session('switched_site.id'))->doesntHave('is_child')->get();
        return view('admin.sections.menus.index', compact('dash_active', 'menus'));
    }

    public function getForm($id = false) {
        $dash_active = 'menus';

        if($id) $menu = Menu::with('items.page.attributes', 'items.sub_menu.menu')->find($id);
        else $menu = new Menu();

        if($id) $menus = Menu::where('id', '!=', $id)
            ->whereDoesntHave('items.sub_menu', function($query) use ($id) {
                $query->where('menu_id', $id);
            })
            ->where('site_id', session('switched_site.id'))
            ->orderBy('name')
            ->get();
        else $menus = null;

        $pages = Page::with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();

        $nestable = MenuHelper::buildBackendNestable($menu, $pages);

        return view('admin.sections.menus.form', compact('menu', 'dash_active', 'menus', 'pages', 'nestable'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'reference' => 'required',
            'name' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation, 'The form has invalid inputs.');
        } else {
            $menu = $id ? Menu::find($id) : new Menu();
            $menu->reference = $request->input('reference');
            $menu->name = $request->input('name');
            if(!$id) $menu->site_id = session('switched_site.id');
            if($request->has('is_everywhere')) $menu->is_everywhere = 1;
            else $menu->is_everywhere = 0;
            $menu->save();

            return redirect_success('Menu updated.');
        }
    }

    private function buildMenu($menu_id, $data, $request) {
        $order = 1;

        MenuItem::where('menu_id', $menu_id)->delete();

        foreach($data as $d) {
            $id = $d->id;

            $item = new MenuItem();
                $item->title = $request->input('title')[$id];
                $item->page_anchor = $request->input('page_anchor')[$id];
                $item->menu_id = $menu_id;
                $item->order = $order++;
                $item->lang = session('switched_language');

                if(isset($request->input('is_void')[$id])) {
                    $item->is_void = true;
                } else {
                    if($request->input('custom_url')[$id] != '') $item->custom_url = $request->input('custom_url')[$id];
                    else $item->page_id = $request->input('page_id')[$id];
                }
            $item->save();

            if(isset($d->children) && !empty($d->children)) {
                $exists = Menu::where('reference', 'sub_menu_' . str_slug($item->title, '_'))->first();
                $menu = $exists ?? new Menu();
                    $menu->name = 'Sub Menu ' . $item->title;
                    $menu->reference = 'sub_menu_' . \Str::slug($item->title, '_');
                    $menu->site_id = session('switched_site.id');
                    $menu->is_everywhere = 0;
                $menu->save();

                $sub = new MenuSubMenu();
                    $sub->parent_id = $item->id;
                    $sub->menu_id = $menu->id;
                $sub->save();

                $this->buildMenu($menu->id, $d->children, $request);
            }
        }
    }

    public function updateItems(Request $request) {
        $data = json_decode($request->input('nestableData'));
        $this->buildMenu($request->input('menu_id'), $data, $request);

        MenuSubMenu::doesntHave('item')->delete();

        return redirect_success('Menu updated.');
    }

    public function delete($id, $internal_call = false, $is_recursive = false) {
        $menu = Menu::find($id);
        MenuSubMenu::where('menu_id', $menu->id)->delete();

        foreach($menu->items as $item) {
            if($item->sub_menu) {
                $this->delete($item->sub_menu->menu_id, true, true);
                $item->sub_menu->delete();
            }

            $item->delete();
        }

        if($is_recursive || !$internal_call) {
            $menu->delete();
        }

        if(!$internal_call) {
            return redirect_success('Menu and items deleted.');
        }
    }

    public function buildItem(Request $request) {
        $item = $request->input('item');
        $pages = Page::with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();

        return view('admin.sections.menus.blocks.nestable_item', compact('item', 'pages'));
    }

}
