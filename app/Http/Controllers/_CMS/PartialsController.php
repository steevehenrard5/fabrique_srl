<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Module;
use App\Models\_CMS\Partial;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class PartialsController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Partial';

    public function index() {
        $dash_active = 'partials';
        $partials = Partial::with(['module' => function($query) {
            $query->orderBy('modules.name');
        }])->get();
        return view('admin.sections.partials.index', compact('dash_active', 'partials'));
    }

    public function getForm($id = false) {
        $dash_active = 'partials';
        $partial = $id ? Partial::find($id) : new Partial();
        $modules = Module::where('is_active', 1)->orderBy('name')->get();
        return view('admin.sections.partials.form', compact('dash_active', 'partial', 'modules'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'partial_path' => 'required',
            'module_id' => 'required|not_in:0',
        ];

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()) return redirect_error_form($validation);

        $partial = $id ? Partial::find($id) : new Partial();
            $partial->name = $request->input('name');
            $partial->description = $request->input('description');
            $partial->side = $request->input('side');
            $partial->partial_path = $request->input('partial_path');
            $partial->module_id = $request->input('module_id');
            $partial->body_class = $request->input('body_class');
            if($request->has('is_details_partial')) $partial->is_details_partial = 1;
            else $partial->is_details_partial = 0;
        $partial->save();

        return redirect_success('Partial updated.');
    }

    public function deletePartial($id) {
        Partial::find($id)->delete();
        return redirect_success('Partial removed.');
    }

}