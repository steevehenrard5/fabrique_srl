<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Admin;
use App\Models\_CMS\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use PragmaRX\Google2FA\Google2FA;
use Validator;

class AdminsController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Admin';

    public function index() {
        $dash_active = 'operators';
        $operators = Admin::all();
        return view('admin.sections.operators.index', compact('dash_active', 'operators'));
    }

    public function getForm($id = false) {
        $dash_active = 'operators';

        $user = $id ? Admin::find($id) : new Admin();
        $modules = Module::where('is_active', 1)->orderBy('name')->get();

        return view('admin.sections.operators.form', compact('dash_active', 'user', 'modules'));
    }

    public function saveForm(Request $request, $id = false) {
        $user = $id ? Admin::find($id) : new Admin();
        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'password_confirmation' => 'required_with:password'
        ];
        if($request->filled('password')) {
            $rules['password'] = 'confirmed';
        }
        if(!$id) {
            $rules['password'] = 'required';
            $rules['email'] = 'unique:admins';
            $rules['username'] = 'unique:admins';
        } else {
            if($user->email != $request->input('email')) {
                $rules['email'] = 'unique:admins';
            }
            if($user->username != $request->input('username')) {
                $rules['username'] = 'unique:admins';
            }
        }
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $user->username = $request->input('username');
            $user->first_name = ucfirst($request->input('first_name'));
            $user->last_name = ucfirst($request->input('last_name'));
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->level = $request->input('level');
            $user->permissions = json_encode($request->input('permissions'));
            if($request->has('is_support')) {
                $user->is_support = 1;
            } else $user->is_support = 0;
            if($request->filled('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
            return redirect_success('Admin updated.');
        }
    }

    public function delete($id) {
        if(guard_admin()->level < 100) return redirect_error('You have no permission to delete this operator.');
        $class = 'App\Models\\' . $this->model;
        if(!$this->soft_delete) {
            $class::find($id)->delete();
        } else {
            $element = $class::find($id);
            $element->is_deleted = 1;
            $element->save();
        }
        return redirect()->back()->with('success', $this->model . ' deleted with success.');
    }

    public function getGoogle2FAForm() {
        $admin = guard_admin();
        if(!$admin->g2fa_confirmed) {
            $google2fa = new Google2FA();
            $secret = $google2fa->generateSecretKey(32);

            $admin->g2fa_secret = $secret;
            $admin->save();

            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                config('_project.name') . ' | ' . config('_CMS._global.name'),
                $admin->email,
                $secret
            );
        } else {
            $secret = null;
            $google2fa_url = null;
        }
        return view('admin.google_auth', compact('secret', 'google2fa_url'));
    }

    public function checkGoogle2FAForm(Request $request) {
        $secret = $request->input('google_secret');
        if($request->has('google_secret')) {
            try {
                $google2fa = new Google2FA();
                $window = config('_CMS._global.google2FA_window', 0);
                $valid = $google2fa->verifyKey(guard_admin()->g2fa_secret, $secret, $window);
                if($valid) {
                    $admin = guard_admin();
                    $admin->g2fa_confirmed = 1;
                    $admin->save();
                    $request->session()->put('google_authentication', true);
                    return redirect()->route('admin.dashboard')
                        ->with('success', 'Google 2FA Authentication succeeded.')
                        ->cookie('google2FA', 1, config('_CMS._global.google2FA_cookie_lifespan', 60));
                } else
                    return redirect()->back()->with('error', 'Failed : The code does not match.');
            } catch(\ErrorException $e){
                return redirect()->route('admin.google')->with('error', 'An error has occurred.');
            }
        } else return redirect()->back()->with('error', 'Please enter the code.');
    }

}
