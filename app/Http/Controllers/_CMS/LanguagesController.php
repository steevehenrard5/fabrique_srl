<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Language;
use App\Models\_CMS\Variable;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class LanguagesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Language';

    public function index() {
        $dash_active = 'languages';
        $languages = Language::all();
        $variables = Variable::with('siblings')
            ->where('lang', session('switched_language'))
            ->orderBy('reference')
            ->get();
        $can_operate = in_array(guard_admin()->level, [100]);

        return view('admin.sections.languages.index', compact('dash_active', 'languages', 'variables', 'can_operate'));
    }

    public function getForm($id = false) {
        $dash_active = 'languages';
        $language = $id ? Language::find($id) : new Language();
        return view('admin.sections.languages.form', compact('dash_active', 'language'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'code' => 'required',
            'name' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $language = $id ? Language::find($id) : new Language();
                $language->code = strtolower($request->input('code'));
                $language->name = ucfirst($request->input('name'));
                if($request->has('default_backend')) {
                    $before = Language::where('default_backend', 1)->first();
                    if(($before && $id && $before->id != $id) || ($before && !$id)) {
                        $before->default_backend = 0;
                        $before->save();
                    }
                    $language->default_backend = 1;
                } else $language->default_backend = 0;
                if($request->has('default_frontend')) {
                    $before = Language::where('default_frontend', 1)->first();
                    if(($before && $id && $before->id != $id) || ($before && !$id)) {
                        $before->default_frontend = 0;
                        $before->save();
                    }
                    $language->default_frontend = 1;
                } else $language->default_frontend = 0;
            $language->save();
            return redirect_success('Language updated.');
        }
    }

    public function switchActiveState($id) {
        $language = Language::find($id);
        $language->is_used = !$language->is_used;
        $language->save();
        return redirect()->back()->with('success', 'Active state switched.');
    }

}
