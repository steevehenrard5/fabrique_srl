<?php namespace App\Http\Controllers\_CMS;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class GroupsController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_CMS\Group';

    public function index() {
        $dash_active = 'groups';
        return view('admin.sections.groups.index', compact('dash_active'));
    }

}