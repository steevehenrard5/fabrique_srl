<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Module;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class ModulesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Module';

    public function index() {
        if(guard_admin()->level < 100) return redirect()->route('admin.dashboard')->with('error', 'You can\'t access this location.');
        $dash_active = 'modules';
        $modules = Module::orderBy('is_active', 'DESC')->orderBy('name')->get();
        return view('admin.sections.modules.index', compact('dash_active', 'modules'));
    }

    public function getForm($id = false) {
        $dash_active = 'modules';
        $module = $id ? Module::find($id) : new Module();
        return view('admin.sections.modules.form', compact('dash_active', 'module'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'reference' => 'required',
            'class_name' => 'required',
            'icon' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
             return redirect_error_form($validation);
        } else {
            $module = $id ? Module::find($id) : new Module();
            $module->name = $request->input('name');
            $module->reference = $request->input('reference');
            $module->class_name = $request->input('class_name');
            $module->description = $request->input('description');
            $module->targetable_by = str_replace(' ', '', trim($request->input('targetable_by')));
            $module->orderable_by = str_replace(' ', '', trim($request->input('orderable_by')));
            $module->icon = $request->input('icon');
            if($request->has('is_block')) $module->is_block = 1;
            else $module->is_block = 0;
            if($request->has('is_linkable')) $module->is_linkable = 1;
            else $module->is_linkable = 0;
            if($request->has('is_multiple')) $module->is_multiple = 1;
            else $module->is_multiple = 0;
            if($request->has('is_targetable_by_slug')) $module->is_targetable_by_slug = 1;
            else $module->is_targetable_by_slug = 0;
            $module->save();
            return redirect_success('Module updated.');
        }
    }

    public function switchActiveState($id) {
        $module = Module::find($id);
        $module->is_active = !$module->is_active;
        $module->save();
        return redirect()->back()->with('success', 'Active state switched.');
    }

}