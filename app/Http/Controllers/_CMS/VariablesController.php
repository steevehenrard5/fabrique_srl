<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Language;
use App\Models\_CMS\Variable;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class VariablesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Variable';

    public function getForm($id = false) {
        $dash_active = 'languages';
        $variable = $id ? Variable::find($id) : new Variable();
        $languages = Language::where('is_used', 1)->get();
        return view('admin.sections.variables.form', compact('dash_active', 'variable', 'languages'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'value' => 'required',
        ];
        if(!$id) {
            $rules['reference'] = 'required';
            $rules['lang'] = 'required|not_in:0';
        }
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $variable = $id ? Variable::find($id) : new Variable();
                if(!$id) {
                    $exists = Variable::where('reference', $request->input('reference'))->where('lang', $request->input('lang'))->first();
                    if($exists) $variable = $exists;

                    $variable->lang = $request->input('lang');
                    $variable->reference = $request->input('reference');
                }
                $variable->value = $request->input('value');
            $variable->save();
            return redirect_success('Variable updated.');
        }
    }

    public function sync($code) {
        $variables = Variable::where('lang', session('switched_language'))
            ->whereDoesntHave('siblings', function($query) use($code) {
                $query->where('lang', $code);
            })
            ->get();

        if($variables->count() == 0)
            return redirect_error('Variables are already synced.');

        foreach($variables as $variable) {
            $sync = $variable->replicate();
                $sync->lang = $code;
            $sync->save();
        }

        return redirect_success('Variables successfully synced.');
    }

}
