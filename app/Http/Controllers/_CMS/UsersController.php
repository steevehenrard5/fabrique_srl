<?php namespace App\Http\Controllers\_CMS;

use App\Models\User;
use App\Models\UserLog;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class UsersController extends CRUDController {

    protected $soft_delete = true;
    protected $model = 'User';

    public function index() {
        $dash_active = 'users';
        $users = User::orderBy('username')->where('site_id', session('switched_site.id'))->where('is_destroyed', 0)->paginate(30);
        return view('admin.sections.users.index', compact('dash_active', 'users'));
    }

    public function getForm($id = false) {
        $dash_active = 'users';
        $user = $id ? User::find($id) : new User();
        return view('admin.sections.users.form', compact('dash_active', 'user'));
    }

    public function saveForm(Request $request, $id = false) {
        $user = $id ? User::find($id) : new User();
        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'password_confirmation' => 'required_with:password'
        ];
        if($request->filled('password')) {
            $rules['password'] = 'confirmed';
        }
        if(!$id) {
            $rules['password'] = 'required';
            $rules['email'] = 'unique:users';
            $rules['username'] = 'unique:users';
        } else {
            if($user->email != $request->input('email')) {
                $rules['email'] = 'unique:users';
            }
            if($user->username != $request->input('username')) {
                $rules['username'] = 'unique:users';
            }
        }
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
             return redirect_error_form($validation);
        } else {
            $user->username = $request->input('username');
            $user->first_name = ucfirst($request->input('first_name'));
            $user->last_name = ucfirst($request->input('last_name'));
            $user->email = $request->input('email');
            if($request->filled('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
            return redirect_success('User updated.');
        }
    }

    public function deleteLog($id) {
        UserLog::find($id)->delete();
        return redirect_success('Log deleted.');
    }

}
