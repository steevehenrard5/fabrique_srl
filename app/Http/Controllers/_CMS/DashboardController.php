<?php namespace App\Http\Controllers\_CMS;

use App\Models\_CMS\Admin;
use App\Models\_CMS\Page;
use App\Models\_CMS\Site;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Session;
use Analytics;

class DashboardController extends Controller {

    public function index() {
        $dash_active = 'dashboard';
        $support = Admin::where('is_support', 1)->get();
        $counters = new \StdClass();
        $counters->users = User::count();
        $counters->admins = Admin::count();
        $counters->pages = Page::where('site_id', session('switched_site.id'))->where('is_deleted', 0)->count();
        return view('admin.dashboard', compact('dash_active', 'counters', 'support'));
    }

    public function logout($guard = 'admin') {
        Auth::guard($guard)->logout();
        return redirect()->to('/');
    }

    public function getLoginForm($guard = 'admin') {
        if(Auth::guard($guard)->check()) {
            return redirect()->to('/admin');
        }
        return view('admin.login');
    }

    public function login(Request $request, $guard = 'admin') {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect()->back()->with('error', 'The form has invalid inputs.');
        } else {
            if(Auth::guard($guard)->attempt([
                'username' => $request->input('username'), 'password' => $request->input('password')
            ])) {
                return redirect()->route('admin.dashboard');
            } else {
                return redirect()->back()->with('error', 'Invalid username and/or password.');
            }
        }
    }

    public function switchLanguage($code) {
        Session::put('switched_language', $code);
        return redirect()->back()->with('success', 'Language switched');
    }

    public function switchSite($id) {
        $site = Site::find($id);
        Session::put('switched_site', $site);
        return redirect()->back()->with('success', 'Site switched');
    }

    public function flushSession() {
        $user = guard_admin();
        $site = session('switched_site');
        $language_code = session('switched_language');

        Session::flush();

        Session::put('switched_site', $site);
        Session::put('switched_language', $language_code);
        Auth::guard('admin')->login($user);

        return redirect()->back();
    }

}
