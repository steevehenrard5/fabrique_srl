<?php namespace App\Http\Controllers;

use App\Models\_CMS\Module;
use App\Models\_CMS\Page;
use App\Models\_CMS\PageAttribute;
use App\Models\_CMS\Partial;
use App\Http\Requests;
use App;

class FrontController extends Controller {

    public $functions_controller = 'App\Http\Controllers\FunctionsController';

    public function getView($url = false) {
        $page = $this->getPage($url);
        if(!$page) App::abort(404, trans('errors.not_found'));

        $attributes = $page->f_attributes;

        $contents_ = $this->getContents($page);
        $contents = $contents_['contents'];
        $contents_by_ref = $contents_['contents_by_ref'];

        $params = null;
        switch($url) {
            //-- Custom content loading
        }

        if(config('app.debug')) {
            debug($page);
            debug($attributes);
            debug($contents);
            debug($contents_by_ref);
        }

        if($page->directory && !empty($page->directory)) $view = $page->directory;
        else $view = 'pages/_default';

        return view('front.' . $view, compact('page', 'attributes', 'contents', 'contents_by_ref', 'params'));
    }

    public function getPreviewModuleDetails($url, $reference, $id_or_slug) {
        return $this->getModuleDetails($url, $reference, $id_or_slug, true);
    }

    public function getHiddenModuleDetails($url, $reference, $id_or_slug) {
        return $this->getModuleDetails($url, $reference, $id_or_slug, true, false);
    }

    public function getModuleDetails($url, $reference, $id_or_slug, $is_preview = false, $is_manageable = true) {
        $page = $this->getPage($url);
        if(!$page) App::abort(404, trans('errors.not_found'));

        $module = Module::where('reference', $reference)
            ->where(function($query) use($is_manageable) {
                if($is_manageable) {
                    $query->where('is_active', 1);
                }
            })
            ->first();

        if(!$module) App::abort(404, trans('errors.not_found'));

        $class_name = '\App\Models\_Modules\\' . $module->class_name;
        if(class_exists($class_name)) {
            $class = new $class_name;
        } else App::abort(404, trans('errors.not_found'));

        if(method_exists($class, 'getDetailsContent')) {
            $item = $class->getDetailsContent($module, $id_or_slug, $is_preview);
        } else App::abort(404, trans('errors.not_found'));
        if(!$item) App::abort(404, trans('errors.not_found'));

        $contents_ = $this->getContents($page);
        $contents = $contents_['contents'];
        $contents_by_ref = $contents_['contents_by_ref'];

        $attributes = $page->f_attributes;

        if(config('app.debug')) {
            debug($page);
            debug($attributes);
            debug($module);
            debug($item);
        }

        $partial_details = Partial::where('is_details_partial', 1)->where('module_id', $module->id)->first();

        if($page->directory && !empty($page->directory)) $view = $page->directory;
        else $view = 'pages/_default';


        return view('front.' . $view . '_details', compact('page', 'item', 'module', 'partial_details', 'attributes', 'contents', 'contents_by_ref'));
    }

    private function getPage($url = false) {
        if(!$url) {
            $page = Page::has('f_attributes')
                ->with('f_attributes', 'f_contents', 'menus.menu.f_items.page.f_attributes', 'menus.menu.f_items.sub_menu.menu')
                ->where('is_home', 1)
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('is_destroyed', 0)
                ->where('site_id', session('switched_site.id'))
                ->first();
        } else {
            $page = Page::whereHas('f_attributes', function($query) use($url) {
                    $query
                        ->where('lang', App::getLocale())
                        ->where('url', $url);
                })
                ->with('f_attributes', 'f_contents', 'menus.menu.f_items.page.f_attributes', 'menus.menu.f_items.sub_menu.menu')
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('is_destroyed', 0)
                ->where('site_id', session('switched_site.id'))
                ->first();
        }
        return $page;
    }

    private function getContents($page) {
        $contents = $page->f_contents;
        $contents_by_ref = collect();

        if($contents) {
            $contents->load('partial');
            $contents->load('type');
            $contents->load('module');
            foreach($contents as $key => &$content) {
                if($content->module_id) {
                    $class_name = '\App\Models\_Modules\\' . $content->module->class_name;
                    if(class_exists($class_name)) {
                        $class = new $class_name;
                    } else {
                        $contents->forget($key);
                        continue;
                    }
                    if(method_exists($class, 'getFrontContent')) {
                        $items = $class->getFrontContent($content);
                        $content->items = $items;
                    } else {
                        $contents->forget($key);
                        continue;
                    }
                }
                $contents_by_ref->put($content->reference, $content);
            }
        }

        return [
            'contents_by_ref' => $contents_by_ref,
            'contents' => $contents,
        ];
    }

}
