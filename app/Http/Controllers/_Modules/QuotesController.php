<?php namespace App\Http\Controllers\_Modules;

use App\Models\_CMS\Module;
use App\Models\_CMS\Partial;
use App\Models\_Modules\Quote;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class QuotesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_Modules\Quote';

    public function index() {
        $dash_active = 'module_quotes';
        $quotes = Quote::where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.quotes.index', compact('dash_active', 'quotes'));
    }

    public function getForm($id = false) {
        $dash_active = 'module_quotes';
        $quote = $id ? Quote::find($id) : new Quote();
        $module = Module::where('reference', 'quotes')->first();
        $partials = Partial::where('module_id', $module->id)->orderBy('name')->get();
        return view('admin.modules.quotes.form', compact('dash_active', 'quote', 'partials'));
    }

    public function switchDraftState($id) {
        $quote = Quote::find($id);
        $quote->is_draft = !$quote->is_draft;
        $quote->save();
        return redirect_success('Draft state changed.');
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'title' => 'required',
            'head_title' => 'required',
            'description' => 'required',
            'partial_id' => 'required|not_in:0',
            'quote' => 'required',
            'author' => 'required',
            'function' => 'required',
        ];
        if(!$id) $rules['image'] = 'required';
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $quote = $id ? Quote::find($id) : new Quote();
            if(!$id) {
                $quote->lang = session('switched_language');
                $quote->site_id = session('switched_site.id');
            }
            $quote->partial_id = $request->input('partial_id');
            $quote->name = $request->input('name');
            $quote->head_title = $request->input('head_title');
            $quote->title = $request->input('title');
            $quote->description = $request->input('description');
            $quote->quote = $request->input('quote');
            $quote->author = $request->input('author');
            $quote->function = $request->input('function');
            if($request->hasFile('image')) {
                $directory = public_path() . '/uploads/img/modules/quotes/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('image')->getClientOriginalName();
                $request->file('image')->move($directory, $name);
                $quote->image = $name;
            }
            $quote->save();
            return redirect_success('Quote updated.');
        }
    }

}