<?php namespace App\Http\Controllers\_Modules;

use App\Models\_Modules\Article;
use App\Models\_Modules\ArticleCategory;
use App\Models\_Modules\ArticleType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class ArticlesController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_Modules\Article';

    public function index() {
        $dash_active = 'module_articles';
        $articles = Article::with('type')->where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.articles.index', compact('dash_active', 'articles'));
    }

    public function switchDraftState($id) {
        $article = Article::find($id);
        $article->is_draft = !$article->is_draft;
        $article->save();
        return redirect_success('Draft state changed.');
    }

    public function getForm($id = false) {
        $dash_active = 'module_articles';
        $article = $id ? Article::find($id) : new Article();
        $categories = ArticleCategory::where('lang', session('switched_language'))->orderBy('name')->get();
        $types = ArticleType::orderBy('name')->get();
        return view('admin.modules.articles.form', compact('dash_active', 'article', 'categories', 'types'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'title' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $article = $id ? Article::find($id) : new Article();
            $article->title = $request->input('title');
            if($request->has('slug') && !empty($request->input('slug'))) {
                $article->slug = \Str::slug($request->input('slug'));
            } else $article->slug = \Str::slug($article->title);
            $article->description = $request->input('description');
            $article->description_short = $request->input('description_short');
            $article->custom_date = date('Y-m-d', strtotime($request->input('custom_date')));
            $article->content = $request->input('content');
            if($request->has('category_id') && $request->input('category_id') != 0) $article->category_id = $request->input('category_id');
            else $article->category_id = null;
            $article->type_id = $request->input('type_id');
            $article->video_url = $request->input('video_url');
            $article->file_name = $request->input('file_name');
            $article->tags = $request->input('tags');
            if(!$id) {
                $article->creator_id = guard_admin()->id;
                $article->lang = session('switched_language');
                $article->site_id = session('switched_site.id');
            }
            if($request->has('delete_picture')) {
                $article->picture = '';
            }
            if($request->has('delete_file')) {
                $article->file = '';
            }
            if($request->hasFile('picture')) {
                $directory = public_path() . '/uploads/img/modules/articles/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('picture')->getClientOriginalName();
                $request->file('picture')->move($directory, $name);
                $article->picture = $name;
            }
            if($request->hasFile('file')) {
                $directory = public_path() . '/uploads/files/modules/articles/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('file')->getClientOriginalName();
                $request->file('file')->move($directory, $name);
                $article->file = $name;
            }
            $article->save();
            return redirect_success('Article updated.');
        }
    }

    /* Categories */

    public function categories() {
        $dash_active = 'module_articles';
        $categories = ArticleCategory::where('lang', session('switched_language'))->paginate(12);
        return view('admin.modules.articles.categories.index', compact('dash_active', 'categories'));
    }

    public function editCategory($id) {
        return $this->getFormCategory($id);
    }

    public function addCategory() {
        return $this->getFormCategory();
    }

    public function createCategory(Request $request) {
        return $this->saveFormCategory($request);
    }

    public function updateCategory(Request $request, $id) {
        return $this->saveFormCategory($request, $id);
    }

    public function getFormCategory($id = false) {
        $dash_active = 'module_articles';
        $category = $id ? ArticleCategory::find($id) : new ArticleCategory();
        return view('admin.modules.articles.categories.form', compact('dash_active', 'category'));
    }

    public function saveFormCategory($request, $id = false) {
        $rules = [
            'name' => 'required'
        ];

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()) return redirect_error_form($validation);

        $category = $id ? ArticleCategory::find($id) : new ArticleCategory();
        if(!$id) $category->lang = session('switched_language');
        $category->name = $request->input('name');
        if(!$request->has('slug')) $category->slug = \Str::slug($request->input('name'));
        else $category->slug = $request->input('slug');
        $category->save();
        return redirect_success('Category updated.');
    }

    public function deleteCategory($id) {
        ArticleCategory::find($id)->delete();
        Article::where('category_id', $id)->update(['category_id' => null]);
        return redirect_success('Category removed');
    }

}
