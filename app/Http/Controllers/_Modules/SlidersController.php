<?php namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\CRUDController;
use App\Models\_CMS\Module;
use App\Models\_CMS\Page;
use App\Models\_CMS\Partial;
use App\Models\_Modules\SliderPicture;
use App\Models\_Modules\Slider;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

class SlidersController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_Modules\Slider';

    public function index() {
        $dash_active = 'module_sliders';
        $sliders = Slider::with('pictures')->where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.sliders.index', compact('dash_active', 'sliders'));
    }

    public function switchDraftState($id) {
        $slider = Slider::find($id);
        $slider->is_draft = !$slider->is_draft;
        $slider->save();
        return redirect_success('Draft state changed.');
    }

    public function getForm($id = false) {
        $dash_active = 'module_sliders';
        $slider = $id ? Slider::find($id) : new Slider();

        $max_queue_size = 15;
        $max_files_size = 20;

        $module = Module::where('reference', 'sliders')->first();
        $partials = Partial::where('module_id', $module->id)->orderBy('name')->get();

        $pages = Page::with('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();

        return view('admin.modules.sliders.form', compact('dash_active', 'slider', 'max_queue_size', 'max_files_size', 'partials', 'pages'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'partial_id' => 'required|not_in:0',
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $slider = $id ? Slider::find($id) : new Slider();
            $slider->name = $request->input('name');
            $slider->partial_id = $request->input('partial_id');
            $slider->call_to_action_text = $request->input('cta_text');
            $slider->call_to_action_anchor = $request->input('cta_anchor');
            if(!$id) {
                $slider->creator_id = guard_admin()->id;
                $slider->lang = session('switched_language');
                $slider->site_id = session('switched_site.id');
            }
            $slider->save();
            if($request->has('titles')) {
                foreach($request->input('titles') as $key => $title) {
                    $picture = SliderPicture::find($key);
                    $picture->title = $title;
                    $picture->head_title = $request->input('head_titles')[$key];
                    $picture->description = $request->input('descriptions')[$key];
                    $picture->call_to_action_text = $request->input('call_to_action_texts')[$key];
                    $picture->call_to_action_type = $request->input('call_to_action_types')[$key];
                    if($request->input('page_ids')[$key]) {
                        $picture->page_id = $request->input('page_ids')[$key];
                    } else $picture->custom_url = $request->input('custom_urls')[$key];
                    $picture->save();
                }
            }
            return redirect_success('Slider updated.');
        }
    }

    public function upItem($id) {
        $item = SliderPicture::find($id);
        $before = SliderPicture::where('order', $item->order - 1)->where('slider_id', $item->slider_id)->first();
        $before->order++;
        $before->save();
        $item->order--;
        $item->save();
        return redirect_success('Pictures order has been updated.');
    }

    public function downItem($id) {
        $item = SliderPicture::find($id);
        $after = SliderPicture::where('order', $item->order + 1)->where('slider_id', $item->slider_id)->first();
        $after->order--;
        $after->save();
        $item->order++;
        $item->save();
        return redirect_success('Pictures order has been updated.');
    }

    public function deleteItem($id) {
        $item = SliderPicture::find($id);
        $after_items = SliderPicture::where('order', '>', $item->order)->where('slider_id', $item->slider_id)->get();
        foreach($after_items as $after) {
            $after->order--;
            $after->save();
        }
        $item->delete();
        return redirect_success('Picture removed from the slider.');
    }

    public function uploadPictures(Request $request) {
        $slider_id = $request->input('slider_id');
        $order = SliderPicture::where('slider_id', $slider_id)->max('order');

        $directory = public_path() . '/uploads/img/modules/sliders/';
        if(!file_exists($directory)) mkdir($directory, 0777, true);
        if($request->hasFile('file')) {
            foreach($request->file('file') as $picture) {
                $order++;
                $photo = new SliderPicture();
                $photo->order = $order;
                $photo->slider_id = $slider_id;

                $name = time() . '_' . $picture->getClientOriginalName();
                $picture->move($directory, $name);
                $photo->photo = $name;

                $photo->save();
            }
        } else return 'error';
        return 'success';
    }

}