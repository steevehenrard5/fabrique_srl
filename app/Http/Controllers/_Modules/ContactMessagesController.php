<?php namespace App\Http\Controllers\_Modules;

use App\Models\_Modules\ContactMessage;
use App\Http\Controllers\CRUDController;
use Validator;

class ContactMessagesController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_Modules\ContactMessage';

    public function index() {
        $dash_active = 'module_contact_messages';
        $contact_messages = ContactMessage::orderBy('is_deleted')
            ->orderBy('is_new', 'DESC')
            ->orderBy('created_at')
            ->where('site_id', session('switched_site.id'))
            ->where('is_destroyed', 0)
            ->paginate(30);
        return view('admin.modules.contact_messages.index', compact('dash_active', 'contact_messages'));
    }

    public function show($id) {
        $dash_active = 'module_contact_messages';
        $message = ContactMessage::find($id);
        $message->is_new = 0;
        $message->save();
        return view('admin.modules.contact_messages.show', compact('dash_active', 'message'));
    }

}