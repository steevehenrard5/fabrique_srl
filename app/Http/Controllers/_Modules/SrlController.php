<?php

namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\_Modules\Srl;
use Illuminate\Support\Facades\Validator;

class SrlController extends Controller
{
    protected $model = '_Modules\Srl';

    public function saveForm(Request $request) {
        $rules = [
            'firstname' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $srl = new Srl();
            $srl->lastname = $request->input('lastname');
            $srl->firstname = $request->input('firstname');
            $srl->email = $request->input('email');
            $srl->phone = $request->input('phone');
            $srl->enterprise_start = $request->input('enterprise_start');
            $srl->activity = $request->input('activity');
            $srl->project_text = $request->input('project_text');
            $srl->call_request = $request->input('call_request');
            $srl->save();
            return redirect_success('Demande de devis envoyée!');
        }
    }
}
