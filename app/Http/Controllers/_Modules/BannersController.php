<?php namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\CRUDController;
use App\Models\_CMS\Page;
use App\Models\_Modules\Banner;
use App\Models\_Modules\BannerLink;
use App\Models\_CMS\Partial;
use Illuminate\Http\Request;
use App\Models\_CMS\Content;
use App\Models\_CMS\Module;
use App\Http\Requests;
use Validator;

class BannersController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_Modules\Banner';

    public function index() {
        $dash_active = 'module_banners';
        $banners = Banner::where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.banners.index', compact('dash_active', 'banners'));
    }

    public function getForm($id = false) {
        $dash_active = 'module_banners';
        $banner = $id ? Banner::with('links')->find($id) : new Banner();
        $pages = Page::with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();
        $module = Module::where('reference', 'banners')->first();
        $partials = Partial::where('module_id', $module->id)->orderBy('name')->get();
        return view('admin.modules.banners.form', compact('dash_active', 'banner', 'pages', 'partials'));
    }

    public function switchDraftState($id) {
        $banner = Banner::find($id);
        $banner->is_draft = !$banner->is_draft;
        $banner->save();
        return redirect_success('Draft state changed.');
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'partial_id' => 'required|not_in:0',
        ];
        if(!$id) $rules['image'] = 'required';

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()) return redirect_error_form($validation);

        $banner = $id ? Banner::find($id) : new Banner();
            if(!$id) {
                $banner->site_id = session('switched_site.id');
                $banner->lang = session('switched_language');
            }
            $banner->name = $request->input('name');
            $banner->partial_id = $request->input('partial_id');
            if($request->hasFile('image')) {
                $directory = public_path() . '/uploads/img/modules/banners/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('image')->getClientOriginalName();
                $request->file('image')->move($directory, $name);
                $banner->image = $name;
            }
        $banner->save();

        BannerLink::where('banner_id', $banner->id)->delete();
        if($request->has('banner_call_to_action_texts') && !empty($request->input('banner_call_to_action_texts'))) {
            foreach($request->input('banner_call_to_action_texts') as $key => $value) {
                if(!$value) continue;
                $link = new BannerLink();
                $link->banner_id = $banner->id;
                $link->call_to_action_text = $value;
                $link->type = $request->input('banner_types')[$key];
                if($request->input('banner_page_ids')[$key]) $link->page_id = $request->input('banner_page_ids')[$key];
                $link->custom_url = $request->input('banner_custom_urls')[$key];
                $link->video_url = $request->input('banner_video_urls')[$key];
                $link->save();
            }
        }
        return redirect_success('Banner updated.');
    }

    public function create_new(Request $request) {
        $banner = new Banner();
            $banner->partial_id = $request->input('block_partial_id');
            $banner->lang = session('switched_language');
            $banner->site_id = session('switched_site.id');
            $banner->name = $request->input('block_name');
            $banner->is_draft = 0;
            if($request->hasFile('block_image')) {
                $directory = public_path() . '/uploads/img/modules/banners/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('block_image')->getClientOriginalName();
                $request->file('block_image')->move($directory, $name);
                $banner->image = $name;
            }
        $banner->save();

        if($request->has('banner_call_to_action_texts') && !empty($request->input('banner_call_to_action_texts'))) {
            foreach($request->input('banner_call_to_action_texts') as $key => $value) {
                if(!$value) continue;
                $link = new BannerLink();
                    $link->banner_id = $banner->id;
                    $link->call_to_action_text = $value;
                    $link->type = $request->input('banner_types')[$key];
                    if($request->input('banner_page_ids')[$key]) $link->page_id = $request->input('banner_page_ids')[$key];
                    $link->custom_url = $request->input('banner_custom_urls')[$key];
                    $link->video_url = $request->input('banner_video_urls')[$key];
                $link->save();
            }
        }

        $order = Content::where('page_id', $request->input('page_id'))->where('lang', session('switched_language'))->max('order');
        $module = Module::find($request->input('module_id'));

        $content = new Content();
            $content->reference = $request->input('reference');
            if($request->has('css_class')) $content->css_class = $request->input('css_class');
            $content->module_id = $module->id;
            $content->lang = session('switched_language');
            $content->page_id = $request->input('page_id');
            $content->order = $order + 1;
            $content->module_item_id = $banner->id;
            if($request->has('is_published')) {
                $content->is_draft = 0;
            }
        $content->save();
        return redirect_success('Module content created.');
    }

}