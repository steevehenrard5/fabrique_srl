<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class GuestMiddleware {

    public function handle($request, Closure $next, $guard = 'user') {
        if(Auth::guard($guard)->check()) {
            return redirect()->route('front.account');
        }
        return $next($request);
    }

}