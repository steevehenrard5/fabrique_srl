<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthMiddleware {

    public function handle($request, Closure $next, $guard = 'user') {
        if(!Auth::guard($guard)->check()) {
            return redirect()->route('front.login');
        }
        return $next($request);
    }

}