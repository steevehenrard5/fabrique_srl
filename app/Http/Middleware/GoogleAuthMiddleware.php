<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class GoogleAuthMiddleware {

    public function handle($request, Closure $next, $guard = 'admin') {
        if(config('_CMS._global.google2FA')) {
            if((is_null($request->cookie('google2FA'))) || (is_null(Auth::guard($guard)->user()->google2fa_secret))) {
                if(!$request->session()->has('google_authentication')) {
                    return redirect()->route('admin.login.google2fa');
                }
            }
        }
        return $next($request);
    }

}
