<?php namespace App\Http\ViewComposers;

use App\Models\_CMS\Module;
use Illuminate\View\View;

class AdminSidebarComposer {

    public function compose(View $view) {
        $modules = Module::where('is_active', 1)->orderBy('name')->get();
        $view->with('_modules', $modules);
    }

}
