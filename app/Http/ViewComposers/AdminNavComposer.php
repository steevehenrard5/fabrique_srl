<?php namespace App\Http\ViewComposers;

use App\Models\_CMS\Language;
use App\Models\_CMS\Site;
use Illuminate\View\View;

class AdminNavComposer {

    public function compose(View $view) {
        $languages = Language::where('is_used', 1)->get();
        $view->with('_languages', $languages);
        $sites = Site::all();
        $view->with('_sites', $sites);
    }

}
