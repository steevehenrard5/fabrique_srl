<?php namespace App\Http\ViewComposers;

use App\Models\_CMS\Configuration;
use App\Models\_CMS\Language;
use App\Models\_CMS\Menu;
use App;
use Illuminate\View\View;

class FrontComposer {

    public function compose(View $view) {
        $language = Language::where('code', App::getLocale())->first();
        $languages = Language::where('is_used', 1)->get()->mapWithKeys(function($item) {
            return [$item['code'] => $item];
        });

        $configuration = Configuration::where('site_id', session('switched_site.id'))->first();

        $global_menus = Menu::with('f_items.page.f_attributes',
                'f_items.sub_menu.menu.f_items.page.f_attributes',
                'f_items.sub_menu.menu.f_items.sub_menu.menu.f_items.page.f_attributes',
                'f_items.sub_menu.menu.f_items.sub_menu.menu.f_items.sub_menu.menu.f_items.page.f_attributes'
            )
            ->where('is_everywhere', 1)
            ->where('site_id', session('switched_site.id'))->get();

        $view->with(compact('language', 'languages', 'configuration', 'global_menus'));
    }

}
