<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class MenuSubMenu extends Model {

    protected $table = 'menus_submenus';

    public function menu() {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function item() {
        return $this->belongsTo(MenuItem::class, 'parent_id');
    }

}