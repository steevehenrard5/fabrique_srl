<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class PageMenu extends Model {

    protected $table = 'pages_menus';

    public function menu() {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

}
