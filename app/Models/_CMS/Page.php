<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;
use App;

class Page extends Model {

    protected $table = 'pages';

    /* BACKEND */

    public function attributes() {
        return $this->hasOne(PageAttribute::class, 'page_id')->where('lang', session('switched_language'));
    }

    public function menus() {
        return $this->hasMany(PageMenu::class, 'page_id');
    }

    public function contents() {
        return $this->hasMany(Content::class, 'page_id')->orderBy('order')->where('lang', session('switched_language'));
    }

    public function logs() {
        return$this->hasMany(PageLog::class, 'page_id');
    }

    public function isTranslated($language) {
        return PageAttribute::where('page_id', $this->id)->where('lang', $language)->first();
    }

    /* FRONT */

    public function f_attributes() {
        return $this->hasOne(PageAttribute::class, 'page_id')->where('lang', App::getLocale());
    }

    public function f_contents() {
        return $this->hasMany(Content::class, 'page_id')->orderBy('order')->where('lang', App::getLocale())->where('is_draft', 0);
    }

}
