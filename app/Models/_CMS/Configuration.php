<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model {

    protected $table = 'configurations';

    public function getLogoFooter() {
        return asset('/assets/front/img/' . $this->logo_website_footer);
    }

}
