<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;
use App;

class Menu extends Model {

    protected $table = 'menus';

    /* BACKEND */

    public function items() {
        return $this->hasMany(MenuItem::class, 'menu_id')->orderBy('order')->where('lang', session('switched_language'));
    }

    public function is_child() {
        return $this->hasOne(MenuSubMenu::class, 'menu_id');
    }

    /* FRONT */

    public function f_items() {
        return $this->hasMany(MenuItem::class, 'menu_id')->orderBy('order')->where('lang', App::getLocale());
    }

}
