<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model {

    protected $table = 'variables';

    public function siblings() {
        return $this
            ->hasMany(Variable::class, 'reference', 'reference')
            ->where('lang', '!=', $this->lang);
    }

}
