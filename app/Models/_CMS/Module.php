<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

    protected $table = 'modules';

    public function partials() {
        return $this->hasMany(Partial::class, 'module_id');
    }

}
