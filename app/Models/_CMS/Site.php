<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

    protected $table = 'sites';

    public function configuration() {
        return $this->hasOne(Configuration::class, 'site_id');
    }

}
