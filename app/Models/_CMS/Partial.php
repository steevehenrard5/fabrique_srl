<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Partial extends Model {

    protected $table = 'partials';

    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

}
