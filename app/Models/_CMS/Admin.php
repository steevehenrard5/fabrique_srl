<?php namespace App\Models\_CMS;

use App\Models\People as People;

class Admin extends People {

    protected $table = 'admins';

    public function getAvatar() {
        if($this->avatar != '') {
            return asset('/assets/admin/img/users/avatars/' . $this->avatar);
        } else {
            return asset('/assets/admin/img/users/avatars/default.jpg');
        }
    }

    public function canAccess($reference, $for_select = false) {
        $permissions = json_decode($this->permissions);
        if(!$permissions || empty($permissions))
            return !$for_select;

        return in_array($reference, $permissions);
    }

}
