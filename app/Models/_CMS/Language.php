<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {
    protected $table = 'languages';
}
