<?php namespace App\Models\_CMS;

use App\Helpers\_CMS\FileHelper;
use Illuminate\Database\Eloquent\Model;

class Media extends Model {

    protected $table = 'medias';

    public function user() {
        return $this->belongsTo(Admin::class, 'user_id');
    }

    public function getMedia() {
        return asset('uploads/medias/' . $this->file_name);
    }

    public function isImage() {
        $allowed = ['png', 'jpg', 'jpeg', 'gif', 'svg'];
        $type = $this->file_extension;
        return in_array($type, $allowed);
    }

    public function getFormattedSize_() {
        return FileHelper::getFormattedSize($this->file_size);
    }

    public function getIcon_() {
        return FileHelper::getIcon($this->file_extension);
    }

}
