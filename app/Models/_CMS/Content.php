<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Content extends Model {

    protected $table = 'contents';

    public function type() {
        return $this->belongsTo(ContentType::class, 'type_id');
    }

    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function draft() {
        return $this->hasOne(ContentDraft::class, 'content_id');
    }

    public function targeted_page() {
        return $this->belongsTo(Page::class, 'content_page_id');
    }

    public function module_item() {
        return $this->module()->find($this->module_item_id);
    }

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

}
