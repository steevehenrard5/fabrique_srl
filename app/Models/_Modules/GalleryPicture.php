<?php namespace App\Models\_Modules;

class GalleryPicture extends _Module {

    protected $table = '_mod_galleries_pictures';

    public function getPath() {
        return asset('uploads/img/modules/galleries/' . $this->photo);
    }

}
