<?php namespace App\Models\_Modules;

use App\Helpers\_CMS\PlaceholderHelper;

class Team extends _Module {

    protected $table = '_mod_team';

    public function getPicture() {
        if($this->picture != '') {
            return asset('uploads/img/modules/team/' . $this->picture);
        }
        return '';
    }

}
