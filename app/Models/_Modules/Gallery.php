<?php namespace App\Models\_Modules;

use App\Models\_CMS\Partial;

class Gallery extends _Module {

    protected $table = '_mod_galleries';

    public function pictures() {
        return $this->hasMany(GalleryPicture::class, 'gallery_id')->orderBy('order');
    }

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

}
