<?php namespace App\Models\_Modules;

class SliderPicture extends _Module {

    protected $table = '_mod_sliders_pictures';

    public function getPath() {
        return asset('uploads/img/modules/sliders/' . $this->photo);
    }

}
