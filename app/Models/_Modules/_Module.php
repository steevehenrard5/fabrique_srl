<?php namespace App\Models\_Modules;

use Illuminate\Database\Eloquent\Model;
use App\Models\_CMS\Admin;
use App;

class _Module extends Model {

    public function creator() {
        return $this->belongsTo(Admin::class, 'creator_id');
    }

    public function getLinkableContents($order, $sorting = 'ASC', $is_front = false, $take = false) {
        $query = $this->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('is_draft', 0)
            ->where('lang', $is_front ? App::getLocale() : session('switched_language'))
            ->where('site_id', session('switched_site.id'))
            ->orderBy($order, $sorting);
            if($take) return $query->take($take)->get();
            else return $query->get();
    }

    public function getFrontContent($content) {
        if($content->module_order_by) {
            $parts = explode('|', $content->module_order_by);
            $order = $parts[0];
            $sorting = $parts[1];
        } else {
            $order = $content->module->targetable_by;
        }
        if($content->module_item_id) {
            return $this->find($content->module_item_id);
        }
        if($content->module_all_items) {
            return $this->getLinkableContents($order, $sorting, true);
        }
        if($content->module_number_items) {
            return $this->getLinkableContents($order, $sorting, true, $content->module_number_items);
        }
        return null;
    }

    public function getDetailsContent($module, $id_or_slug) {
        $query = $this->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('is_draft', 0)
            ->where('lang', App::getLocale())
            ->where('site_id', session('switched_site.id'));
        if($module->is_targetable_by_slug) $item = $query->where('slug', $id_or_slug)->first();
        else $item = $query->where('id', $id_or_slug)->first();
        return $item;
    }

}
