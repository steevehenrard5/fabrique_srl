<?php namespace App\Models\_Modules;

use App\Models\_CMS\Partial;

class Slider extends _Module {

    protected $table = '_mod_sliders';

    public function pictures() {
        return $this->hasMany(SliderPicture::class, 'slider_id')->orderBy('order');
    }

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

}
