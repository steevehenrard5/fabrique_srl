<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class databaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup of the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Create a snapshot of the database
        Artisan::call('snapshot:create');

        // Get all files from folder
        $path = Storage::disk('snapshots')->getAdapter()->getPathPrefix();
        $files = scandir($path);

        // Zip all files
        foreach($files as $file){
            if(!in_array($file, ['.', '..', '.DS_Store'])){
                self::zipFile($file, $path);
            }
        }

        self::cleanFiles($files, $path);

        //Log::info('Database backup done...');
    }

    public static function zipFile($file, $path){
        $exp = explode('.', $file);
        $extension = $exp[count($exp)-1];
        $file_name = $exp[0];

        if($extension == "sql") {
            $zip = new \ZipArchive();

            $DelFilePath = $file_name.".zip";

            if(file_exists($path.$DelFilePath)) {
                unlink ($path.$DelFilePath);
            }
            if ($zip->open($path.$DelFilePath, \ZipArchive::CREATE) != TRUE) {
                die ("Could not open archive");
            }
            $zip->addFromString(basename($file),  file_get_contents($path.$file));
            $zip->close();

            if(file_exists($path.$file)) {
                unlink ($path.$file);
            }
        }

    }

    public function cleanFiles($files, $path){
        $counter = 0;
        // Remove older files when we have 10 backups
        if(count($files)>12) {
            foreach ($files as $file) {
                if(!in_array($file, ['.', '..', '.DS_Store'])) {
                    if ($counter == 0) unlink($path . $file);
                    $counter++;
                }
            }
        }
    }
}
