<?php

use Illuminate\Database\Seeder;

class ModGalleriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Galleries',
            'icon' => 'image',
            'reference' => 'galleries',
            'class_name' => 'Gallery',
            'is_linkable' => 1,
            'is_multiple' => 0,
            'is_block' => 0,
            'is_targetable_by_slug' => 0,
            'targetable_by' => 'name',
            'orderable_by' => 'created_at',
        ]);
    }
}
