<?php

use Illuminate\Database\Seeder;

class ModArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'News',
            'icon' => 'write',
            'reference' => 'articles',
            'class_name' => 'Article',
            'is_linkable' => 1,
            'is_multiple' => 1,
            'is_block' => 1,
            'is_targetable_by_slug' => 1,
            'targetable_by' => 'title',
            'orderable_by' => 'custom_date,created_at',
        ]);

        DB::table('_mod_articles_types')->insert([
            'name' => 'Article (default)',
            'reference' => 'default',
        ]);
        DB::table('_mod_articles_types')->insert([
            'name' => 'File',
            'reference' => 'file',
        ]);
        DB::table('_mod_articles_types')->insert([
            'name' => 'Video',
            'reference' => 'video',
        ]);
    }
}
