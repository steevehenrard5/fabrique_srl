<?php

use Illuminate\Database\Seeder;

class ModQuotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Quotes',
            'icon' => 'comment',
            'reference' => 'quotes',
            'class_name' => 'Quote',
            'is_linkable' => 1,
            'is_multiple' => 0,
            'is_block' => 0,
            'is_targetable_by_slug' => 0,
            'targetable_by' => 'name',
            'orderable_by' => 'created_at',
        ]);
    }
}
