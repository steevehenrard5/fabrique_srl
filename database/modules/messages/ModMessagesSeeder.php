<?php

use Illuminate\Database\Seeder;

class ModMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Messages',
            'icon' => 'comment-alt',
            'reference' => 'contact_messages',
            'class_name' => 'ContactMessage',
        ]);
    }
}
