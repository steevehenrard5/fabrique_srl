<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_sliders')) {
            Schema::create('_mod_sliders', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->integer('creator_id')->nullable()->unsigned();
                $table->index('creator_id');
                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('call_to_action_text')->nullable();
                $table->string('call_to_action_anchor')->nullable();
            });
        }

        if(!Schema::hasTable('_mod_sliders_pictures')) {
            Schema::create('_mod_sliders_pictures', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('slider_id')->nullable()->unsigned();
                $table->index('slider_id');
                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->string('photo')->nullable();
                $table->string('title')->nullable();
                $table->string('head_title')->nullable();
                $table->string('description')->nullable();
                $table->string('call_to_action_text')->nullable();
                $table->string('call_to_action_type')->nullable();
                $table->string('custom_url')->nullable();
                $table->integer('order')->unsigned()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_sliders');
        Schema::dropIfExists('_mod_sliders_pictures');
    }
}
