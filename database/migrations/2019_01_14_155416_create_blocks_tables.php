<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_content_blocks')) {
            Schema::create('_mod_content_blocks', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('image')->nullable();
                $table->tinyInteger('is_video')->unsigned()->nullable()->default(0);
                $table->string('title')->nullable();
                $table->string('head_title')->nullable();
                $table->text('description')->nullable();
                $table->string('call_to_action_text')->nullable();
                $table->string('custom_url')->nullable();
                $table->string('video_title')->nullable();
                $table->string('video_time')->nullable();
                $table->string('video_url')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_content_blocks');
    }
}
