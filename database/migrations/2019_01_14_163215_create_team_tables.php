<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_team')) {
            Schema::create('_mod_team', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->text('quote')->nullable();
                $table->text('biography')->nullable();
                $table->string('function')->nullable();
                $table->string('email')->nullable();
                $table->string('phone')->nullable();
                $table->string('facebook')->nullable();
                $table->string('twitter')->nullable();
                $table->string('linkedin')->nullable();
                $table->string('picture')->nullable();
                $table->string('tags')->nullable();
                $table->integer('order')->unsigned()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_team');
    }
}
