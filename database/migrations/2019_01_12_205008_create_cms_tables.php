<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTables extends Migration {

    public function up() {
        // -- Admins table
        if(!Schema::hasTable('admins')) {
            Schema::create('admins', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('username')->nullable()->unique();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('email')->nullable()->unique();
                $table->tinyInteger('is_support')->default(0)->nullable()->unsigned();
                $table->tinyInteger('g2fa_confirmed')->default(0)->nullable()->unsigned();
                $table->integer('level')->unsigned()->nullable()->default(99);
                $table->text('permissions')->nullable();
                $table->string('password')->nullable();
                $table->string('avatar')->nullable();
                $table->string('phone')->nullable();
                $table->string('g2fa_secret')->nullable();
                $table->string('remember_token', 100)->nullable();
            });
        }

        // -- Configurations table
        if(!Schema::hasTable('configurations')) {
            Schema::create('configurations', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->string('email_contact')->nullable();
                $table->string('phone_contact')->nullable();
                $table->string('logo_website_default')->nullable();
                $table->string('logo_website_header')->nullable();
                $table->string('logo_website_footer')->nullable();
                $table->text('footer_copyright')->nullable();
                $table->text('address_contact')->nullable();
                $table->string('facebook')->nullable();
                $table->string('twitter')->nullable();
                $table->string('linkedin')->nullable();
                $table->string('instagram')->nullable();
                $table->string('youtube')->nullable();
            });
        }

        // -- Contents table
        if(!Schema::hasTable('contents')) {
            Schema::create('contents', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->string('head_title')->nullable();
                $table->string('title')->nullable();
                $table->char('lang', 5)->nullable();
                $table->integer('type_id')->nullable()->unsigned();
                $table->index('type_id');
                $table->integer('module_id')->nullable()->unsigned();
                $table->index('module_id');
                $table->string('reference')->nullable();
                $table->string('css_class')->nullable();
                $table->integer('order')->nullable()->unsigned();
                $table->text('content')->nullable();
                $table->integer('module_item_id')->nullable()->unsigned();
                $table->index('module_item_id');
                $table->integer('content_page_id')->nullable()->unsigned();
                $table->index('content_page_id');
                $table->tinyInteger('module_all_items')->unsigned()->nullable();
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('module_number_items')->unsigned()->nullable();
                $table->string('module_order_by')->nullable();
                $table->string('content_custom_url')->nullable();
            });
        }

        // -- Contents drafts table
        if(!Schema::hasTable('contents_drafts')) {
            Schema::create('contents_drafts', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('reference')->nullable();
                $table->string('css_class')->nullable();
                $table->text('content')->nullable();
                $table->integer('content_page_id')->unsigned()->nullable();
                $table->string('content_custom_url')->nullable();
                $table->integer('content_id')->nullable()->unsigned();
                $table->index('content_id');
            });
        }

        // -- Contents types table
        if(!Schema::hasTable('contents_types')) {
            Schema::create('contents_types', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->string('icon')->nullable();
                $table->string('reference')->nullable();
                $table->tinyInteger('available')->unsigned()->nullable()->default(1);
                $table->string('input_type')->nullable();
                $table->string('input_name')->nullable();
                $table->string('input_class')->nullable();
                $table->string('input_placeholder')->nullable();
            });
        }

        // -- Languages table
        if(!Schema::hasTable('languages')) {
            Schema::create('languages', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->char('code', 5)->nullable();
                $table->tinyInteger('is_used')->unsigned()->nullable();
                $table->tinyInteger('default_backend')->unsigned()->nullable();
                $table->tinyInteger('default_frontend')->unsigned()->nullable();
            });
        }

        // -- Menus table
        if(!Schema::hasTable('menus')) {
            Schema::create('menus', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->string('reference')->nullable();
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->tinyInteger('is_everywhere')->unsigned()->nullable()->default(0);
            });
        }

        // -- Menus items table
        if(!Schema::hasTable('menus_items')) {
            Schema::create('menus_items', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('menu_id')->nullable()->unsigned();
                $table->index('menu_id');
                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->string('page_anchor')->nullable();
                $table->string('custom_url')->nullable();
                $table->string('title')->nullable();
                $table->tinyInteger('is_void')->unsigned()->nullable()->default(0);
                $table->char('lang', 5)->nullable();
                $table->integer('order')->nullable()->unsigned();
            });
        }

        // -- Menus sub_menus table
        if(!Schema::hasTable('menus_submenus')) {
            Schema::create('menus_submenus', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('menu_id')->nullable()->unsigned();
                $table->index('menu_id');
                $table->integer('parent_id')->nullable()->unsigned();
                $table->index('parent_id');
            });
        }

        // -- Modules table
        if(!Schema::hasTable('modules')) {
            Schema::create('modules', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->string('description')->nullable();
                $table->string('icon')->nullable();
                $table->string('reference')->nullable();
                $table->string('class_name')->nullable();
                $table->string('targetable_by')->nullable();
                $table->string('orderable_by')->nullable();
                $table->tinyInteger('is_active')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_linkable')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_multiple')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_block')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_targetable_by_slug')->unsigned()->nullable()->default(0);
            });
        }

        // -- Pages table
        if(!Schema::hasTable('pages')) {
            Schema::create('pages', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->tinyInteger('level_access')->unsigned()->nullable()->default(0);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->string('directory')->nullable();
                $table->string('body_class')->nullable();
                $table->tinyInteger('is_active')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_home')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_secured')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_guest')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
            });
        }

        // -- Pages attributes table
        if(!Schema::hasTable('pages_attributes')) {
            Schema::create('pages_attributes', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->char('lang', 5)->nullable();
                $table->string('title')->nullable();
                $table->string('image')->nullable();
                $table->string('url')->nullable();
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->text('description')->nullable();
                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->integer('creator_id')->nullable()->unsigned();
                $table->index('creator_id');
            });
        }

        // -- Pages menus table
        if(!Schema::hasTable('pages_menus')) {
            Schema::create('pages_menus', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->integer('menu_id')->nullable()->unsigned();
                $table->index('menu_id');
            });
        }

        // -- Partials
        if(!Schema::hasTable('partials')) {
            Schema::create('partials', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('module_id')->nullable()->unsigned();
                $table->index('module_id');
                $table->string('name')->nullable();
                $table->string('description')->nullable();
                $table->string('partial_path')->nullable();
                $table->string('side')->nullable();
                $table->string('body_class')->nullable();
                $table->tinyInteger('is_details_partial')->unsigned()->nullable()->default(0);
            });
        }

        // -- Sites table
        if(!Schema::hasTable('sites')) {
            Schema::create('sites', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->tinyInteger('is_default')->unsigned()->nullable()->default(0);
            });
        }

        // -- Users table
        if(!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('username')->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('avatar')->nullable();
                $table->string('email')->nullable()->unique();
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->string('password')->nullable();
                $table->string('remember_token', 100)->nullable();
                $table->tinyInteger('gender')->unsigned()->nullable();
                $table->date('birth_date')->nullable();
                $table->string('street')->nullable();
                $table->string('cp')->nullable();
                $table->string('city')->nullable();
                $table->string('country')->nullable();
                $table->string('formatted_address')->nullable();
                $table->text('comments')->nullable();
            });
        }

        // -- Variables table
        if(!Schema::hasTable('variables')) {
            Schema::create('variables', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->char('lang', 5)->nullable();
                $table->string('reference')->nullable();
                $table->string('value')->nullable();
            });
        }

        // -- Medias table
        if(!Schema::hasTable('medias')) {
            Schema::create('medias', function(Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('file_name')->nullable();
                $table->string('file_extension')->nullable();
                $table->integer('file_size')->nullable()->unsigned();
                $table->integer('user_id')->nullable()->unsigned();
                $table->index('user_id');
            });
        }
    }

    public function down() {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('configurations');
        Schema::dropIfExists('contents');
        Schema::dropIfExists('contents_drafts');
        Schema::dropIfExists('contents_types');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('menus');
        Schema::dropIfExists('menus_items');
        Schema::dropIfExists('menus_submenus');
        Schema::dropIfExists('modules');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('pages_attributes');
        Schema::dropIfExists('pages_menus');
        Schema::dropIfExists('partials');
        Schema::dropIfExists('sites');
        Schema::dropIfExists('users');
        Schema::dropIfExists('variables');
        Schema::dropIfExists('medias');
    }

}
