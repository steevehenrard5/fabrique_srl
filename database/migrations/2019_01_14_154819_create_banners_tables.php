<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_banners')) {
            Schema::create('_mod_banners', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->string('name')->nullable();
                $table->string('image')->nullable();
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->char('lang', 5)->nullable();
            });
        }

        if(!Schema::hasTable('_mod_banners_links')) {
            Schema::create('_mod_banners_links', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('banner_id')->nullable()->unsigned();
                $table->index('banner_id');
                $table->integer('page_id')->nullable()->unsigned();
                $table->index('page_id');
                $table->string('call_to_action_text')->nullable();
                $table->string('custom_url')->nullable();
                $table->string('video_url')->nullable();
                $table->string('type')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_banners');
        Schema::dropIfExists('_mod_banners_links');
    }
}
