<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CMSSeeder extends Seeder {

    public function run() {
        // -- Languages
        DB::table('languages')->insert([
            'name' => 'English',
            'code' => 'en',
            'is_used' => 1,
            'default_backend' => 1,
            'default_frontend' => 1,
        ]);
        DB::table('languages')->insert([
            'name' => 'Français',
            'code' => 'fr',
            'is_used' => 0,
            'default_backend' => 0,
            'default_frontend' => 0,
        ]);
        DB::table('languages')->insert([
            'name' => 'Nederlands',
            'code' => 'nl',
            'is_used' => 0,
            'default_backend' => 0,
            'default_frontend' => 0,
        ]);

        // Contents types
        DB::table('contents_types')->insert([
            'name' => 'Text editor (wysiwyg)',
            'icon' => 'align-left',
            'reference' => 'input-text',
            'available' => 1,
            'input_type' => 'textarea',
            'input_name' => 'content',
            'input_class' => 'editor-cms form-control',
            'input_placeholder' => '',
        ]);
        DB::table('contents_types')->insert([
            'name' => 'Image',
            'icon' => 'picture-o',
            'reference' => 'input-image',
            'available' => 0,
            'input_type' => 'file',
            'input_name' => 'image',
            'input_class' => 'form-control',
            'input_placeholder' => '',
        ]);
    }

}
