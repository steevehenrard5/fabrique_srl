<?php
    Route::group(['prefix' => 'cms/setup'], function() {

        // -- Setup
        Route::get('/', '_CMS\MainController@setup')->name('cms.setup');
        Route::post('/step/database', '_CMS\MainController@postDatabaseSetup')->name('cms.setup.database.post');
        Route::post('/step/site', '_CMS\MainController@postSiteSetup')->name('cms.setup.site.post');
        Route::post('/step/config', '_CMS\MainController@postConfigSetup')->name('cms.setup.config.post');

    });

    Route::group(['prefix' => config('_CMS._global.prefix_backend')], function () {

        // -- Authentication
        Route::get('/login', '_CMS\DashboardController@getLoginForm')->name('admin.login');
        Route::post('/login', '_CMS\DashboardController@login');

        Route::group(['middleware' => 'admin_auth'], function() {
            /* Google 2FA */
            Route::get('/google/2fa', '_CMS\AdminsController@getGoogle2FAForm')->name('admin.login.google2fa');
            Route::post('/google/2fa/check', '_CMS\AdminsController@checkGoogle2FAForm')->name('admin.login.google2fa.check');

            Route::group(['middleware' => 'google_auth'], function() {
                /* Global */
                Route::get('/', '_CMS\DashboardController@index')->name('admin.dashboard');
                Route::get('/logout', '_CMS\DashboardController@logout')->name('admin.logout');
                Route::get('/language/switch/{language}', '_CMS\DashboardController@switchLanguage')->name('admin.language.switch');
                Route::get('/site/switch/{id}', '_CMS\DashboardController@switchSite')->name('admin.site.switch');
                Route::get('/flush', '_CMS\DashboardController@flushSession')->name('admin.session.flush');

                /* Pages */
                Route::group(['middleware' => 'can_access:pages'], function() {
                    Route::get('/pages', '_CMS\PagesController@index')->name('admin.pages');
                    Route::get('/pages/{id}', '_CMS\PagesController@edit')->name('admin.pages.edit');
                    Route::get('/pages/search/clear', '_CMS\PagesController@clear')->name('admin.pages.search.clear');
                    Route::get('/pages/create', '_CMS\PagesController@add')->name('admin.pages.create');
                    Route::get('/pages/{id}/delete', '_CMS\PagesController@delete')->name('admin.pages.delete');
                    Route::get('/pages/{id}/restore', '_CMS\PagesController@restore')->name('admin.pages.restore');
                    Route::get('/pages/{id}/destroy', '_CMS\PagesController@destroy')->name('admin.pages.destroy');
                    Route::get('/pages/{id}/active/switch', '_CMS\PagesController@switchActiveState')->name('admin.pages.active.switch');
                    Route::get('/pages/content/{id}/remove', '_CMS\PagesController@removeContent')->name('admin.pages.content.remove');
                    Route::get('/pages/content/{id}/order/up', '_CMS\PagesController@upContent')->name('admin.pages.content.up');
                    Route::get('/pages/content/{id}/order/down', '_CMS\PagesController@downContent')->name('admin.pages.content.down');
                    Route::get('/pages/attributes/duplicate/{id_page}/{code}', '_CMS\PagesController@duplicateAttributes')->name('admin.pages.attributes.duplicate');
                    Route::get('/pages/duplicated/delete/{id}', '_CMS\PagesController@deleteTranslated')->name('admin.pages.translated.delete');
                    Route::get('/pages/duplicated/edit/{id}', '_CMS\PagesController@editTranslated')->name('admin.pages.translated.edit');
                    Route::get('/pages/content/{id}/publish', '_CMS\PagesController@publishContent')->name('admin.pages.content.publish');
                    Route::get('/pages/content/{id}/un_publish', '_CMS\PagesController@unPublishContent')->name('admin.pages.content.un_publish');

                    Route::post('/pages', '_CMS\PagesController@search');
                    Route::post('/pages/create', '_CMS\PagesController@create');
                    Route::post('/pages/content/add', '_CMS\PagesController@addContent')->name('admin.pages.content.add');
                    Route::post('/pages/module/add', '_CMS\PagesController@addModule')->name('admin.pages.module.add');
                    Route::post('/pages/draft/update', '_CMS\PagesController@updateDraft')->name('admin.pages.draft.update');
                    Route::post('/pages/content/update', '_CMS\PagesController@updateContent')->name('admin.pages.content.update');
                    Route::post('/pages/module/update', '_CMS\PagesController@updateModule')->name('admin.pages.module.update');
                    Route::post('/pages/{id}', '_CMS\PagesController@update');
                });

                /* Menus */
                Route::group(['middleware' => 'can_access:menus'], function() {
                    Route::get('/menus', '_CMS\MenusController@index')->name('admin.menus');
                    Route::get('/menus/{id}', '_CMS\MenusController@edit')->name('admin.menus.edit');
                    Route::get('/menus/create', '_CMS\MenusController@add')->name('admin.menus.create');
                    Route::get('/menus/{id}/delete', '_CMS\MenusController@delete')->name('admin.menus.delete');
                    Route::get('/menus/item/{id}/order/up', '_CMS\MenusController@upItem')->name('admin.menus.item.up');
                    Route::get('/menus/item/{id}/order/down', '_CMS\MenusController@downItem')->name('admin.menus.item.down');
                    Route::get('/menus/item/{id}/delete', '_CMS\MenusController@deleteItem')->name('admin.menus.item.delete');
                    Route::get('/menus/item/sub/{id}/delete', '_CMS\MenusController@deleteSubMenu')->name('admin.menus.item.sub.delete');

                    Route::post('/menus/create', '_CMS\MenusController@create');
                    Route::post('/menus/{id}', '_CMS\MenusController@update');
                    Route::post('/menus/{id}/item/add', '_CMS\MenusController@addItem')->name('admin.menus.item.add');
                    Route::post('/menus/item/sub/add', '_CMS\MenusController@addSubMenu')->name('admin.menus.item.sub.add');
                    Route::post('/menus/items/update', '_CMS\MenusController@updateItems')->name('admin.menus.items.update');
                    Route::post('/menus/item/build', '_CMS\MenusController@buildItem')->name('admin.menus.item.build.ajax');
                });

                /* Users */
                Route::get('/users', '_CMS\UsersController@index')->name('admin.users');
                Route::get('/users/{id}', '_CMS\UsersController@edit')->name('admin.users.edit');
                Route::get('/users/create', '_CMS\UsersController@add')->name('admin.users.create');
                Route::get('/users/{id}/delete', '_CMS\UsersController@delete')->name('admin.users.delete');
                Route::get('/users/{id}/restore', '_CMS\UsersController@restore')->name('admin.users.restore');
                Route::get('/users/{id}/destroy', '_CMS\UsersController@destroy')->name('admin.users.destroy');
                Route::get('/users/{id}/log/delete', '_CMS\UsersController@deleteLog')->name('admin.users.log.delete');

                Route::post('/users/create', '_CMS\UsersController@create');
                Route::post('/users/{id}', '_CMS\UsersController@update');

                /* Operators */
                Route::get('/operators', '_CMS\AdminsController@index')->name('admin.operators');
                Route::get('/operators/{id}', '_CMS\AdminsController@edit')->name('admin.operators.edit');
                Route::get('/operators/create', '_CMS\AdminsController@add')->name('admin.operators.create');
                Route::get('/operators/{id}/delete', '_CMS\AdminsController@delete')->name('admin.operators.delete');

                Route::post('/operators/create', '_CMS\AdminsController@create');
                Route::post('/operators/{id}', '_CMS\AdminsController@update');

                /* Groups */
                Route::get('/groups', '_CMS\GroupsController@index')->name('admin.groups');

                /* Languages */
                Route::get('/languages', '_CMS\LanguagesController@index')->name('admin.languages');
                Route::get('/languages/{id}', '_CMS\LanguagesController@edit')->name('admin.languages.edit');
                Route::get('/languages/create', '_CMS\LanguagesController@add')->name('admin.languages.create');
                Route::get('/languages/{id}/delete', '_CMS\LanguagesController@delete')->name('admin.languages.delete');
                Route::get('/languages/{id}/active/switch', '_CMS\LanguagesController@switchActiveState')->name('admin.languages.active.switch');

                Route::post('/languages/create', '_CMS\LanguagesController@create');
                Route::post('/languages/{id}', '_CMS\LanguagesController@update');

                /* Variables */
                Route::get('/variables/{id}', '_CMS\VariablesController@edit')->name('admin.variables.edit');
                Route::get('/variables/create', '_CMS\VariablesController@add')->name('admin.variables.create');
                Route::get('/variables/{id}/delete', '_CMS\VariablesController@delete')->name('admin.variables.delete');
                Route::get('/variables/sync/to/{code}', '_CMS\VariablesController@sync')->name('admin.variables.sync');

                Route::post('/variables/create', '_CMS\VariablesController@create');
                Route::post('/variables/{id}', '_CMS\VariablesController@update');

                /* Sites */
                Route::get('/sites', '_CMS\SitesController@index')->name('admin.sites');

                /* Configuration */
                Route::get('/configuration', '_CMS\ConfigurationController@index')->name('admin.configuration');
                Route::get('/configuration/logo/remove/{reference}', '_CMS\ConfigurationController@removeLogo')->name('admin.configuration.logo.remove');

                Route::post('/configuration', '_CMS\ConfigurationController@saveForm');

                /* Modules */
                Route::get('/modules', '_CMS\ModulesController@index')->name('admin.modules');
                Route::get('/modules/{id}', '_CMS\ModulesController@edit')->name('admin.modules.edit');
                Route::get('/modules/create', '_CMS\ModulesController@add')->name('admin.modules.create');
                Route::get('/modules/{id}/delete', '_CMS\ModulesController@delete')->name('admin.modules.delete');
                Route::get('/modules/{id}/active/switch', '_CMS\ModulesController@switchActiveState')->name('admin.modules.active.switch');

                Route::post('/modules/create', '_CMS\ModulesController@create');
                Route::post('/modules/{id}', '_CMS\ModulesController@update');

                /* Partials */
                Route::get('/partials', '_CMS\PartialsController@index')->name('admin.partials');
                Route::get('/partials/{id}', '_CMS\PartialsController@edit')->name('admin.partials.edit');
                Route::get('/partials/create', '_CMS\PartialsController@add')->name('admin.partials.create');
                Route::get('/partials/{id}/delete', '_CMS\PartialsController@delete')->name('admin.partials.delete');

                Route::post('/partials/create', '_CMS\PartialsController@create');
                Route::post('/partials/{id}', '_CMS\PartialsController@update');

                /* Medias */
                Route::group(['middleware' => 'can_access:medias'], function() {
                    Route::get('medias', '_CMS\MediasController@index')->name('admin.medias');
                    Route::get('medias/{id}/delete', '_CMS\MediasController@delete')->name('admin.medias.delete');
                    Route::post('medias/upload', '_CMS\MediasController@upload')->name('admin.medias.upload');
                });

            });

        });
    });
