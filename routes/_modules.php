<?php
    /*
     * DEFAULT MODULES
     * */

    Route::group(['prefix' => Config::get('_CMS._global.prefix_backend')], function () {
        Route::group(['middleware' => 'admin_auth'], function() {
            Route::group(['prefix' => 'modules'], function() {

                /* Modules */
                Route::group(['middleware' => 'can_access:articles'], function() {
                    /* Articles */
                    Route::get('/articles', '_Modules\ArticlesController@index')->name('admin.modules.articles');
                    Route::get('/articles/{id}', '_Modules\ArticlesController@edit')->name('admin.modules.articles.edit');
                    Route::get('/articles/create', '_Modules\ArticlesController@add')->name('admin.modules.articles.create');
                    Route::get('/articles/{id}/delete', '_Modules\ArticlesController@delete')->name('admin.modules.articles.delete');
                    Route::get('/articles/{id}/restore', '_Modules\ArticlesController@restore')->name('admin.modules.articles.restore');
                    Route::get('/articles/{id}/destroy', '_Modules\ArticlesController@destroy')->name('admin.modules.articles.destroy');
                    Route::get('/articles/{id}/draft/switch', '_Modules\ArticlesController@switchDraftState')->name('admin.modules.articles.draft.switch');

                    Route::post('/articles/create', '_Modules\ArticlesController@create');
                    Route::post('/articles/{id}', '_Modules\ArticlesController@update');
                    /* Articles Categories */
                    Route::get('/articles/categories', '_Modules\ArticlesController@categories')->name('admin.modules.articles.categories');
                    Route::get('/articles/categories/{id}', '_Modules\ArticlesController@editCategory')->name('admin.modules.articles.categories.edit');
                    Route::get('/articles/categories/create', '_Modules\ArticlesController@addCategory')->name('admin.modules.articles.categories.create');
                    Route::get('/articles/categories/{id}/delete', '_Modules\ArticlesController@deleteCategory')->name('admin.modules.articles.categories.delete');

                    Route::post('/articles/categories/create', '_Modules\ArticlesController@createCategory');
                    Route::post('/articles/categories/{id}', '_Modules\ArticlesController@updateCategory');
                });

                Route::group(['middleware' => 'can_access:team'], function() {
                    /* Team */
                    Route::get('/team', '_Modules\TeamController@index')->name('admin.modules.team');
                    Route::get('/team/{id}', '_Modules\TeamController@edit')->name('admin.modules.team.edit');
                    Route::get('/team/create', '_Modules\TeamController@add')->name('admin.modules.team.create');
                    Route::get('/team/{id}/delete', '_Modules\TeamController@delete')->name('admin.modules.team.delete');
                    Route::get('/team/{id}/restore', '_Modules\TeamController@restore')->name('admin.modules.team.restore');
                    Route::get('/team/{id}/destroy', '_Modules\TeamController@destroy')->name('admin.modules.team.destroy');
                    Route::get('/team/{id}/active/switch', '_Modules\TeamController@switchActiveState')->name('admin.modules.team.active.switch');

                    Route::post('/team/create', '_Modules\TeamController@create');
                    Route::post('/team/{id}', '_Modules\TeamController@update');
                });

                Route::group(['middleware' => 'can_access:galleries'], function() {
                    /* Galleries */
                    Route::get('/galleries', '_Modules\GalleriesController@index')->name('admin.modules.galleries');
                    Route::get('/galleries/{id}', '_Modules\GalleriesController@edit')->name('admin.modules.galleries.edit');
                    Route::get('/galleries/create', '_Modules\GalleriesController@add')->name('admin.modules.galleries.create');
                    Route::get('/galleries/{id}/delete', '_Modules\GalleriesController@delete')->name('admin.modules.galleries.delete');
                    Route::get('/galleries/{id}/restore', '_Modules\GalleriesController@restore')->name('admin.modules.galleries.restore');
                    Route::get('/galleries/{id}/destroy', '_Modules\GalleriesController@destroy')->name('admin.modules.galleries.destroy');
                    Route::get('/galleries/{id}/draft/switch', '_Modules\GalleriesController@switchDraftState')->name('admin.modules.galleries.draft.switch');
                    Route::get('/galleries/picture/{id}/order/up', '_Modules\GalleriesController@upItem')->name('admin.modules.galleries.picture.up');
                    Route::get('/galleries/picture/{id}/order/down', '_Modules\GalleriesController@downItem')->name('admin.modules.galleries.picture.down');
                    Route::get('/galleries/picture/{id}/delete', '_Modules\GalleriesController@deleteItem')->name('admin.modules.galleries.picture.delete');

                    Route::post('/galleries/create', '_Modules\GalleriesController@create');
                    Route::post('/galleries/{id}', '_Modules\GalleriesController@update');
                    Route::post('/galleries/pictures/upload', '_Modules\GalleriesController@uploadPictures')->name('admin.modules.galleries.pictures.upload');
                });

                Route::group(['middleware' => 'can_access:content-blocks'], function() {
                    /* Content Blocks */
                    Route::get('/content-blocks', '_Modules\ContentBlocksController@index')->name('admin.modules.content_blocks');
                    Route::get('/content_blocks/{id}', '_Modules\ContentBlocksController@edit')->name('admin.modules.content_blocks.edit');
                    Route::get('/content_blocks/create', '_Modules\ContentBlocksController@add')->name('admin.modules.content_blocks.create');
                    Route::get('/content_blocks/{id}/delete', '_Modules\ContentBlocksController@delete')->name('admin.modules.content_blocks.delete');
                    Route::get('/content_blocks/{id}/draft/switch', '_Modules\ContentBlocksController@switchDraftState')->name('admin.modules.content_blocks.draft.switch');

                    Route::post('/content_blocks/create_new', '_Modules\ContentBlocksController@create_new')->name('admin.modules.content_blocks.new');
                    Route::post('/content_blocks/create', '_Modules\ContentBlocksController@create');
                    Route::post('/content_blocks/{id}', '_Modules\ContentBlocksController@update');
                });

                Route::group(['middleware' => 'can_access:banners'], function() {
                    /* Banners */
                    Route::get('/banners', '_Modules\BannersController@index')->name('admin.modules.banners');
                    Route::get('/banners/{id}', '_Modules\BannersController@edit')->name('admin.modules.banners.edit');
                    Route::get('/banners/create', '_Modules\BannersController@add')->name('admin.modules.banners.create');
                    Route::get('/banners/{id}/delete', '_Modules\BannersController@delete')->name('admin.modules.banners.delete');
                    Route::get('/banners/{id}/draft/switch', '_Modules\BannersController@switchDraftState')->name('admin.modules.banners.draft.switch');

                    Route::post('/banners/create_new', '_Modules\BannersController@create_new')->name('admin.modules.banners.new');
                    Route::post('/banners/create', '_Modules\BannersController@create');
                    Route::post('/banners/{id}', '_Modules\BannersController@update');
                });

                Route::group(['middleware' => 'can_access:quotes'], function() {
                    /* Quotes */
                    Route::get('/quotes', '_Modules\QuotesController@index')->name('admin.modules.quotes');
                    Route::get('/quotes/{id}', '_Modules\QuotesController@edit')->name('admin.modules.quotes.edit');
                    Route::get('/quotes/create', '_Modules\QuotesController@add')->name('admin.modules.quotes.create');
                    Route::get('/quotes/{id}/delete', '_Modules\QuotesController@delete')->name('admin.modules.quotes.delete');
                    Route::get('/quotes/{id}/draft/switch', '_Modules\QuotesController@switchDraftState')->name('admin.modules.quotes.draft.switch');

                    Route::post('/quotes/create', '_Modules\QuotesController@create');
                    Route::post('/quotes/{id}', '_Modules\QuotesController@update');
                });

                Route::group(['middleware' => 'can_access:sliders'], function() {
                    /* Sliders */
                    Route::get('/sliders', '_Modules\SlidersController@index')->name('admin.modules.sliders');
                    Route::get('/sliders/{id}', '_Modules\SlidersController@edit')->name('admin.modules.sliders.edit');
                    Route::get('/sliders/create', '_Modules\SlidersController@add')->name('admin.modules.sliders.create');
                    Route::get('/sliders/{id}/delete', '_Modules\SlidersController@delete')->name('admin.modules.sliders.delete');
                    Route::get('/sliders/{id}/restore', '_Modules\SlidersController@restore')->name('admin.modules.sliders.restore');
                    Route::get('/sliders/{id}/destroy', '_Modules\SlidersController@destroy')->name('admin.modules.sliders.destroy');
                    Route::get('/sliders/{id}/draft/switch', '_Modules\SlidersController@switchDraftState')->name('admin.modules.sliders.draft.switch');
                    Route::get('/sliders/picture/{id}/order/up', '_Modules\SlidersController@upItem')->name('admin.modules.sliders.picture.up');
                    Route::get('/sliders/picture/{id}/order/down', '_Modules\SlidersController@downItem')->name('admin.modules.sliders.picture.down');
                    Route::get('/sliders/picture/{id}/delete', '_Modules\SlidersController@deleteItem')->name('admin.modules.sliders.picture.delete');

                    Route::post('/sliders/create', '_Modules\SlidersController@create');
                    Route::post('/sliders/{id}', '_Modules\SlidersController@update');
                    Route::post('/sliders/pictures/upload', '_Modules\SlidersController@uploadPictures')->name('admin.modules.sliders.pictures.upload');
                });

                Route::group(['middleware' => 'can_access:contact_messages'], function() {
                    /* Contact Messages */
                    Route::get('/contact_messages', '_Modules\ContactMessagesController@index')->name('admin.modules.contact_messages');
                    Route::get('/contact_messages/{id}', '_Modules\ContactMessagesController@show')->name('admin.modules.contact_messages.show');
                    Route::get('/contact_messages/{id}/delete', '_Modules\ContactMessagesController@delete')->name('admin.modules.contact_messages.delete');
                    Route::get('/contact_messages/{id}/restore', '_Modules\ContactMessagesController@restore')->name('admin.modules.contact_messages.restore');
                    Route::get('/contact_messages/{id}/destroy', '_Modules\ContactMessagesController@destroy')->name('admin.modules.contact_messages.destroy');
                });

            });
        });
    });
