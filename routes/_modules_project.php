<?php
/*
 * CUSTOM PROJECT MODULES
 * */

Route::group(['prefix' => Config::get('_CMS._global.prefix_backend')], function () {
    Route::group(['middleware' => 'admin_auth'], function() {
        Route::group(['prefix' => 'modules'], function() {

            //...

        });
    });
});