<?php

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function() {

    Date::setLocale(App::getLocale());

    Route::get('/{url?}', 'FrontController@getView')->name('front.page');
    Route::get('/{url}/{reference}/{id_or_slug}', 'FrontController@getModuleDetails')->name('front.page.module.details');

    Route::post('/demande-de-devis', '_Modules\SrlController@saveForm')->name('front.page.modules.quote.post');

});