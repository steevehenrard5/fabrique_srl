@extends('cms.setup._master')

@section('_intro')
    <p>
        <strong>{{Config::get('_CMS._global.name')}} {{Config::get('_CMS._global.version')}} - Laravel {{App::VERSION()}}</strong>
        <br><small>The database appears to be missing or incomplete, this will allow you to set it up.</small>
    </p>
@stop

@section('_content')
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <form method="POST" action="{{route('cms.setup.database.post')}}" enctype="multipart/form-data">
        {{csrf_field()}}

        <h5>Database configuration</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Host *</label>
                    <input required type="text" class="form-control" name="host" value="{{old('host') ?: env('DB_HOST', 'localhost')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Database name *</label>
                    <input required type="text" class="form-control" name="db_name" value="{{old('db_name') ?: env('DB_DATABASE', 'cms')}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Username *</label>
                    <input required type="text" class="form-control" name="username" value="{{old('username') ?: env('DB_USERNAME', 'root')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="form-control" name="db_pwd" value="{{old('db_pwd') ?: env('DB_PASSWORD', '')}}">
                </div>
            </div>
        </div>


        <button type="submit" class="btn btn-primary mb-2">Next step <i class="fa fa-arrow-circle-right ml5"></i></button>
    </form>
@stop