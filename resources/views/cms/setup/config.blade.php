@extends('cms.setup._master')

@section('_intro')
    <p>
        <strong>{{Config::get('_CMS._global.name')}} {{Config::get('_CMS._global.version')}} - Laravel {{App::VERSION()}}</strong>
    </p>
@stop

@section('_content')
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <form method="POST" action="{{route('cms.setup.config.post')}}" enctype="multipart/form-data">
        {{csrf_field()}}

        <h5>Basic information <sup><small>optional</small></sup></h5>
        <div class="form-group">
            <label>Choose a default logo for your website</label>
            <input type="file" name="logo_website_default" class="form-control-file">
        </div>
        <div class="form-group">
            <label>Do you have a contact email ?</label>
            <input type="email" class="form-control" name="email_contact" placeholder="info@example.com" value="{{old('email_contact')}}">
        </div>
        <div class="form-group">
            <label>Do you have a contact phone number ?</label>
            <input type="text" class="form-control" name="phone_contact" placeholder="+32 000 00 00 00" value="{{old('phone_contact')}}">
        </div>

        </div>
        <div class="_box_steps">

        <h5>Administrator account <sup><small>required</small></sup></h5>
        <div class="form-group">
            <label>Username *</label>
            <input required type="text" class="form-control" name="username" placeholder="admin" value="{{old('username')}}">
        </div>
        <div class="form-group">
            <label>Email *</label>
            <input required type="email" class="form-control" name="email" placeholder="admin@test.com" value="{{old('email')}}">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Password *</label>
                    <input required type="password" class="form-control" name="password" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Password confirmation *</label>
                    <input required type="password" class="form-control" name="password_confirmation" placeholder="">
                </div>
            </div>
        </div>

        </div>
        <div class="_box_steps">

        <button type="submit" class="btn btn-success">Finalize <i class="fa fa-check ml5"></i></button>
    </form>
@stop