@extends('cms.setup._master')

@section('_intro')
    <p>
        <strong>{{Config::get('_CMS._global.name')}} {{Config::get('_CMS._global.version')}} - Laravel {{App::VERSION()}}</strong>
        <br><small>Thank you for installing our CMS ! We'll guide you trough some configuration steps.</small>
    </p>
@stop

@section('_content')
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <form method="POST" action="{{route('cms.setup.site.post')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label>What's the name of your website ? *</label>
            <input required type="text" class="form-control" placeholder="My awesome website" name="name">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Next step <i class="fa fa-arrow-circle-right ml5"></i></button>
    </form>
@stop