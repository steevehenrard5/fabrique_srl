{!!menu('mainmenu', get_defined_vars())!!}

<header id="navbar-spy" class="header header-transparent">
    <nav class="navbar navbar-expand-lg navbar-light navbar-bordered">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img class="logo-light" src="{{asset('assets/front/img/logo_website_fabrique_a_srl.png')}}" alt="La fabrique à SRL" width="200">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="toogle-inner"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav  ml-auto">
                    <li>
                        <a data-scroll="scrollTo" href="/">Accueil</a>
                    </li>
                    <li>
                        <a href="/news">Actualités</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<section id="hero1" class="hero hero-1 text-center  bg-overlay bg-overlay-dark" style="@yield('banner_style')">
    <div class="bg-section">
        <img src="{{asset('assets/front/images/background/39.jpg')}}" alt="background">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                <div class="hero-content">
                    <h1 class="hero-headline">@yield('header_title')</h1>
                    <p class="hero-desc">
                        @yield('header_desc')
                    </p>
                    @yield('header_cta')
                    <!-- Modal -->
                    <div class="modal fade popup-1" id="popup1" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row split-section">
                                        <div class="col-sm-12 col-sm-12 col-lg-5 split-img-warp">
                                            <div class="split-img">
                                                <div class="bg-section">
                                                    <img src="{{asset('assets/front/images/popup/1.jpg')}}" alt="img" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .col-lg-5 end -->
                                        <div class="col-sm-12 col-sm-12 col-lg-7 offset-lg-5 split-content-warp">
                                            <div class="popup--content">
                                                <div class="heading heading-1 text-left">
                                                    <h2 class="heading-title">Créer votre SRL</h2>
                                                    <p class="heading-desc"><small>Si vous souhaitez utiliser notre ‘Fabrique à SRL’, merci de compléter les quelques informations suivantes, sur base desquelles nous vous contacterons pour établir l’offre qui vous correspond.</small></p>
                                                </div>
                                                <!-- .heading end -->
                                                <div class="popup--form">
                                                    <form method="post" action="{{route('front.page.modules.quote.post')}}" class="mb-0">
                                                        @csrf
                                                        <div class="form-group row">
                                                            <div class="col-6">
                                                                <input type="text" id="namePopUp1" class="form-control" name="lastname" placeholder="Nom" required>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="text" id="namePopUp1" class="form-control" name="firstname" placeholder="Prénom" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-6">
                                                                <input type="email" id="emailPopUp1" class="form-control" name="email" placeholder="Email" required>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="text" id="emailPopUp1" class="form-control" name="address" placeholder="Adresse" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-6">
                                                                <input type="phone" id="phonePopUp1" class="form-control" name="phone" placeholder="Numéro de téléphone" required>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="text" id="phonePopUp1" class="form-control" name="activity" placeholder="Secteur d'activité" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-6">
                                                                <label><small>Date de début souhaitée</small></label>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="date" id="emailPopUp1" class="form-control" name="enterprise_start"  required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-12">
                                                                <textarea name="project_text" rows="3" class="form-control" placeholder="Expliquer en 3 lignes le projet d’entreprise"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-6">
                                                                <label><small>Quand pouvons nous vous appeler?</small></label>
                                                            </div>
                                                            <div class="col-6">
                                                                <input type="datetime-local" id="emailPopUp1" class="form-control" name="call_request" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn--primary btn--block">Demander un devis</button>
                                                        </div>
                                                    </form>
                                                    <div class="popup-alert"></div>
                                                </div>
                                                <!-- .popup-form end -->
                                            </div>
                                            <!-- .popup-content -->
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <!-- .col-lg-7 end -->
                                    </div>
                                    <!-- .row end -->
                                </div>
                                <!-- .Modal-body end -->
                            </div>
                            <!-- .Modal-content end -->
                        </div>
                    </div>
                    <!-- .Modal end -->
                </div>
                <!-- .hero-content end -->
            </div>
            <!-- .col-lg-8 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
