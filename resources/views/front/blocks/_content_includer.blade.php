@foreach($contents as $key => $content)

    @php
        if((isset($exclude_references) && in_array($content->reference, $exclude_references))) continue;
    @endphp

    @if($content->module && $content->items && !$content->module->is_block)
        @php
            $item = $content->items;
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $content, 'key' => $key])
        @endif
    @elseif($content->module && $content->module->is_block && $content->partial)
        @include('front/partials/' . $content->partial->partial_path, ['content' => $content, 'key' => $key])
    @elseif(!$content->module)

        <section id="{{$content->reference}}" class="{{$content->css_class}}">
            @if($content->type->reference == 'input-image')
                <img src="{{asset('uploads/img/pages/' . $content->content)}}" class="img-responsive">
            @else
                {!! $content->content !!}
            @endif
        </section>

    @endif

@endforeach