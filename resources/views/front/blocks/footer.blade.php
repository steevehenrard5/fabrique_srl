<footer id="footer1" class="footer footer-1 text-center">
    <!-- Widget Section
	============================================= -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="widget-links">
                        <ul class="list-unstyled mb-0">
                            <li><a href="https://www.wikipreneurs.be/">Wikipreneurs</a></li>
                            <li><a href="#">Contactez nous</a></li>
                        </ul>
                    </div>
                    <!-- .footer-widget-links end -->
                    <div class="widget-logo">
                        <a href="#">
                            <img src="{{asset('assets/front/img/logo_website_fabrique_a_srl.png')}}" alt="logo" width="200">
                        </a>
                    </div>
                    <!-- .footer-widget-social end -->
                    <div class="widget-social">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    </div>
                    <!-- .footer-widget-social end -->
                    <div class="footer-copyright mb-0">
                        <span> 2021 &copy;  <a href="#">La Fabrique à SRL</a> Tous droits réservés.</span>
                    </div>
                    <!-- .footer-copyright end -->
                </div>
                <!-- .col-lg-12 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </div>
    <!-- .footer-widget end -->
</footer>