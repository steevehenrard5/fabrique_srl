<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="{{$attributes->description ?? ''}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$attributes->title ?? config('_project.name')}}">
    <meta property="og:url" content="/">
    <meta property="og:image" content="{{asset('assets/front/img/logo.png')}}">
    <meta property="og:site_name" content="{{config('_project.name')}}">

    <title>{{$attributes->title ?? config('_project.name')}}@yield('_title')</title>
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/front/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles() !!}
    <link href="{{asset('assets/front/css/external.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet">

    <!-- COLORS -->
    <link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-default.css')}}"/>
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-brown.css')}}"/>-->
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-green.css')}}"/>-->
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-orange.css')}}"/>-->
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-red.css')}}"/>-->
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/themes/theme-yellow.css')}}"/>-->
    <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet">

    @yield('_styles')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{asset('assets/front/js/html5shiv.js')}}"></script>
    <script src="{{asset('assets/front/js/respond.min.js')}}"></script>
</head>

<body class="{{isset($partial_details) ? $partial_details->body_class : (isset($page->body_class) ? $page->body_class : '')}}@yield('_body_class') body-scroll">
@include('front._template')
<!-- JS Global -->
<script src="{{asset('assets/front/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('assets/front/js/plugins.js')}}"></script>
<script src="{{asset('assets/front/js/functions.js')}}"></script>
<script src="{{asset('assets/front/js/plugins/skrollr.min.js')}}"></script>
<script src="{{asset('assets/front/js/plugins/typed.js')}}"></script>
<script src="{{asset('assets/front/js/plugins/tilt.js')}}"></script>
<script src="{{asset('assets/front/js/landing.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}"></script>
{!! \App\Helpers\_CMS\TemplateHelper::scripts() !!}
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('_scripts')
</body>
</html>