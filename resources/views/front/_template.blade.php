<div class="site-container">
    <div id="wrapper" class="wrapper clearfix">
        @include('front.blocks.header')
        <main class="site-content" role="main">
            @yield('_content')
        </main>
        @include('front.blocks.footer')
    </div>
</div>