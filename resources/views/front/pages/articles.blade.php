@extends('front._master')

@section('header_title')
    <span style="font-weight:300">Actualités</span>
@endsection

@section('header_desc')
    Retrouvez ici nos dernières actualités pour ne plus rien manquer !
@endsection

@section('_content')
    <section id="propirties1" class="propirties propirties-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                    <div class="heading heading-6 heading-center mb-70">
                        <h2 class="heading-title">Nos actualités</h2>
                        <p class="heading-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet consequat risus, ac condimentum nisl.</p>
                    </div>
                </div>
            </div>

            @foreach($contents as $key => $content)
                <div class="row">
                    @foreach($content->items as $item)
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="property-item">
                                <div class="property--img">
                                    <a href="{{ route('front.page.module.details', ['news', 'articles', $item->slug]) }}">
                                        <img src="{{asset('assets/front/images/propirties/grid/1.jpg')}}" alt="property">
                                    </a>
                                    <span class="label">Actu</span>
                                </div>
                                <!-- .proprty-img end -->
                                <div class="property--info">
                                    <a href="{{ route('front.page.module.details', ['news', 'articles', $item->slug]) }}">
                                        <h4>{{$item->title}}</h4>
                                    </a>
                                    <p>{{$item->description_short}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
@endsection