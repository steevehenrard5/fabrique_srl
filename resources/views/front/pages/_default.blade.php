@extends('front._master')

@section('_content')
    @if($contents)
        <div class="sections">
            @include('front.blocks._content_includer')
        </div>
    @endif
@stop
