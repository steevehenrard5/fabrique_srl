@extends('front._master')

@section('header_title')
    <span style="font-weight:300">{{$item->title}}</span>
@endsection

@section('header_desc')
    {{$item->description}}
@endsection

<!-- Banner -->
@section('banner_style', 'min-height:0px!important;')

@section('_content')
    <section id="propirties1" class="propirties propirties-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                    <div class="heading heading-6 heading-center mb-70">
                        <h2 class="heading-title">{{$item->title}}</h2>
                    </div>
                </div>
                <div class="col-12">
                    {!! $item->content !!}
                </div>
                <div class="col-12">
                    <small>Publié le <strong>{{$item->created_at->format('d/m/Y à H:i')}}</strong></small>
                </div>

            </div>
        </div>
    </section>
@endsection
