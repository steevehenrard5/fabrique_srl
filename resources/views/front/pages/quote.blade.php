@extends('front._master')

@section('header_title', 'La Fabrique à SRL')

@section('header_desc')
    D’abord bravo, vraiment bravo pour le courage (et le plaisir !) de créer votre propre entreprise ! <br>
    Pour l’avoir fait plusieurs fois nous-même et avoir accompagné des centaines de personnes dans cette magnifique aventure professionnelle, nous savons ce que c’est…
@endsection

@section('header_cta')
    <a href="#" class="btn btn--primary" data-toggle="modal" data-target="#popup1">Formulaire</a>
@endsection

@section('_content')

@endsection