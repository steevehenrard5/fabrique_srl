@extends('front._master')

@section('_content')
    @include('front/partials/' . $partial_details->partial_path, ['item' => $item])
@stop
