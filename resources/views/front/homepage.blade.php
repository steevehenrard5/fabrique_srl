@extends('front._master')

@section('header_title')
    <span style="font-weight:300">La Fabrique à</span> SRL
@endsection

@section('header_desc')
    Prêt à lancer votre propre SRL (Société à Responsabilité Limitée) ?<br>
    Restent les étapes administratives ?<br>
    Welcome, c’est par ici que ça se passe…
@endsection

@section('header_cta')
    <a href="#" class="btn" style="background-color: #ea6434; color:white;" data-toggle="modal" data-target="#popup1">Créer ma SRL</a>
@endsection

@section('_content')
    <section id="feature1" class="feature feature-1 text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                    <div class="heading heading-1 heading-center mb-70">
                        <h2 class="heading-title">CREEZ <span style="font-weight:300">VOTRE ENTREPRISE</span></h2>
                        <p class="heading-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus cursus malesuada pellentesque.</p>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="feature-panel">
                        <div class="feature-icon">
                            <i class="fa fa-wifi" style="color:#ea6434;"></i>
                        </div>
                        <div class="feature-content">
                            <h3>En ligne</h3>
                            <p>Simple, facile, sans vous déplacer<br>Avec un interlocuteur unique</p>
                        </div>
                    </div>

                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="feature-panel">
                        <div class="feature-icon">
                            <i class="fa fa-users" style="color:#ea6434;"></i>
                        </div>
                        <div class="feature-content">
                            <h3>Une aide complète</h3>
                            <p>Pour comprendre, valider <br>et démarrer avec les bons outils</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="feature-panel">
                        <div class="feature-icon">
                            <i class="fa fa-hand-grab-o" style="color:#ea6434;"></i>
                        </div>
                        <div class="feature-content">
                            <h3>Par/Pour des entrepreneurs</h3>
                            <p>Centré sur l’essentiel, le vraiment utile<br>Car nous savons ce que c’est !</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="banner1" class="banner banner-1 bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="heading heading-1">
                        <h2 class="heading-title"><span style="font-weight:300">Pour</span> créer votre SRL<span style="font-weight:300">, vous voulez :</span></h2>
                        <ul style="padding:0;">
                            <li class="mb-2"><span style="font-size:22px;"><i class="fa fa-chevron-right mr-1"></i> Comprendre facilement les éléments importants de cette grande étape ?</span></li>
                            <li class="mb-2"><span style="font-size:20px;"><i class="fa fa-chevron-right mr-1"></i> Gagner du temps ?</span></li>
                            <li class="mb-2"><span style="font-size:18px;"><i class="fa fa-chevron-right mr-1"></i> Vous faciliter la vie (votre valeur ajoutée n’est pas dans l’administratif mais bien dans votre métier) ?</span></li>
                            <li class="mb-2"><span style="font-size:16px;"><i class="fa fa-chevron-right mr-1"></i> Vous protéger comme il faut ?</span></li>
                            <li class="mb-2"><i class="fa fa-chevron-right mr-1"></i> Pour un coût minimal (quasi le même coût que si vous le faisiez vous-même, grâce à notre process digital et à nos partenariats)</li>
                            <li class="mb-2"><i class="fa fa-chevron-right mr-1"></i> Rejoindre une super communauté d’Entrepreneurs ?</li>
                            <li class="mb-2"><i class="fa fa-chevron-right mr-1"></i> Et découvrir une surprise qui vous aidera à décoller… ?</li>
                        </ul>
                    </div>
                    <!-- .heading  end -->
                </div>
                <!-- .col-lg-5 end -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-5 offset-lg-1">
                    <div class="banner-img text-right">
                        <img src="{{asset('assets/front/images/banners/1.png')}}" alt="banner" class="img-fluid">
                    </div>
                </div>
                <!-- .col-lg-6 end -->
            </div>
            <!-- .row end -->
            <hr>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="banner-img">
                        <img src="{{asset('assets/front/images/banners/2.png')}}" alt="banner" class="img-fluid">
                    </div>
                </div>
                <!-- .col-lg-6 end -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-5 offset-lg-1">
                    <div class="heading heading-1">
                        <h2 class="heading-title"><span style="font-weight:300">Ca y est ! C’est le grand départ ? </span>Prêt à démarrer l’aventure ?</h2>
                        <p class="heading-desc">
                            Restent les étapes administratives (notaire, banque, plan financier, tva, guichet, greffe, …).<br>
                            Chez Wikipreneurs, nous savons ce que représente cette étape, là où tout commence...
                            Pour la franchir, nous vous facilitons la vie en prenant en charge tout le processus administratif
                            grâce à notre outil digital et nos partenaires. <br> De plus, nous vous accompagnons en vous donnant les
                            bons conseils et les bons outils pour démarrer dans les meilleures conditions. Car nous l’avons déjà fait,
                            plusieurs fois nous-mêmes, et en accompagnant des centaines d’Entrepreneurs.
                        </p>
                    </div>
                    <!-- .heading  end -->
                </div>
                <!-- .col-lg-5 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>

    <section id="cta1" class="cta cta-1 bg-theme text-center" style="min-height: 300px; position:relative;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1" style="margin-bottom:80px;">
                    <div class="heading heading-1 heading-center mb-40">
                        <h2 class="heading-title" style="line-height:36px;">
                            <span style="font-weight: 300">L’accès à ‘La Fabrique à SRL’ comprenant</span> le processus complet de création de votre société <span style="font-weight: 300">vous est accessible suite à un entretien.</span>
                        </h2>
                    </div>
                    <p><i class="fa fa-arrow-circle-o-down" style="color:#fff; font-size:50px;"></i></p>
                </div>
            </div>
        </div>
    </section>


    <section id="pricing4" class="pricing pricing-4 text-center bg-gray" style="margin-top:-275px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 price-table">
                    <div class="pricing-panel" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="pricing-img" style="padding-top:50px;">
                                    <img src="{{asset('assets/front/images/pricing/2.png')}}" alt="img" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-md-7 text-left" style="padding-top:20px;">
                                <div class="pricing-heading" style="padding-left:0;">

                                    <h4>La Fabrique à SRL</h4>
                                </div>
                                <div class="pricing--body">

                                    <p> En fonction de votre situation et de vos besoins :</p>

                                    <ul class="pricing-list list-unstyled" style="padding:0; margin-bottom:20px;">
                                        <li><i class="fa fa-check mr-1"></i> Notaire compris</li>
                                        <li><i class="fa fa-check mr-1"></i> Conseil compris</li>
                                        <li><i class="fa fa-check mr-1"></i> Explications comprises</li>
                                        <li><i class="fa fa-check mr-1"></i> Inscriptions comprises</li>
                                        <li><i class="fa fa-check mr-1"></i> Publications comprises</li>
                                    </ul>
                                </div>
                                <div class="pricing--footer">
                                    <p>1350,00€ <small style="font-size:12px;">(déductibles)</small></p>
                                    <a class="btn" style="width:60%; background-color: #ea6434; color:white;" href="#" data-toggle="modal" data-target="#popup1">Je crée mon entreprise</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="banner2" class="banner banner-1 bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="banner-img">
                        <img src="{{asset('assets/front/images/banners/2.png')}}" alt="banner" class="img-fluid">
                    </div>
                </div>
                <!-- .col-lg-6 end -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-5 offset-lg-1">
                    <div class="heading heading-1">
                        <h2 class="heading-title">Pourquoi <span style="font-weight: 300">Wikipreneurs ?</span></h2>
                        <p class="heading-desc">
                            Wikipreneurs développe depuis des années des outils pour faciliter la vie des Entrepreneurs et des professionnels qui les accompagnent.
                        </p>
                        <ul style="padding:0;">
                            <li><i class="fa fa-check mr-1"></i> + 500.000 outils utilisés en 2020</li>
                            <li><i class="fa fa-check mr-1"></i> + 35.000 Entrepreneurs membres</li>
                            <li><i class="fa fa-check mr-1"></i> Un groupe d’entraide de + 5.000 Entrepreneurs</li>
                            <li><i class="fa fa-check mr-1"></i> Le salon ‘6 jours pour créer/développer ma boîte’</li>
                            <li><i class="fa fa-check mr-1"></i> Des partenaires – privés, publics, grands, moyens, petits – depuis des années</li>
                            <li><i class="fa fa-check mr-1"></i> Une équipe passionnée par les ‘actes d’entreprendre’</li>
                        </ul>
                    </div>
                    <!-- .heading  end -->
                </div>
                <!-- .col-lg-5 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>

    <section id="cta2" class="cta cta-1 bg-theme text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                    <div class="heading heading-1 heading-center mb-40">
                        <h2 class="heading-title">Créez <span style="font-weight: 300">votre entreprise</span></h2>
                        <p style="color:#fff;">Grâce à la ‘Fabrique à SRL’ de Wikipreneurs, spécialement développée pour cela !</p>
                        <ul style="padding:0; color:#fff; margin-left:50px;" class="text-left">
                            <li><i class="fa fa-check mr-1"></i> Simple, facile, efficace.</li>
                            <li><i class="fa fa-check mr-1"></i> Sans vous déplacer, 100% en ligne.</li>
                            <li><i class="fa fa-check mr-1"></i> Via un seul contact qui s’occupe de tout pour vous</li>
                            <li><i class="fa fa-check mr-1"></i> En vous conseillant avec « bienveillance/compétence/boost Entrepreneur ».</li>
                            <li><i class="fa fa-check mr-1"></i> Les derniers outils ‘up to date’ qui vous aideront à bien gérer par la suite.</li>
                            <li><i class="fa fa-check mr-1"></i> Par/Pour des Entrepreneurs (qui savent ce que c’est…).</li>
                        </ul>
                    </div>

                    <p><i class="fa fa-arrow-circle-o-down" style="color:#fff; font-size:50px; margin-bottom:30px;"></i></p>

                    <a href="/demande-de-devis" class="btn btn--white" data-toggle="modal" data-target="#popup1">Obtenir un devis</a>
                </div>
                <!-- .col-lg-8 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <section id="clients2" class="clients clients-1 bg-theme section-divider-light">
        <div class="container">
            <div class="row">
                <!-- Client #1 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/1.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
                <!-- Client #2 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/2.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
                <!-- Client #3 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/3.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
                <!-- Client #4 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/4.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
                <!-- Client #5 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/5.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
                <!-- Client #6 -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <div class="client">
                        <a href="#"><img class="center-block" src="{{asset('assets/front/images/clients/1.png')}}" alt="client"></a>
                    </div>
                </div>
                <!-- .col-lg-2 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>

    <section id="propirties1" class="propirties propirties-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                    <div class="heading heading-6 heading-center mb-70">
                        <h2 class="heading-title">Nos actualités</h2>
                        <p class="heading-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet consequat risus, ac condimentum nisl.</p>
                    </div>
                </div>
            </div>

            @foreach($contents as $key => $content)
            <div class="row">
                @foreach($content->items as $item)
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="property-item">
                            <div class="property--img">
                                <a href="{{ route('front.page.module.details', ['news', 'articles', $item->slug]) }}">
                                    <img src="{{asset('assets/front/images/propirties/grid/1.jpg')}}" alt="property">
                                </a>
                                <span class="label">Actu</span>
                            </div>
                            <!-- .proprty-img end -->
                            <div class="property--info">
                                <a href="{{ route('front.page.module.details', ['news', 'articles', $item->slug]) }}">
                                    <h4>{{$item->title}}</h4>
                                </a>
                                <p>{{$item->description_short}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </section>

@endsection