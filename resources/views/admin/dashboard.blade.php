@extends('admin._master')

@section('_title')
    Administration -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_dashboard')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-4">
            <div class="white-box">
                <a href="{{route('admin.users')}}"><h3 class="box-title">@lang('backend.users')</h3></a>
                <ul class="list-inline two-part">
                    <li><i class="icon-people text-info"></i></li>
                    <li class="text-right"><span class="counter">{{$counters->users}}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="white-box">
                <h3 class="box-title">@lang('backend.admins')</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-people text-danger"></i></li>
                    <li class="text-right"><span class="counter">{{$counters->admins}}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="white-box">
                <a href="{{route('admin.pages')}}"><h3 class="box-title">@lang('backend.title_pages')</h3></a>
                <ul class="list-inline two-part">
                    <li><i class="icon-layers text-success"></i></li>
                    <li class="text-right"><span class="counter">{{$counters->pages}}</span></li>
                </ul>
            </div>
        </div>
    </div>
@stop