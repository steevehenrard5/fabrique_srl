@extends('admin._master')

@section('_title')
    @lang('modules.title_contact_messages') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.contact_messages')}}">@lang('modules.title_contact_messages')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_contact_messages')
@stop

@section('_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <th width="50"></th>
                        <th>@lang('modules.contact_messages_created_at_label')</th>
                        <th>@lang('modules.contact_messages_name_label')</th>
                        <th>@lang('modules.contact_messages_email_label')</th>
                        <th>@lang('modules.contact_messages_phone_label')</th>
                        <th width="200" style="text-align: center;">@lang('backend.actions')</th>
                    </thead>
                    <tbody>
                        @foreach($contact_messages as $message)
                            <tr>
                                <td style="text-align: center;">
                                    @if($message->is_new)
                                        <i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>
                                    @endif
                                </td>
                                <td>{{date('d/m/Y h:i', strtotime($message->created_at))}}</td>
                                <td>{{$message->getName()}}</td>
                                <td>{{$message->email}}</td>
                                <td>{{$message->phone}}</td>
                                <td style="text-align: center;">
                                    @if($message->is_deleted)
                                        <a href="{{route('admin.modules.contact_messages.restore', ['id' => $message->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light">@lang('backend.restore')</a>
                                        <a href="{{route('admin.modules.contact_messages.destroy', ['id' => $message->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.destroy')</a>
                                    @else
                                        <a href="{{route('admin.modules.contact_messages.show', ['id' => $message->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('modules.contact_message_see')</a>
                                        <a href="{{route('admin.modules.contact_messages.delete', ['id' => $message->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$contact_messages->render()}}
            </div>
        </div>
    </div>
@stop