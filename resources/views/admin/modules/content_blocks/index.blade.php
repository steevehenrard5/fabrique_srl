@extends('admin._master')

@section('_title')
    @lang('modules.title_content_blocks') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.content_blocks')}}">@lang('modules.title_content_blocks')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_content_blocks')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.content_blocks.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_content_block')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($blocks as $block)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$block->is_deleted)
                        <a href="{{route('admin.modules.content_blocks.draft.switch', ['id' => $block->id])}}" type="button" class="switcher-active btn btn-{{$block->is_draft ? 'default' : 'success'}} btn-circle waves-effect waves-light"><i class="fa fa-{{$block->is_draft ? 'eye-slash' : 'eye'}}"></i> </a>
                    @endif
                    <h3 class="m-t-20 m-b-20 {{$block->is_deleted ? 'is-deleted' : ''}}">
                        @if($block->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$block->name}}
                    </h3>
                    <p>
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($block->updated_at))}}
                    </p>
                    @if($block->is_deleted)
                        <a href="{{route('admin.modules.content_blocks.restore', ['id' => $block->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                        <a href="{{route('admin.modules.content_blocks.destroy', ['id' => $block->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                    @else
                        <a href="{{route('admin.modules.content_blocks.edit', ['id' => $block->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.modules.content_blocks.delete', ['id' => $block->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$blocks->render()}}
        </div>
    </div>
@stop