<input type="hidden" name="action_url" value="_Modules\ContentBlocksController">
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_partial_label')</label>
        <select name="block_partial_id" class="form-control">
            @foreach($module->partials as $partial)
                <option value="{{$partial->id}}">{{$partial->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_name_label')</label>
        <input type="text" name="block_name" class="form-control" placeholder="@lang('modules.content_block_name_label')">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_head_title_label')</label>
        <input type="text" name="block_head_title" class="form-control" placeholder="@lang('modules.content_block_head_title_label')">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_title_label')</label>
        <input type="text" name="block_title" class="form-control" placeholder="@lang('modules.content_block_title_label')">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_description_label')</label>
        <textarea name="block_description" class="form-control editor-cms-small"></textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_call_action_label')</label>
        <input type="text" name="block_call_to_action" class="form-control" placeholder="@lang('modules.content_block_call_action_label')">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_page_label')</label>
        <select name="block_page_id" class="form-control">
            <option value="0">-- @lang('backend.input_select_page')</option>
            @foreach($pages as $page)
                <option value="{{$page->id}}">{{$page->attributes->title}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_custom_url_label')</label>
        <input type="text" name="block_custom_url" class="form-control" placeholder="@lang('modules.content_block_custom_url_label')">
    </div>
</div>

<div class="vtabs customvtab form-group">
    <ul class="nav tabs-vertical">
        <li class="tab active">
            <a data-toggle="tab" href="#picture_content_block" aria-expanded="false">@lang('modules.content_block_picture')</a>
        </li>
        <li class="tab">
            <a data-toggle="tab" href="#video_content_block" aria-expanded="false">@lang('modules.content_block_video')</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="picture_content_block" class="tab-pane active">
            <div class="form-group m-b-0">
                <div class="col-md-12">
                    <label>@lang('modules.content_block_image_label')</label>
                    <input  type="file" name="block_image" class="form-control">
                </div>
            </div>
        </div>
        <div id="video_content_block" class="tab-pane">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="checkbox checkbox-primary">
                        <input name="block_is_video" id="checkbox_video_{{$module->id}}" type="checkbox" value="1">
                        <label for="checkbox_video_{{$module->id}}">@lang('modules.content_block_is_video')</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>@lang('modules.content_block_video_title_label')</label>
                    <input type="text" name="block_video_title" class="form-control" placeholder="@lang('modules.content_block_video_title_label')">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>@lang('modules.content_block_video_url_label')</label>
                    <input type="text" name="block_video_url" class="form-control" placeholder="@lang('modules.content_block_video_url_label')">
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-md-12">
                    <label>@lang('modules.content_block_video_time_label')</label>
                    <input type="text" name="block_video_time" class="form-control" placeholder="@lang('modules.content_block_video_time_label')">
                </div>
            </div>
        </div>
    </div>
</div>