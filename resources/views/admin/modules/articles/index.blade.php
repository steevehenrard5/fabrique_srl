@extends('admin._master')

@section('_title')
    @lang('modules.title_articles') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.articles')}}">@lang('modules.title_articles')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_articles')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.articles.create')}}" class="btn btn-success m-t-10 m-r-5 btn-rounded waves-effect waves-light">@lang('modules.new_article')</a>
                        <a href="{{route('admin.modules.articles.categories')}}" class="btn btn-default m-t-10 btn-rounded waves-effect waves-light">@lang('modules.articles_manage_categories')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($articles as $article)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$article->is_deleted)
                        <a href="{{route('admin.modules.articles.draft.switch', ['id' => $article->id])}}" type="button" class="switcher-active btn btn-{{$article->is_draft ? 'default' : 'success'}} btn-circle waves-effect waves-light"><i class="fa fa-{{$article->is_draft ? 'eye-slash' : 'eye'}}"></i> </a>
                    @endif
                    <h3 class="m-t-20 m-b-20 {{$article->is_deleted ? 'is-deleted' : ''}}">
                        @if($article->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$article->title}}
                    </h3>
                    <p>
                        <strong><i class="ti-user"></i></strong> {{$article->creator->getName()}}
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($article->updated_at))}}
                    </p>
                    @if($article->is_deleted)
                        <a href="{{route('admin.modules.articles.restore', ['id' => $article->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                        <a href="{{route('admin.modules.articles.destroy', ['id' => $article->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                    @else
                        <a href="{{route('admin.modules.articles.edit', ['id' => $article->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.modules.articles.delete', ['id' => $article->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$articles->render()}}
        </div>
    </div>
@stop