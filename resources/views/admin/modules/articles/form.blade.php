@extends('admin._master')

@section('_title')
    @lang('modules.title_articles') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.articles')}}">@lang('modules.title_articles')</a></li>
    <li class="active">@lang('modules.edit_article')</li>
@stop

@section('_title_section')
    @lang('modules.edit_article')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.articles')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.article_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_title')</label>
                    <div class="col-md-12">
                        <input required type="text" name="title" class="form-control" value="{{isset($article->id) ? $article->title : old('title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_slug')</label>
                    <div class="col-md-12">
                        <input type="text" name="slug" class="form-control" value="{{isset($article->id) ? $article->slug : old('slug')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_type')</label>
                    <div class="col-md-12">
                        <select name="type_id" class="form-control">
                            @foreach($types as $type)
                                <option value="{{$type->id}}" {{isset($article->id) && $article->type_id == $type->id ? 'selected="selected"' : ''}}>{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_category')</label>
                    <div class="col-md-12">
                        <select name="category_id" class="form-control">
                            <option value="0">-- @lang('modules.article_category_select')</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" {{isset($article->id) && $article->category_id == $category->id ? 'selected="selected"' : ''}}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_video_url')</label>
                    <div class="col-md-12">
                        <input type="text" name="video_url" class="form-control" value="{{isset($article->id) ? $article->video_url : old('video_url')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_tags')</label>
                    <div class="col-md-12">
                        <input type="text" name="tags" placeholder="@lang('modules.article_label_add_tags')" data-role="tagsinput" class="form-control" value="{{isset($article->id) ? $article->tags : old('tags')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_description_short')</label>
                    <div class="col-md-12">
                        <textarea name="description_short" class="form-control" rows="2">{{isset($article->id) ? $article->description_short : old('description_short')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_description')</label>
                    <div class="col-md-12">
                        <textarea name="description" class="form-control" rows="4">{{isset($article->id) ? $article->description : old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_date')</label>
                    <div class="col-md-12">
                        <input type="text" name="custom_date" class="datepicker-cms form-control" value="{{isset($article->id) ? $article->custom_date : old('custom_date')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_content')</label>
                    <div class="col-md-12">
                        <textarea name="content" class="form-control editor-cms">{{isset($article->id) ? $article->content : old('content')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_image')</label>
                    <div class="col-md-12">
                        @if(isset($article->id) && $article->picture != '')
                            <img src="{{asset('/uploads/img/modules/articles/' . $article->picture)}}" class="max-w-300">
                            <div class="checkbox checkbox-danger m-b-5">
                                <input name="delete_picture" id="checkbox_del_picture" type="checkbox" value="1">
                                <label for="checkbox_del_picture">@lang('modules.article_label_image_delete')</label>
                            </div>
                        @endif
                        <input type="file" name="picture" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_file')</label>
                    <div class="col-md-12">
                        @if(isset($article->id) && $article->file != '')
                            <a target="_blank" class="link-u" href="{{asset('/uploads/files/modules/articles/' . $article->file)}}">{{$article->file}}</a>
                            <div class="checkbox checkbox-danger m-b-5">
                                <input name="delete_file" id="checkbox_del_file" type="checkbox" value="1">
                                <label for="checkbox_del_file">@lang('modules.article_label_file_delete')</label>
                            </div>
                        @endif
                        <input type="file" name="file" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_label_file_name')</label>
                    <div class="col-md-12">
                        <input type="text" name="file_name" class="form-control" value="{{isset($article->id) ? $article->file_name : old('file_name')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop