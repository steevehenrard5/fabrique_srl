@extends('admin._master')

@section('_title')
    @lang('modules.title_articles_categories') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.articles.categories')}}">@lang('modules.title_articles_categories')</a></li>
    <li class="active">@lang('modules.edit_article_category')</li>
@stop

@section('_title_section')
    @lang('modules.edit_article_category')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.articles.categories')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.article_category_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_category_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($category->id) ? $category->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.article_category_label_slug')</label>
                    <div class="col-md-12">
                        <input type="text" name="slug" class="form-control" value="{{isset($category->id) ? $category->slug : old('slug')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop