@extends('admin._master')

@section('_title')
    @lang('modules.title_articles_categories') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.articles.categories')}}">@lang('modules.title_articles_categories')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_articles_categories')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.articles.categories.create')}}" class="btn btn-success m-t-10 m-r-5 btn-rounded waves-effect waves-light">@lang('modules.new_article_category')</a>
                        <a href="{{route('admin.modules.articles')}}" class="btn btn-default m-t-10 btn-rounded waves-effect waves-light">@lang('backend.back')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($categories as $category)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    <h3 class="m-t-20 m-b-20">
                       {{$category->name}}
                    </h3>
                    <p>
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($category->updated_at))}}
                    </p>
                    <a href="{{route('admin.modules.articles.categories.edit', ['id' => $category->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                    <a href="{{route('admin.modules.articles.categories.delete', ['id' => $category->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$categories->render()}}
        </div>
    </div>
@stop