@extends('admin._master')

@section('_title')
    @lang('modules.title_galleries') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.galleries')}}">@lang('modules.title_galleries')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_galleries')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.galleries.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_gallery')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($galleries as $gallery)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$gallery->is_deleted)
                        <a href="{{route('admin.modules.galleries.draft.switch', ['id' => $gallery->id])}}" type="button" class="switcher-active btn btn-{{$gallery->is_draft ? 'default' : 'success'}} btn-circle waves-effect waves-light"><i class="fa fa-{{$gallery->is_draft ? 'eye-slash' : 'eye'}}"></i> </a>
                    @endif
                    <h3 class="m-t-20 m-b-20 {{$gallery->is_deleted ? 'is-deleted' : ''}}">
                        @if($gallery->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$gallery->name}}
                    </h3>
                    <p>
                        <strong><i class="ti-user"></i></strong> {{$gallery->creator->getName()}}
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($gallery->updated_at))}}
                    </p>
                    <p class="m-t-15">
                        @if($gallery->pictures->count() == 0)
                            <div class="box-pic-gallery more">?</div>
                        @endif
                        @foreach($gallery->pictures as $key => $picture)
                            @if($key < 4)
                                <div class="box-pic-gallery" style="background-image: url('{{$picture->getPath()}}')"></div>
                            @endif
                        @endforeach
                        @if($gallery->pictures->count() > 4)
                            <div class="box-pic-gallery more">
                                +{{$gallery->pictures->count() - 4}}
                            </div>
                        @endif
                    </p>
                    <p class="m-t-15">
                        <i class="ti-image m-r-5"></i>{{$gallery->pictures->count()}} @lang('modules.gallery_pictures')
                    </p>
                    @if($gallery->is_deleted)
                        <a href="{{route('admin.modules.galleries.restore', ['id' => $gallery->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                        <a href="{{route('admin.modules.galleries.destroy', ['id' => $gallery->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                    @else
                        <a href="{{route('admin.modules.galleries.edit', ['id' => $gallery->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.modules.galleries.delete', ['id' => $gallery->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$galleries->render()}}
        </div>
    </div>
@stop