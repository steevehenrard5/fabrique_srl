<div class="remodal" data-remodal-id="remodal_upload_images">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>@lang('modules.gallery_upload_pictures_for') :<br>{{$gallery->name}} <br><small>(max. : {{$max_queue_size}} @lang('modules.gallery_files_per_queue'), {{ini_get('post_max_size')}} max.)</small></h1>
    <form action="{{route('admin.modules.galleries.pictures.upload')}}" class="dropzone" id="dropzone-galleries" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="gallery_id" value="{{isset($gallery->id) ? $gallery->id : 0}}">
        <div class="fallback">
            <input name="pictures" type="file" multiple/>
        </div>
    </form>
    <button data-remodal-action="cancel" class="remodal-cancel m-t-15">@lang('backend.cancel_modal')</button>
    <button class="remodal-confirm processQueue m-t-15">@lang('modules.gallery_process_queue')</button>
</div>