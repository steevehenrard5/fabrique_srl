@extends('admin._master')

@section('_title')
    @lang('modules.title_sliders') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.sliders')}}">@lang('modules.title_sliders')</a></li>
    <li class="active">@lang('modules.edit_slider')</li>
@stop

@section('_title_section')
    @lang('modules.edit_slider')
@stop

@section('_content')
    @if(isset($slider->id)) @include('admin.modules.sliders.remodals.upload_images') @endif

    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.sliders')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <p class="m-b-0">@lang('modules.slider_translate_editing', ['lang' => '<strong>' . strtoupper(session('switched_language')) . '</strong>'])</p>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.slider_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.slider_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($slider->id) ? $slider->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.slider_label_partial')</label>
                    <div class="col-md-12">
                        <select name="partial_id" class="form-control">
                            <option value="0">-- @lang('modules.slider_label_partial_select')</option>
                            @foreach($partials as $partial)
                                <option value="{{$partial->id}}" {{isset($slider->id) && $slider->partial_id == $partial->id ? 'selected="selected' : ''}}>{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.slider_label_head_cta_text')</label>
                    <div class="col-md-12">
                        <input type="text" name="cta_text" class="form-control" value="{{isset($slider->id) ? $slider->call_to_action_text : old('cta_text')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.slider_label_cta_anchor')</label>
                    <div class="col-md-12">
                        <input type="text" name="cta_anchor" class="form-control" value="{{isset($slider->id) ? $slider->call_to_action_anchor : old('cta_anchor')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>

            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title m-b-0">@lang('modules.slider_pictures_title')</h3>
                @if(!isset($slider->id))
                    <p class="m-b-0">@lang('modules.slider_save_first')</p>
                @else
                    <div class="col-md-3">
                        <div class="well m-t-10 m-b-0 text-center">
                            <a href="#" data-remodal-target="remodal_upload_images" class="btn btn-rounded btn-success"><i class="ti-upload m-r-5"></i>@lang('modules.slider_upload_images')</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                    @foreach($slider->pictures as $picture)
                        <div class="col-md-3">
                            <div class="well m-t-15 m-b-0">
                                <div class="pull-right">#{{$picture->order}}</div>
                                <div class="text-center m-b-15">
                                    @if($picture->order > 1)
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.sliders.picture.up', $picture->id)}}"><i class="fa fa-chevron-left"></i></a>
                                    @endif
                                    @if($picture->order < $slider->pictures->count())
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.sliders.picture.down', $picture->id)}}"><i class="fa fa-chevron-right"></i></a>
                                    @endif
                                </div>
                                <img src="{{$picture->getPath()}}" class="img m-b-15 h-225">
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="head_titles[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.slider_photo_head_title_label')" value="{{$picture->head_title}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="titles[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.slider_photo_title_label')" value="{{$picture->title}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <textarea name="descriptions[{{$picture->id}}]" class="form-control no-grow" rows="3" placeholder="@lang('modules.slider_photo_description_label')">{{$picture->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="call_to_action_texts[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.slider_photo_cta_text_label')" value="{{$picture->call_to_action_text}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <select name="call_to_action_types[{{$picture->id}}]" class="form-control">
                                            <option value="in" {{$picture->call_to_action_type == 'in' ? 'selected="selected"' : ''}}>@lang('modules.slider_in_type')</option>
                                            <option value="out" {{$picture->call_to_action_type == 'out' ? 'selected="selected"' : ''}}>@lang('modules.slider_out_type')</option>
                                            <option value="video" {{$picture->call_to_action_type == 'video' ? 'selected="selected"' : ''}}>@lang('modules.slider_video_type')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <select name="page_ids[{{$picture->id}}]" class="form-control">
                                            <option value="0">-- @lang('modules.slider_photo_page_select_label')</option>
                                            @foreach($pages as $page)
                                                <option value="{{$page->id}}" {{$picture->page_id == $page->id ? 'selected="selected' : ''}}>{{$page->attributes->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="custom_urls[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.slider_photo_custom_url_label')" value="{{$picture->custom_url}}">
                                    </div>
                                </div>
                                <button type="submit" class="btn-rounded btn btn-outline btn-success waves-effect waves-light">@lang('backend.update')</button>
                                <a href="{{route('admin.modules.sliders.picture.delete', $picture->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                            </div>
                        </div>
                    @endforeach
                    <div class="clear"></div>
                @endif
            </div>
        </div>
    </form>
@stop

@section('_scripts')
    <script>
        Dropzone.autoDiscover = false;
        $(function() {
            var myDropzone = new Dropzone('#dropzone-sliders', {
                uploadMultiple: true,
                autoProcessQueue: false,
                maxFiles: null,
                parallelUploads: '{{$max_queue_size}}',
                maxFilesize: '{{$max_files_size}}'
            });
            myDropzone.on('completemultiple', function() {
                location.reload();
            });
            myDropzone.on('addedfile', function() {
                $('.processQueue').fadeIn(500);
            });

            $('.processQueue').click(function(e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
        });
    </script>
@stop