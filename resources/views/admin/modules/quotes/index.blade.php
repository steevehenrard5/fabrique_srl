@extends('admin._master')

@section('_title')
    @lang('modules.title_quotes') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.quotes')}}">@lang('modules.title_quotes')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_quotes')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.quotes.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_quote')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($quotes as $quote)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$quote->is_deleted)
                        <a href="{{route('admin.modules.quotes.draft.switch', ['id' => $quote->id])}}" type="button" class="switcher-active btn btn-{{$quote->is_draft ? 'default' : 'success'}} btn-circle waves-effect waves-light"><i class="fa fa-{{$quote->is_draft ? 'eye-slash' : 'eye'}}"></i> </a>
                    @endif
                    <h3 class="m-t-20 m-b-20 {{$quote->is_deleted ? 'is-deleted' : ''}}">
                        @if($quote->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$quote->name}}
                    </h3>
                    <p>
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($quote->updated_at))}}
                    </p>
                    @if($quote->is_deleted)
                        <a href="{{route('admin.modules.quotes.restore', ['id' => $quote->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                        <a href="{{route('admin.modules.quotes.destroy', ['id' => $quote->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                    @else
                        <a href="{{route('admin.modules.quotes.edit', ['id' => $quote->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.modules.quotes.delete', ['id' => $quote->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$quotes->render()}}
        </div>
    </div>
@stop