@extends('admin._master')

@section('_title')
    @lang('modules.title_banners') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.banners')}}">@lang('modules.title_banners')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_banners')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.banners.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_banner')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($banners as $banner)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$banner->is_deleted)
                        <a href="{{route('admin.modules.banners.draft.switch', ['id' => $banner->id])}}" type="button" class="switcher-active btn btn-{{$banner->is_draft ? 'default' : 'success'}} btn-circle waves-effect waves-light"><i class="fa fa-{{$banner->is_draft ? 'eye-slash' : 'eye'}}"></i> </a>
                    @endif
                    <h3 class="m-t-20 m-b-20 {{$banner->is_deleted ? 'is-deleted' : ''}}">
                        @if($banner->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$banner->name}}
                    </h3>
                    <p>
                        <br><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($banner->updated_at))}}
                    </p>
                    @if($banner->is_deleted)
                        <a href="{{route('admin.modules.banners.restore', ['id' => $banner->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                        <a href="{{route('admin.modules.banners.destroy', ['id' => $banner->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                    @else
                        <a href="{{route('admin.modules.banners.edit', ['id' => $banner->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.modules.banners.delete', ['id' => $banner->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$banners->render()}}
        </div>
    </div>
@stop