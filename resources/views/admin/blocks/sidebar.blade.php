<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="#" class="waves-effect">
                    <img src="{{guard_admin()->getAvatar()}}" alt="user-img"  class="img-circle">
                    <span class="hide-menu"> {{guard_admin()->first_name}} ({{guard_admin()->username}})<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> @lang('backend.logout')</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('admin.dashboard')}}" class="waves-effect {{$dash_active == 'dashboard' ? 'active' : ''}}"><i data-icon="a" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_dashboard')</span></a>
            </li>
            <li class="nav-small-cap m-t-10 p-l-30">@lang('backend.title_managing')</li>
            @if(guard_admin()->canAccess('pages'))
                <li>
                    <a href="{{route('admin.pages')}}" class="waves-effect {{$dash_active == 'pages' ? 'active' : ''}}"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_pages')</span></a>
                </li>
            @endif
            @if(guard_admin()->canAccess('menus'))
                <li>
                    <a href="{{route('admin.menus')}}" class="waves-effect {{$dash_active == 'menus' ? 'active' : ''}}"><i data-icon="&#xe026;" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_menus')</span></a>
                </li>
            @endif
            @if(guard_admin()->level >= 100)
                <li>
                    <a href="{{route('admin.partials')}}" class="waves-effect {{$dash_active == 'partials' ? 'active' : ''}}"><i data-icon="&#xe025;" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_partials')</span></a>
                </li>
            @endif
            @if(guard_admin()->canAccess('medias'))
                <li>
                    <a href="{{route('admin.medias')}}" class="waves-effect {{$dash_active == 'medias' ? 'active' : ''}}"><i data-icon="^" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">Medias</span></a>
                </li>
            @endif
            <li class="nav-small-cap m-t-10 p-l-30">@lang('backend.title_modules')</li>
            @if(guard_admin()->level >= 100)
                <li>
                    <a href="{{route('admin.modules')}}" class="waves-effect {{$dash_active == 'modules' ? 'active' : ''}}"><i data-icon="?" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.manage')</span></a>
                </li>
            @endif
            <li class="{{strpos($dash_active, 'module_') !== false ? 'active' : ''}}">
                <a href="#" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="7"></i> <span class="hide-menu"> @lang('backend.title_modules_active') <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">{{$_modules->count()}}</span></span></a>
                <ul class="nav nav-second-level collapse {{strpos($dash_active, 'module_') !== false ? 'in' : ''}}">
                    @foreach($_modules as $module)
                        @if(guard_admin()->canAccess($module->reference))
                            <li><a class="waves-effect {{$dash_active == 'module_' . $module->reference ? 'active' : ''}}" href="{{route('admin.modules.' . $module->reference)}}">{{$module->name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
            <li class="nav-small-cap m-t-10 p-l-30">@lang('backend.title_settings')</li>
            <li class="{{$dash_active == 'users' || $dash_active == 'operators' || $dash_active == 'groups' ? 'active' : ''}}">
                <a href="#" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="9"></i> <span class="hide-menu"> @lang('backend.title_users') <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level collapse {{$dash_active == 'users' || $dash_active == 'operators' || $dash_active == 'groups' ? 'in' : ''}}">
                    <li>
                        <a href="{{route('admin.users')}}" class="waves-effect {{$dash_active == 'users' ? 'active' : ''}}"><span class="hide-menu">@lang('backend.title_users_list')</span></a>
                    </li>
                    @if(guard_admin()->level >= 100)
                        <li>
                            <a href="{{route('admin.operators')}}" class="waves-effect {{$dash_active == 'operators' ? 'active' : ''}}"><span class="hide-menu">@lang('backend.title_operators')</span></a>
                        </li>
                    @endif
                    <!--<li>
                        <a href="{{route('admin.groups')}}" class="waves-effect {{$dash_active == 'groups' ? 'active' : ''}}"><span class="hide-menu">@lang('backend.title_groups')</span></a>
                    </li>-->
                </ul>
            </li>
            <li>
                <a href="{{route('admin.languages')}}" class="waves-effect {{$dash_active == 'languages' ? 'active' : ''}}"><i data-icon="." class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_languages')</span></a>
            </li>
            <li>
                <a href="{{route('admin.configuration')}}" class="waves-effect {{$dash_active == 'configuration' ? 'active' : ''}}"><i data-icon="P" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_configuration')</span></a>
            </li>
            <!--<li>
                <a href="{{route('admin.sites')}}" class="waves-effect {{$dash_active == 'sites' ? 'active' : ''}}"><i data-icon="O" class="linea-icon linea-basic fa-fw m-r-5"></i><span class="hide-menu">@lang('backend.title_sites')</span></a>
            </li>-->
        </ul>
    </div>
</div>
