<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part">
            <a class="logo" href="{{route('admin.dashboard')}}">
                <b>
                    <img src="{{asset('assets/admin/img/belead-logo.png')}}" alt="home" class="dark-logo" style="max-width: 30px;">
                </b>
                <span class="hidden-xs">
                    <img src="{{asset('assets/admin/img/belead-text.png')}}" alt="home" class="dark-logo">
                </span>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li class="m-r-10"><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="ti-menu"></i></a></li>
            {{--<li style="line-height: 60px;" class="m-r-10">--}}
                {{--<button type="button" class="btn waves-effect waves-light btn-success btn-outline dropdown-toggle btn-rounded" data-toggle="dropdown">{{session('switched_site.name')}} <span class="caret m-l-5"></span></button>--}}
                {{--<ul class="dropdown-menu">--}}
                    {{--@foreach($_sites as $site)--}}
                        {{--<li>--}}
                            {{--<a href="{{route('admin.site.switch', ['id' => $site->id])}}">{{$site->name}}</a>--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li style="line-height: 60px;">
                <a class="waves-effect waves-light m-r-5" href="{{route('front.page')}}"><i class="ti-arrow-left m-r-5"></i>@lang('backend.back_to_website')</a>
            </li>
            <li style="line-height: 60px;">
                <a class="waves-effect waves-light m-r-5" href="{{route('admin.session.flush')}}"><i class="ti-reload m-r-5"></i>@lang('backend.flush_session')</a>
            </li>
            <li style="line-height: 60px;">
                <button type="button" class="btn waves-effect waves-light btn-warning btn-outline dropdown-toggle btn-rounded" data-toggle="dropdown">{{strtoupper(session('switched_language'))}} <span class="caret m-l-5"></span></button>
                <ul class="dropdown-menu">
                    @foreach($_languages as $language)
                        <li>
                            <a href="{{route('admin.language.switch', ['language' => $language->code])}}">{{strtoupper($language->code)}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</nav>