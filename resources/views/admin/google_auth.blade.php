<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('_title'){{config('_CMS._global.name')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/admin/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles(true) !!}
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
    <style>
        .page-signup-modal {
            position: relative;
            top: auto;
            right: auto;
            bottom: auto;
            left: auto;
            z-index: 1;
            display: block;
        }
        .page-signup-form-group { position: relative; }
        .page-signup-icon {
            position: absolute;
            line-height: 21px;
            width: 36px;
            border-color: rgba(0, 0, 0, .14);
            border-right-width: 1px;
            border-right-style: solid;
            left: 1px;
            top: 9px;
            text-align: center;
            font-size: 15px;
        }
        html[dir="rtl"] .page-signup-icon {
            border-right: 0;
            border-left-width: 1px;
            border-left-style: solid;
            left: auto;
            right: 1px;
        }
        html:not([dir="rtl"]) .page-signup-icon + .page-signup-form-control { padding-left: 50px; }
        html[dir="rtl"] .page-signup-icon + .page-signup-form-control { padding-right: 50px; }
        /* Margins */
        .page-signup-modal > .modal-dialog { margin: 30px 10px; }
        @media (min-width: 544px) {
            .page-signup-modal > .modal-dialog {
                width: 400px;
                margin: 60px auto;
            }
        }
    </style>
</head>

<body>
<section id="wrapper" class="login-register login-google-auth">
    <div class="login-box">
        <div class="white-box">
            <form class="form-horizontal form-material" method="POST" action="{{route('admin.login.google2fa.check')}}">
                {{csrf_field()}}
                <h3 class="box-title m-b-20">Google Two-Factor Authentication</h3>

                @if(!is_null($secret))
                    <p>Please scan the QR-Code with your
                        <br>Google Authentication App to confirm your identity.
                        <br><br>Enter the given code in the field below.
                    </p>
                    <p align="center"><img src="{{ $google2fa_url }}" alt=""></p>
                @else
                    <p>Enter your authentication code in the field below.</p>
                @endif

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input name="google_secret" class="form-control text-center" type="text" required placeholder="XXX XXX">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Confirm</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>
                            Don't have the authentication app ?
                            <br>
                            <br>
                            <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank" class="text-primary m-l-5"><b><i class="fa fa-apple"></i> iOS / Apple</b></a>
                            <br>
                            <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank" class="text-primary m-l-5"><b><i class="fa fa-android"></i> Android</b></a>
                        </p>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>
{!! \App\Helpers\_CMS\TemplateHelper::scripts(true) !!}

@if(session('error'))
    <script>
        $.toast({
            heading: 'Error',
            text: "{{session('error')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'error',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif

</body>
</html>