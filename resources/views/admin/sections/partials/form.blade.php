@extends('admin._master')

@section('_title')
    @lang('modules.title_partials') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.partials')}}">@lang('modules.title_partials')</a></li>
    <li class="active">@lang('modules.edit_partial')</li>
@stop

@section('_title_section')
    @lang('modules.edit_partial')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.partials')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.partial_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_label_module')</label>
                    <div class="col-md-12">
                        <select name="module_id" class="form-control">
                            <option value="0">-- @lang('modules.partial_label_module_select')</option>
                            @foreach($modules as $module)
                                <option value="{{$module->id}}" {{isset($partial->id) && $partial->module_id == $module->id ? 'selected="selected' : ''}}>{{$module->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($partial->id) ? $partial->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_description_label')</label>
                    <div class="col-md-12">
                        <textarea name="description" class="form-control" rows="4">{{isset($partial->id) ? $partial->description : old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_label_side')</label>
                    <div class="col-md-12">
                        <input type="text" name="side" class="form-control" value="{{isset($partial->id) ? $partial->side : old('side')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_label_path')</label>
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon">../front/partials/</div>
                            <input required type="text" name="partial_path" class="form-control" value="{{isset($partial->id) ? $partial->partial_path : old('partial_path')}}">
                            <div class="input-group-addon">.blade.php</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_details_partial" id="checkbox_is_details" type="checkbox" value="1" {{isset($partial->id) && $partial->is_details_partial == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_is_details">@lang('modules.partial_is_details')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.partial_label_body_class')</label>
                    <div class="col-md-12">
                        <input type="text" name="body_class" class="form-control" value="{{isset($partial->id) ? $partial->body_class : old('body_class')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop