@extends('admin._master')

@section('_title')
    @lang('modules.title_partials') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.partials')}}">@lang('modules.title_partials')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_partials')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.partials.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_partial')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <th>@lang('modules.partial_label_module')</th>
                        <th>@lang('modules.partial_label_name')</th>
                        <th>@lang('modules.partial_label_path')</th>
                        <th>@lang('modules.partial_label_side')</th>
                        <th>@lang('modules.partial_label_created')</th>
                        <th>@lang('backend.actions')</th>
                    </thead>
                    <tbody>
                        @foreach($partials as $partial)
                            <tr>
                                <td>{{$partial->module->name}}</td>
                                <td>{{$partial->name}}</td>
                                <td>../front/partials/{{$partial->partial_path}}.blade.php</td>
                                <td>{{$partial->side}}</td>
                                <td>{{date('d/m/Y', strtotime($partial->created_at))}}</td>
                                <td>
                                    <a href="{{route('admin.partials.edit', ['id' => $partial->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    <a href="{{route('admin.partials.delete', ['id' => $partial->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop