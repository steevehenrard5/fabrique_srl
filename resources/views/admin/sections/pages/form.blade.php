@extends('admin._master')

@section('_title')
    @lang('backend.title_pages') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.pages')}}">@lang('backend.title_pages')</a></li>
    <li class="active">@lang('backend.page_edit')</li>
@stop

@section('_title_section')
    @lang('backend.page_edit')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.pages')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <p class="m-b-0">@lang('backend.page_translate_editing', ['lang' => '<strong>' . strtoupper(session('switched_language')) . '</strong>'])</p>
            </div>
        </div>
    </div>

    @if(isset($page->id))
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('backend.page_contents')
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse">
                        <i class="ti-minus"></i>
                    </a>
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="false">
                <div class="panel-body">
                    @if($page->contents->count() != 0)
                        <a href="#" class="btn-rounded btn btn-success waves-effect waves-light _addNewContent m-b-15"><i class="fa fa-plus m-r-5"></i> Add a content</a>
                    @endif

                    <a href="#" data-remodal-target="_seeMedia" class="btn-rounded btn btn-warning waves-effect waves-light m-b-15"><i class="fa fa-picture-o m-r-5"></i> Medias</a>

                    @include('admin.sections.pages.remodal_medias')

                    <div class="well" id="chooseContent" style="display: none;">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="list-group {{$modules->count() == 0 ? 'm-b-0' : 'm-b-15'}}">
                                    @foreach($content_types as $type)
                                        <a data-id="{{$type->reference}}" href="#" class="craftable list-group-item _type_{{$type->reference}}">
                                            <h5 class="list-group-item-heading m-b-0">
                                                <i class="fa fa-{{$type->icon}} m-r-5"></i>{{$type->name}}
                                                <div class="pull-right"><i class="fa fa-plus"></i></div>
                                            </h5>
                                        </a>
                                    @endforeach
                                </div>
                                <div class="list-group m-b-0">
                                    @foreach($modules as $module)
                                        <a data-id="{{$module->reference}}" href="#" class="craftable list-group-item">
                                            <h5 class="list-group-item-heading m-b-0">
                                                <i class="ti-{{$module->icon}} m-r-5"></i>{{$module->name}}
                                                <div class="pull-right"><i class="fa fa-plus"></i></div>
                                            </h5>
                                        </a>
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-sm-9">
                                @include('admin.sections.pages.inputs')
                                @include('admin.sections.pages.modules')
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="template-header">Header</div>
                            @if($page->contents->count() == 0)
                                <div class="well m-b-0 text-center">
                                    @lang('backend.page_contents_none')
                                    <br>
                                    <a href="#" class="btn-rounded btn btn-success waves-effect waves-light m-t-5 _toggleFirstContent">
                                        <i class="fa fa-plus m-r-5"></i>Create your first content
                                    </a>
                                </div>
                            @else
                                @include('admin.sections.pages.contents')
                            @endif
                            <div class="template-footer">Footer</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('backend.page_attributes')
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse">
                        <i class="ti-minus"></i>
                    </a>
                </div>
            </div>

            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">@lang('backend.page_label_title')</label>
                                <div class="col-md-12">
                                    <input type="text" name="title" class="form-control" required value="{{isset($page->id) ? $page->attributes->title : old('title')}}">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">@lang('backend.page_label_url') <sup>auto</sup></label>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">{{env('APP_URL')}}/{{session('switched_language')}}/</div>
                                        <input type="text" name="url" class="form-control" value="{{isset($page->id) ? $page->attributes->url : old('url')}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-12">@lang('backend.page_label_body_class')</label>
                                <div class="col-md-12">
                                    <input type="text" name="body_class" class="form-control" value="{{isset($page->id) ? $page->body_class : old('body_class')}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    @if(isset($page->id) && $page->attributes->image != '')
                                        <img src="{{$page->attributes->getImage()}}" class="max-w-300">
                                        <div class="checkbox checkbox-danger m-b-5">
                                            <input name="delete_picture" id="checkbox_del_picture" type="checkbox" value="1">
                                            <label for="checkbox_del_picture">Delete current image</label>
                                        </div>
                                    @endif
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="col-md-12">@lang('backend.page_label_description')</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" name="description">{{isset($page->id) ? $page->attributes->description : old('description')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                </div>
            </div>
        </div>

        @if($can_edit_configuration)
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('backend.page_configuration')
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-{{isset($page->id) ? 'plus' : 'minus'}}"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-wrapper collapse {{isset($page->id) ? '' : 'in'}}" aria-expanded="{{isset($page->id) ? 'false' : 'true'}}">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.page_label_directory')</label>
                            <p class="col-md-12">@lang('backend.page_info_directory')</p>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">../views/front/</div>
                                    <input type="text" name="directory" class="form-control" value="{{isset($page->id) ? $page->directory : old('directory')}}">
                                    <div class="input-group-addon">.blade.php</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12">@lang('backend.page_label_active')</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="is_active">
                                    <option value="0" {{isset($page->id) && $page->is_active == 0 ? 'selected="selected"' : ''}}>@lang('backend.page_label_isInactive')</option>
                                    <option value="1" {{isset($page->id) && $page->is_active == 1 ? 'selected="selected"' : ''}}>@lang('backend.page_label_isActive')</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="is_home" id="checkbox_homepage" type="checkbox" value="1" {{isset($page->id) && $page->is_home == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_homepage">@lang('backend.page_label_homepage')</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="is_secured" id="checkbox_secured" type="checkbox" value="1" {{isset($page->id) && $page->is_secured == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_secured">Is secured</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="is_guest" id="checkbox_guest" type="checkbox" value="1" {{isset($page->id) && $page->is_guest == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_guest">Is guest</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-b-0">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-danger">
                                    <input name="is_deleted" id="checkbox_deleted" type="checkbox" value="1" {{isset($page->id) && $page->is_deleted == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_deleted">@lang('backend.page_label_deleted')</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('backend.page_seo_configuration')
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-wrapper collapse" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.page_label_meta_title')</label>
                            <div class="col-md-12">
                                <input type="text" name="meta_title" class="form-control" value="{{isset($page->id) ? $page->attributes->meta_title : old('meta_title')}}">
                            </div>
                        </div>

                        <div class="form-group m-b-0">
                            <label class="col-md-12">@lang('backend.page_label_meta_description')</label>
                            <div class="col-md-12">
                                <input type="text" name="meta_description" class="form-control" value="{{isset($page->id) ? $page->attributes->meta_description : old('meta_description')}}">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>
                </div>
            </div>

            @if($has_security)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('backend.page_security')
                        <div class="panel-action">
                            <a href="#" data-perform="panel-collapse">
                                <i class="ti-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse" aria-expanded="true">
                        <div class="panel-body">
                            <p>This configuration applies only for secured page.</p>
                            <div class="form-group m-b-0">
                                <label class="col-md-12">@lang('backend.page_level_access')</label>
                                <div class="col-md-12">
                                    <select name="level_access" class="form-control">
                                        <option value="0">-- Everyone</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @if($menus && $menus->count() > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('backend.page_menus')
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-wrapper collapse" aria-expanded="true">
                    <div class="panel-body">
                        @foreach($menus as $key => $menu)
                            <div class="form-group col-sm-12 {{$loop->last ? 'm-b-0' : ''}}">
                                <div class="checkbox checkbox-primary">
                                    <input name="menus[]" id="checkbox_menu_{{$key}}" type="checkbox" value="{{$menu->id}}" {{isset($page->id) && in_array($menu->id, $linked_menus) ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_menu_{{$key}}">{{$menu->name}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>
                </div>
            </div>
        @endif
    </form>

    @if($page->id && $languages->count() > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('backend.page_translate')
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse">
                        <i class="ti-plus"></i>
                    </a>
                </div>
            </div>

            <div class="panel-wrapper collapse" aria-expanded="true">
                <div class="panel-body">
                    <p class="m-b-15">@lang('backend.page_translate_info', ['lang' => strtoupper(session('switched_language'))])</p>
                    @foreach($languages as $language)
                        <div class="col-md-3 col-xs-12 col-sm-6">
                            <div class="well m-b-0">
                                <h5>
                                    {{strtoupper($language->code)}} ({{$language->name}})
                                    @if($translated = $page->isTranslated($language->code))
                                        <a href="{{route('admin.pages.translated.delete', $translated->id)}}" class="askDelete btn-rounded pull-right btn btn-outline btn-danger m-l-5">@lang('backend.remove')</a>
                                        <a href="{{route('admin.pages.translated.edit', [$translated->id])}}" class="btn-rounded pull-right btn btn-outline btn-warning">@lang('backend.edit')</a>
                                    @else
                                        <a href="{{route('admin.pages.attributes.duplicate', [$page->id, $language->code])}}" class="btn-rounded pull-right btn btn-outline btn-success">@lang('backend.duplicate')</a>
                                    @endif
                                </h5>
                            </div>
                        </div>
                    @endforeach
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('_scripts')
    <script>
        $('.craftable').click(function(e) {
            e.preventDefault();
            var $id = $(this).attr('data-id');

            $('.craft-input').hide();
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $('.craftable').removeClass('active');
                $(this).addClass('active');
                $('#' + $id).fadeToggle();
            }
        });
        $('.toggleForm').click(function(e) {
            e.preventDefault();
            var $id = $(this).attr('data-id');
            $('.edit-form-' + $id).fadeToggle(90);
        });
        $('._toggleFirstContent').click(function(e) {
            e.preventDefault();
            if(!$('#chooseContent').is(':visible')) {
                $('#chooseContent').fadeToggle();
                $('._type_input-text').addClass('active');
                $('#input-text').fadeToggle();
            }
        });
        $('._addNewContent').click(function(e) {
            e.preventDefault();
            $('#chooseContent').fadeToggle();
        });
    </script>
    <script>
        $('.vertical-spin').TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $('.vertical-spin').TouchSpin({
            verticalbuttons: true
        });
        if(vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
    </script>
@stop
