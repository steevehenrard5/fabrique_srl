<table class="table template-content">
    <tr>
        <thead>
            <th>@lang('backend.order')</th>
            <th></th>
            <th>@lang('backend.type')</th>
            <th>@lang('backend.reference')</th>
            <th>@lang('backend.css_custom_classes')</th>
            <th></th>
            <th></th>
            <th></th>
        </thead>
    </tr>
    @foreach($page->contents as $content)
        <tr>
            <td width="25">
                {{$content->order}}.
            </td>
            <td width="100" style="text-align: center;">
                @if($content->order > 1)
                    <a href="{{route('admin.pages.content.up', $content->id)}}"><i class="fa fa-chevron-up"></i></a>
                @endif
                @if($content->order < $page->contents->count())
                    <a href="{{route('admin.pages.content.down', $content->id)}}"><i class="fa fa-chevron-down"></i></a>
                @endif
            </td>
            <td width="200">
                @if($content->type)
                    <i class="fa fa-{{$content->type->icon}} m-r-5"></i>{{$content->type->name}}
                @else
                    <i class="ti-{{$content->module->icon}} m-r-5"></i>{{$content->module->name}}
                @endif
            </td>
            <td>
                {{$content->reference}}
            </td>
            <td>
                @if($content->css_class)
                    <code>{{$content->css_class}}</code>
                @endif
            </td>
            <td>
                @if($content->draft)
                    <a href="#" data-id="{{$content->id}}" class="toggleForm link-u">@lang('backend.has_draft') <i class="fa fa-pencil-square-o m-l-5"></i></a>
                @endif
            </td>
            <td width="150">
                @if($content->is_draft)
                    <a href="{{route('admin.pages.content.publish', $content->id)}}" class="btn btn-block btn-success btn-rounded">@lang('backend.publish')</a>
                @else
                    <a href="{{route('admin.pages.content.un_publish', $content->id)}}" class="btn btn-block btn-primary btn-rounded">@lang('backend.un_publish')</a>
                @endif
            </td>
            <td width="80">
                <a href="#" data-id="{{$content->id}}" class="toggleForm btn btn-success btn-circle"><i class="fa fa-pencil"></i></a>
                <a href="{{route('admin.pages.content.remove', $content->id)}}" data-id="{{$content->id}}" class="askDelete btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        <tr class="well edit-form-{{$content->id}} {{$content->draft ? 'is-draft' : ''}}">
            <td colspan="8">
                @php
                    if($content->type) {
                        $action = route('admin.pages.' . ($content->draft ? 'draft' : 'content') . '.update');
                    } else {
                        $action = route('admin.pages.module.update');
                    }
                @endphp
                <form action="{{$action}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="content_id" value="{{$content->id}}">
                    @if($content->type_id) <input type="hidden" name="type_id" value="{{$content->type_id}}"> @endif
                    @if($content->draft)
                        <input type="hidden" name="draft_id" value="{{$content->draft->id}}">
                    @endif

                    <div class="form-group">
                        <div class="col-md-6">
                            <label>@lang('backend.input_type_reference') <sup>optional</sup></label>
                            <input type="text" name="reference" class="form-control" value="{{$content->reference}}">
                        </div>
                        <div class="col-md-6">
                            <label>@lang('backend.input_type_css_class') <sup>optional</sup></label>
                            <input type="text" name="css_class" class="form-control" value="{{$content->css_class}}">
                        </div>
                    </div>

                    @if($content->module && $content->module->is_block)
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>@lang('backend.input_head_title')</label>
                                <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')" value="{{$content->head_title}}">
                            </div>
                            <div class="col-md-6">
                                <label>@lang('backend.input_title')</label>
                                <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')" value="{{$content->title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>@lang('backend.input_partial')</label>
                                <select name="partial_id" class="form-control" required>
                                    <option value="0">-- @lang('backend.input_partial_select')</option>
                                    @foreach($content->module->partials as $partial)
                                        <option value="{{$partial->id}}" {{$content->partial_id == $partial->id ? 'selected="selected"' : ''}}>{{$partial->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    @if(!$content->type)
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.select_module_content')</label>
                            <div class="col-md-12">
                                @php $target = $content->module->targetable_by; @endphp
                                <select name="module_item_id" class="form-control">
                                    <option value="0">-- @lang('backend.select_module_label')</option>
                                    @foreach($content->items as $item)
                                        <option value="{{$item->id}}" {{$content->module_item_id == $item->id ? 'selected="selected"' : ''}}>
                                            {{$item->$target}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    @if($content->module && $content->module->is_multiple)
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="module_all_items" id="checkbox_allc_items_{{$content->id}}" type="checkbox" value="1" {{$content->module_all_items ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_allc_items_{{$content->id}}">@lang('backend.module_content_block_all_items')</label>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group {{ !$content->type && !$content->module->is_multiple ? 'm-b-0' : '' }}">
                        @if($content->type)
                            <label class="col-md-12">@lang('backend.input_' . $content->type->reference)</label>
                        @elseif($content->module->is_multiple)
                            <label class="col-md-12">@lang('backend.module_content_block')</label>
                        @endif
                        @if($content->type)
                            <div class="col-md-12">
                                @switch($content->type->input_type)
                                    @case('text')
                                    <input type="text" required name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}" value="{{ $content->draft ? $content->draft->content : $content->content }}">
                                    @breakswitch
                                    @case('file')
                                    <img src="{{asset('uploads/img/pages/' . ($content->draft ? $content->draft->content : $content->content))}}" class="m-b-15">
                                    <input type="file" name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}">
                                    @breakswitch
                                    @case('textarea')
                                    <textarea name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}">{!! $content->draft ? $content->draft->content : $content->content !!}</textarea>
                                    @breakswitch
                                @endswitch
                            </div>
                        @elseif($content->module->is_multiple)
                            <div class="col-md-3">
                                @lang('backend.module_content_block_number_items')
                                <input type="number" name="module_number_items" min="1" class="form-control" value="{{$content->module_number_items ? $content->module_number_items : 1}}">
                            </div>
                            <div class="col-md-9">
                                @lang('backend.module_content_block_matches')
                                <select name="module_order_by" class="form-control">
                                    <option value="0">-- @lang('backend.module_content_block_matches_select')</option>
                                    @php $orders = explode(',', $content->module->orderable_by); @endphp
                                    @foreach($orders as $order)
                                        <option value="{{$order}}|ASC" {{$content->module_order_by == ($order . '|ASC') ? 'selected="selected"' : ''}}>@lang('modules.order_' . $order . '_asc')</option>
                                        <option value="{{$order}}|DESC" {{$content->module_order_by == ($order . '|DESC') ? 'selected="selected"' : ''}}>@lang('modules.order_' . $order . '_desc')</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </div>
                    @if($content->type && ($content->type->reference == 'input-link' || $content->type->input_name == 'call-to-action'))
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.input_select_page_label')</label>
                            <select name="content_page_id" class="form-control">
                                <option value="0">-- @lang('backend.input_select_page')</option>
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}" {{$content->content_page_id == $page->id ? 'selected="selected"' : ''}}>{{$page->attributes->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.input_custom_action_label')</label>
                            <input type="text" name="content_custom_url" class="form-control" placeholder="@lang('backend.input_custom_action_label')" value="{{$content->content_custom_url}}">
                        </div>
                    @endif
                    @if(!$content->draft)
                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.update')</button>
                        @if(!$content->is_draft && $content->type_id) <button type="submit" name="is_draft" value="1" class="btn-rounded btn btn-primary waves-effect waves-light m-r-10">@lang('backend.draft_new')</button> @endif
                    @else
                        <button type="submit" name="is_draft" class="btn-rounded btn btn-primary waves-effect waves-light m-r-10">@lang('backend.update_draft')</button>
                        <button type="submit" name="is_deleted" value="1" class="btn-rounded btn btn-danger waves-effect waves-light m-r-10">@lang('backend.delete_draft')</button>
                        <button type="submit" name="is_published" value="1" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.publish')</button>
                    @endif
                </form>
            </td>
        </tr>
    @endforeach
</table>
