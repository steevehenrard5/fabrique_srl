@extends('admin._master')

@section('_title')
    @lang('backend.title_pages') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.pages')}}">@lang('backend.title_pages')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_pages')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.pages.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_page')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="POST" class="form form-horizontal">
                        {{csrf_field()}}
                        <div class="col-md-8">
                            <input type="text" name="title" value="{{session('search_pages.title') ?: old('title')}}" placeholder="Search by page title" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block btn-rounded">Search</button>
                        </div>
                        <div class="col-md-2">
                            <a href="{{route('admin.pages.search.clear')}}" class="btn btn-default btn-block btn-rounded">Clear</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#section-active" aria-controls="section-active" role="tab" data-toggle="tab"><h6><i class="fa fa-pencil m-r-5"></i> AVAILABLE PAGES ({{$pages->where('is_deleted', 0)->count()}})</h6></a></li>
                    @if($pages->where('is_deleted', 1)->count() > 0)
                        <li role="presentation"><a href="#section-archived" aria-controls="section-archived" role="tab" data-toggle="tab"><h6><i class="fa fa-trash m-r-5"></i> ARCHIVED/DELETED PAGES ({{$pages->where('is_deleted', 1)->count()}})</h6></a></li>
                    @endif
                </ul>
                <div class="tab-content" style="margin-top: 15px;">
                    <div class="tab-pane active" id="section-active" role="tabpanel">
                        <div class="row">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:75px" class="text-center">&nbsp;</th>
                                        <th>Ref.</th>
                                        <th>Page</th>
                                        <th>Slug</th>
                                        <th>Author</th>
                                        <th>Date</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($pages->where('is_deleted', 0) as $page)
                                        @include('admin.sections.pages.list_element')
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    @if($pages->where('is_deleted', 1)->count() > 0)
                        <div class="tab-pane" id="section-archived" role="tabpanel">
                            <div class="row">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:75px" class="text-center">&nbsp;</th>
                                            <th>Ref.</th>
                                            <th>Page</th>
                                            <th>Slug</th>
                                            <th>Author</th>
                                            <th>Date</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pages->where('is_deleted', 1) as $page)
                                            @include('admin.sections.pages.list_element')
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
