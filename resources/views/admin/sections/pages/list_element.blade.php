@if($page->attributes)
    <tr>
        <td class="text-center">
            @if(!$page->is_deleted)
            <a href="{{route('admin.pages.active.switch', ['id' => $page->id])}}" type="button" class="btn btn-{{$page->is_active ? 'warning' : 'default'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i></a>
            @endif
        </td>
        <td>{{$page->id}}</td>
        <td class="{{$page->is_deleted ? 'is-deleted' : ''}}">
            @if($page->is_home)
                <i class="ti-home m-r-5"></i>
            @endif
            @if($page->is_deleted)
                <i class="fa fa-trash m-r-5"></i>
            @endif
            {{$page->attributes->title}}
        </td>
        <td><span class="m-r-10 text-muted"><i class="ti-link m-r-5"></i>{{$page->attributes->url}}</span></td>
        <td><strong><i class="ti-user"></i></strong> {{$page->attributes->creator->getName()}}</td>
        <td><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($page->attributes->updated_at))}}</td>
        <td class="text-right">
            @if($page->is_deleted)
                <a href="{{route('admin.pages.restore', ['id' => $page->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light">@lang('backend.restore')</a>
                <a href="{{route('admin.pages.destroy', ['id' => $page->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.destroy')</a>
            @else
                <a href="{{route('admin.pages.edit', ['id' => $page->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                <a href="{{route('admin.pages.delete', ['id' => $page->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
            @endif
        </td>
    </tr>
@else
    <tr>
        <td>&nbsp;</td>
        <td>{{$page->id}}</td>
        <td colspan="5">
            <span class="text-muted">@lang('backend.not_translated')</span>
        </td>
    </tr>
@endif