@foreach($modules as $module)
    <section class="well m-b-0 craft-input" id="{{$module->reference}}">
        <form method="POST" action="{{route('admin.pages.module.add')}}" class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="page_id" value="{{$page->id}}">
            <input type="hidden" name="module_id" value="{{$module->id}}">

            <div class="form-group">
                <div class="col-md-6">
                    <label>@lang('backend.input_type_reference') <sup>optional</sup></label>
                    <input type="text" name="reference" class="form-control">
                </div>
                <div class="col-md-6">
                    <label>@lang('backend.input_type_css_class') <sup>optional</sup></label>
                    <input type="text" name="css_class" class="form-control">
                </div>
            </div>

            @if($module->is_block)
                <div class="form-group">
                    <div class="col-md-6">
                        <label>@lang('backend.input_head_title') <sup>optional</sup></label>
                        <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')">
                    </div>
                    <div class="col-md-6">
                        <label>@lang('backend.input_title') <sup>optional</sup></label>
                        <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>@lang('backend.input_partial') *</label>
                        <select name="partial_id" class="form-control" required>
                            <option value="0">-- @lang('backend.input_partial_select')</option>
                            @foreach($module->partials as $partial)
                                <option value="{{$partial->id}}">{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif

            <ul class="nav customtab2 nav-tabs form-group" role="tablist">
                <li role="presentation" class="active"><a href="#single_item_{{$module->id}}" aria-controls="single_item_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="true">@lang('backend.select_module_content')</a></li>
                @if($module->is_multiple)<li role="presentation" class=""><a href="#multiple_items_{{$module->id}}" aria-controls="multiple_items_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="false">@lang('backend.module_content_block')</a></li>@endif
                @if($module->is_volatile)<li role="presentation" class=""><a href="#new_item_{{$module->id}}" aria-controls="new_item_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="false">@lang('backend.module_create_content')</a></li>@endif
            </ul>

            <div class="tab-content m-t-0">
                <div role="tabpanel" class="tab-pane fade active in" id="single_item_{{$module->id}}">
                    <div class="form-group">
                        <div class="col-md-12">
                            @php
                                $target = $module->targetable_by;
                            @endphp
                            <select name="module_item_id" class="form-control">
                                <option value="0">-- @lang('backend.select_module_label')</option>
                                @foreach($module->items as $item)
                                    <option value="{{$item->id}}">{{$item->$target}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @if($module->is_multiple)
                    <div role="tabpanel" class="tab-pane fade" id="multiple_items_{{$module->id}}">
                        <div class="form-group">
                            <div class="col-md-12 m-b-15">
                                @lang('backend.module_content_block_matches')
                                <select name="module_order_by" class="form-control">
                                    <option value="0">-- @lang('backend.module_content_block_matches_select')</option>
                                    @foreach($module->orders as $order)
                                        <option value="{{$order}}|ASC">@lang('modules.order_' . $order . '_asc')</option>
                                        <option value="{{$order}}|DESC">@lang('modules.order_' . $order . '_desc')</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                @lang('backend.module_content_block_number_items')
                                <input type="number" name="module_number_items" min="1" class="form-control" value="1">
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox checkbox-primary p-t-30">
                                    <input name="module_all_items" id="checkbox_all_items_{{$module->id}}" type="checkbox" value="1">
                                    <label for="checkbox_all_items_{{$module->id}}">@lang('backend.module_content_block_all_items')</label>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @endif
                @if($module->is_volatile)
                    <div role="tabpanel" class="tab-pane fade" id="new_item_{{$module->id}}">
                        @include($module->view_name, $module)
                    </div>
                @endif
            </div>
            <button type="submit" class="btn-rounded btn btn-primary waves-effect waves-light m-r-10">@lang('backend.save_as_draft')</button>
            <button type="submit" name="is_published" value="1" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.publish')</button>
        </form>
    </section>
@endforeach
