@extends('admin._master')

@section('_title')
    @lang('backend.title_operators') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.operators')}}">@lang('backend.title_operators')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_operators')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.operators.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_operator')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <th>@lang('backend.user_label_username')</th>
                        <th>@lang('backend.user_label_first_name')</th>
                        <th>@lang('backend.user_label_last_name')</th>
                        <th>@lang('backend.user_label_email')</th>
                        <th>@lang('backend.user_label_phone')</th>
                        <th>@lang('backend.user_label_is_support')</th>
                        <th>Access level</th>
                        <th>@lang('backend.created_at')</th>
                        <th style="text-align: right;">@lang('backend.actions')</th>
                    </thead>
                    <tbody>
                        @foreach($operators as $user)
                            <tr>
                                <td><i class="ti-user m-r-5"></i><a class="link-u" href="{{route('admin.operators.edit', ['id' => $user->id])}}">{{$user->username}}</a></td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>
                                    @if($user->is_support)
                                        <i class="ti-check"></i>
                                    @endif
                                </td>
                                <td>{{$user->level == 100 ? 'Superadmin' : 'Admin'}}</td>
                                <td>{{date('d/m/Y', strtotime($user->created_at))}}</td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.operators.edit', ['id' => $user->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    @if(guard_admin()->level >= 100)
                                        <a href="{{route('admin.operators.delete', ['id' => $user->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
