@extends('admin._master')

@section('_title')
    @lang('backend.title_operators') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.operators')}}">@lang('backend.title_operators')</a></li>
    <li class="active">@lang('backend.operator_edit')</li>
@stop

@section('_title_section')
    @lang('backend.operator_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.operators')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.operator_attributes')</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_username')</label>
                            <div class="col-md-12">
                                <input required type="text" name="username" class="form-control" value="{{isset($user->id) ? $user->username : old('username')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_first_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="first_name" class="form-control" value="{{isset($user->id) ? $user->first_name : old('first_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_last_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="last_name" class="form-control" value="{{isset($user->id) ? $user->last_name : old('last_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_email')</label>
                            <div class="col-md-12">
                                <input required type="email" name="email" class="form-control" value="{{isset($user->id) ? $user->email : old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_phone')</label>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control" value="{{isset($user->id) ? $user->phone : old('phone')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Access level</label>
                            <div class="col-md-12">
                                <select name="level" class="form-control">
                                    <option value="99" {{$user->level == 99 ? 'selected="selected"' : ''}}>Admin standard</option>
                                    <option value="100" {{$user->level == 100 ? 'selected="selected"' : ''}}>Superadmin</option>
                                </select>
                                <br><small><sup>*</sup> Superadmin = Can manage partials, modules and administrators</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="is_support" id="checkbox_support" type="checkbox" value="1" {{isset($user->id) && $user->is_support == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_support">@lang('backend.user_label_is_support')</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password')</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password_confirm')</label>
                            <div class="col-md-12">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        @if(isset($user->id))
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Permissions</h3>

                    <div class="form-group">
                        <label class="col-md-12">Grant access to</label>
                        <div class="col-md-12">
                            <select name="permissions[]" class="form-control _multiple" multiple>
                                <optgroup label="CMS">
                                    <option value="medias" {{$user->canAccess('medias', true) ? 'selected="selected"' : ''}}>Medias</option>
                                    <option value="menus" {{$user->canAccess('menus', true) ? 'selected="selected"' : ''}}>Menus</option>
                                    <option value="pages" {{$user->canAccess('pages', true) ? 'selected="selected"' : ''}}>Pages</option>
                                </optgroup>
                                <optgroup label="Modules">
                                    @foreach($modules as $module)
                                        <option value="{{$module->reference}}" {{$user->canAccess($module->reference, true) ? 'selected="selected"' : ''}}>{{$module->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>

                            <br><small><sup>*</sup> If none are selected, permissions are granted on <u>every modules</u></small>
                        </div>
                    </div>

                    <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                </div>
            </div>
        @endif
    </form>
@stop

@section('_scripts')
    <script>
        $('._multiple').multiSelect();
    </script>
@stop
