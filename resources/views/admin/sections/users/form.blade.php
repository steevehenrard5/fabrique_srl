@extends('admin._master')

@section('_title')
    @lang('backend.title_users') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.users')}}">@lang('backend.title_users')</a></li>
    <li class="active">@lang('backend.user_edit')</li>
@stop

@section('_title_section')
    @lang('backend.user_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.users')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.user_attributes')</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_username')</label>
                            <div class="col-md-12">
                                <input required type="text" name="username" class="form-control" value="{{isset($user->id) ? $user->username : old('username')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_first_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="first_name" class="form-control" value="{{isset($user->id) ? $user->first_name : old('first_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_last_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="last_name" class="form-control" value="{{isset($user->id) ? $user->last_name : old('last_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_email')</label>
                            <div class="col-md-12">
                                <input required type="email" name="email" class="form-control" value="{{isset($user->id) ? $user->email : old('email')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password')</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password_confirm')</label>
                            <div class="col-md-12">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop
