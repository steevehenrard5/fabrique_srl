@extends('admin._master')

@section('_title')
    @lang('backend.title_configuration') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.configuration')}}">@lang('backend.title_configuration')</a></li>
    <li class="active">@lang('backend.configuration_edit')</li>
@stop

@section('_title_section')
    @lang('backend.configuration_edit')
@stop

@section('_content')
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.configuration_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($site->id) ? $site->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_footer')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="footer_copyright">{{isset($site->id) ? $site->configuration->footer_copyright : old('footer_copyright')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_email')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="email_contact">{{isset($site->id) ? $site->configuration->email_contact : old('email_contact')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_phone')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="phone_contact">{{isset($site->id) ? $site->configuration->phone_contact : old('phone_contact')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_address')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="address_contact">{{isset($site->id) ? $site->configuration->address_contact : old('address_contact')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_facebook')</label>
                    <div class="col-md-12">
                        <input type="text" name="facebook" class="form-control" value="{{isset($site->id) ? $site->configuration->facebook : old('facebook')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_twitter')</label>
                    <div class="col-md-12">
                        <input type="text" name="twitter" class="form-control" value="{{isset($site->id) ? $site->configuration->twitter : old('twitter')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_youtube')</label>
                    <div class="col-md-12">
                        <input type="text" name="youtube" class="form-control" value="{{isset($site->id) ? $site->configuration->youtube : old('youtube')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_linkedin')</label>
                    <div class="col-md-12">
                        <input type="text" name="linkedin" class="form-control" value="{{isset($site->id) ? $site->configuration->linkedin : old('linkedin')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_instagram')</label>
                    <div class="col-md-12">
                        <input type="text" name="instagram" class="form-control" value="{{isset($site->id) ? $site->configuration->instagram : old('instagram')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">@lang('backend.configuration_logos')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_default')</label>
                    @if($site->configuration->logo_website_default != '')
                        <img class="max-w-300" src="{{asset('assets/front/img/' . $site->configuration->logo_website_default)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_default')}}" class="askDelete btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_default">
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_header')</label>
                    @if($site->configuration->logo_website_header != '')
                        <img class="max-w-300" src="{{asset('assets/front/img/' . $site->configuration->logo_website_header)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_header')}}" class="askDelete btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_header">
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_footer')</label>
                    @if($site->configuration->logo_website_footer != '')
                        <img class="max-w-300" src="{{asset('assets/front/img/' . $site->configuration->logo_website_footer)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_footer')}}" class="askDelete btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_footer">
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title">PHP server configuration</h3>
            <div style="max-height: 300px; overflow: auto;" class="php-info">
                @php
                    ob_start();
                    phpinfo();
                    $pinfo = ob_get_contents();
                    ob_end_clean();
                    $pinfo = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$pinfo);
                    echo $pinfo;
                @endphp
            </div>
        </div>
    </div>
@stop

@section('_styles')
    <style>
        div.mce-edit-area {
            padding-top: 15px;
        }
    </style>
@stop

@section('_scripts')
    <script>
        var editor = tinymce.init({
            selector: '.editor-footer',
            height: 100,
            menubar: false,
            forced_root_block : '',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link code',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });
    </script>
@stop