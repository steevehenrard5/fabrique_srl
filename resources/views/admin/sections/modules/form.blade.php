@extends('admin._master')

@section('_title')
    @lang('backend.title_modules') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.modules')}}">@lang('backend.title_modules')</a></li>
    <li class="active">@lang('backend.module_edit')</li>
@stop

@section('_title_section')
    @lang('backend.module_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.module_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_reference')</label>
                    <div class="col-md-12">
                        <input required type="text" name="reference" class="form-control" value="{{isset($module->id) ? $module->reference : old('reference')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($module->id) ? $module->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_class')</label>
                    <div class="col-md-12">
                        <input type="text" name="class_name" class="form-control" value="{{isset($module->id) ? $module->class_name : old('class_name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_icon')</label>
                    <div class="col-md-12">
                        <input type="text" name="icon" class="form-control" value="{{isset($module->id) ? $module->icon : old('icon')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_targetable')</label>
                    <div class="col-md-12">
                        <input type="text" name="targetable_by" class="form-control" value="{{isset($module->id) ? $module->targetable_by : old('targetable_by')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_block" id="checkbox_block" type="checkbox" value="1" {{isset($module->id) && $module->is_block == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_block">@lang('backend.module_label_is_block')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_targetable_by_slug" id="checkbox_slugable" type="checkbox" value="1" {{isset($module->id) && $module->is_targetable_by_slug == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_slugable">@lang('backend.module_label_slugable')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_linkable" id="checkbox_linkable" type="checkbox" value="1" {{isset($module->id) && $module->is_linkable == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_linkable">@lang('backend.module_label_linkable')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_multiple" id="checkbox_multiple" type="checkbox" value="1" {{isset($module->id) && $module->is_multiple == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_multiple">@lang('backend.module_label_multiple')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.module_label_orderable')</label>
                    <p class="col-md-12">
                        @lang('backend.module_label_orderable_info')
                    </p>
                    <div class="col-md-12">
                        <input type="text" name="orderable_by" class="form-control" value="{{isset($module->id) ? $module->orderable_by : old('orderable_by')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop