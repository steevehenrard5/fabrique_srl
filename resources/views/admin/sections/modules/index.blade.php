@extends('admin._master')

@section('_title')
    @lang('backend.title_modules') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.modules')}}">@lang('backend.title_modules')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_modules')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_module')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($modules as $module)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    <a href="{{route('admin.modules.active.switch', ['id' => $module->id])}}" type="button" class="switcher-active btn btn-{{$module->is_active ? 'warning' : 'default'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i> </a>
                    <div class="text-muted">
                        <span class="m-r-10">
                            <i class="ti-{{$module->icon}} m-r-5"></i>{{$module->reference}}
                        </span>
                    </div>
                    <h3 class="m-t-20 m-b-20">
                        {{$module->name}}
                    </h3>
                    <p>
                        <i class="ti-folder m-r-5"></i>  App\Models\_Modules\{{$module->class_name}}.php
                    </p>
                    <a href="{{route('admin.modules.edit', ['id' => $module->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                    <a href="{{route('admin.modules.delete', ['id' => $module->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                </div>
            </div>
        @endforeach
    </div>
@stop