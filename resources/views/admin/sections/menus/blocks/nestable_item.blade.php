<div class="form-group">
    <label for="title">@lang('backend.menu_label_item_title')</label>
    <input type="text" name="title[{{$item['id']}}]" class="form-control itemPageLabel" required
           placeholder="@lang('backend.menu_label_item_title')" value="{{$item['label']}}">
</div>

<div class="form-group">
    <label for="title">Select a page</label>
    <select name="page_id[{{$item['id']}}]" class="form-control itemPageId">
        <option value="" @if(!isset($item['page-id'])) selected @endif>--</option>
        @foreach($pages as $page)
            <option value="{{$page->id}}"
                    @if(isset($item['page-id']) && $page->id == $item['page-id']) selected @endif>{{$page->attributes->title}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="custom_url">@lang('backend.menu_label_item_custom')</label>
    <input type="text" name="custom_url[{{$item['id']}}]" class="form-control itemPageCustomURL"
           placeholder="@lang('backend.menu_label_item_custom')" value="{{$item['custom_url'] ?? ''}}">
</div>

<div class="form-group">
    <label for="page_anchor">@lang('backend.menu_label_item_page_anchor')</label>
    <input type="text" name="page_anchor[{{$item['id']}}]" class="form-control itemPageAnchor"
           placeholder="@lang('backend.menu_label_item_page_anchor')" value="{{$item['page_anchor'] ?? ''}}">
</div>

<div class="form-group" style="margin-bottom: 0;">
    <div class="checkbox checkbox-primary">
        <input name="is_void[{{$item['id']}}]" class="itemPageIsVoid" type="checkbox"
               @if(isset($item['is_void']) && $item['is_void']) checked @endif>
        <label for="is_void">@lang('backend.menu_label_item_void')</label>
    </div>
</div>
