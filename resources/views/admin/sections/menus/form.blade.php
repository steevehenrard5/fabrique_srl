@extends('admin._master')

@section('_title')
    @lang('backend.title_menus') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.menus')}}">@lang('backend.title_menus')</a></li>
    <li class="active">@lang('backend.menu_edit')</li>
@stop

@section('_title_section')
    @lang('backend.menu_edit')
@stop

@section('_styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" rel="stylesheet">
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.menus')}}"
                       class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('backend.menu_attributes')
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse">
                        <i class="ti-{{isset($menu->id) ? 'plus' : 'minus'}}"></i>
                    </a>
                </div>
            </div>

            <div class="panel-wrapper collapse {{isset($menu->id) ?: 'in'}}" aria-expanded="{{isset($menu->id) ? 'true' : 'false'}}">
                <form method="POST" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="">
                                <div class="form-group">
                                    <label class="col-md-12">@lang('backend.menu_label_reference')</label>
                                    <div class="col-md-12">
                                        <input required type="text" name="reference" class="form-control"
                                               value="{{isset($menu->id) ? $menu->reference : old('reference')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">@lang('backend.menu_label_name')</label>
                                    <div class="col-md-12">
                                        <input required type="text" name="name" class="form-control"
                                               value="{{isset($menu->id) ? $menu->name : old('name')}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-12">
                                        <div class="checkbox checkbox-primary">
                                            <input name="is_everywhere" id="checkbox_everywhere" type="checkbox"
                                                   value="1" {{isset($menu->id) && $menu->is_everywhere == 1 ? 'checked="checked"' : ''}}>
                                            <label for="checkbox_everywhere">@lang('backend.module_label_everywhere')</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title">@lang('backend.menu_items')</h3>

            @if(!isset($menu->id))
                <p class="m-b-0">@lang('backend.menu_save_first')</p>
            @else
                <div class="col-sm-12">
                    <div class="col-sm-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Add existing page
                                <div class="panel-action">
                                    <a href="#" data-perform="panel-collapse">
                                        <i class="ti-minus"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body addExistingItemMenu">
                                    @if($pages->count() == 0)
                                        <div style="padding: 15px;">No page found.</div>
                                    @endif
                                    <ul id="existing-item" class="list-unstyled">
                                        @foreach($pages as $page)
                                            <li style="clear: both">
                                                <button class="btn btn-primary btn-sm addItem waves-effect waves-light"
                                                        data-page-id="{{$page->id}}"
                                                        data-label="{{$page->attributes->title}}">
                                                    <i class="ti ti-plus"></i>
                                                </button>
                                                <span class="targetItem">@if($page->is_home) <i class="ti ti-home m-r-5"></i>@endif {{$page->attributes->title}}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Create custom link
                                <div class="panel-action">
                                    <a href="#" data-perform="panel-collapse">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-wrapper collapse" aria-expanded="false">
                                <div class="panel-body" id="item-generator">
                                    <div class="form-group">
                                        <label for="title">@lang('backend.menu_label_item_title')</label>
                                        <input type="text" id="item_title" name="title" class="form-control" required
                                               placeholder="@lang('backend.menu_label_item_title')">
                                    </div>

                                    <div class="form-group">
                                        <label for="custom_url">@lang('backend.menu_label_item_custom')</label>
                                        <input type="text" id="custom_url" name="custom_url" class="form-control"
                                               placeholder="@lang('backend.menu_label_item_custom')">
                                    </div>

                                    <div class="form-group">
                                        <label for="page_anchor">@lang('backend.menu_label_item_page_anchor')</label>
                                        <input type="text" id="page_anchor" name="page_anchor" class="form-control"
                                               placeholder="@lang('backend.menu_label_item_page_anchor')">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox checkbox-primary">
                                            <input name="is_void" id="checkbox_void" type="checkbox" value="1">
                                            <label for="checkbox_void">@lang('backend.menu_label_item_void')</label>
                                        </div>
                                    </div>

                                    <button class="btn-rounded btn btn-primary waves-effect waves-light m-r-10 btn-block">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Menu
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <form action="{{route('admin.menus.items.update')}}" method="POST" id="menu-builder">
                                    {{csrf_field()}}
                                    <input type="hidden" name="menu_id" value="{{$menu->id}}">
                                    <div class="panel-body" style="padding: 15px;">
                                        <div class="dd" id="menu-canvas">
                                            <ol class="dd-list" id="menu-nestable">
                                                {!! $nestable !!}
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="submit"
                                                class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            @endif
        </div>
    </div>
@stop

@section('_scripts')
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
    <script>
        $('.btnAddSub').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var value = $('#select_' + id).val();
            if (value === '0') return false;

            $.ajax({
                url: '{{route('admin.menus.item.sub.add')}}',
                method: 'POST',
                data: {
                    item_id: id,
                    value: value
                },
                success: function (data) {
                    location.reload();
                },
                error: function () {
                }
            });
        });

        $('.toggleForm').click(function (e) {
            e.preventDefault();
            $('.formItem').fadeToggle(500);
        });

        $('#menu-canvas').nestable();

        var lastId = 0;

        function buildItem(item, block) {
            var html = "<li class='dd-item _itemId" + item.id + "' data-id='" + item.id + "'>";
            html += "<div class='dd-handle'>" + item.label + "";
            html += "<div class='pull-right'>";
            html += "<a class='_editButton dd-nodrag btn btn-success btn-circle waves-effect waves-light' href='#' data-edit-id='" + item.id + "'><i class='fa fa-pencil'></i></a>";
            html += "<a class='_deleteButton dd-nodrag btn btn-danger btn-circle waves-effect waves-light' href='#' data-edit-id='" + item.id + "'><i class='fa fa-trash'></i></a>";
            html += "</div></div>";
            html += "<div class='_editBox _" + item.id + "'>" + block + "</div>";
            html += "</li>";

            return html;
        }

        $('#item-generator button').on('click', function () {
            var label = $('#item_title').val();

            if (label.length > 2) {
                var newItem = {
                    'id': ++lastId,
                    'label': label,
                    'custom_url': $('#custom_url').val(),
                    'page_anchor': $('#page_anchor').val(),
                    'is_void': $('#checkbox_void').is(':checked'),
                };

                ajaxMenuItem(newItem);
            } else {
                alert('Please, provide item title');
            }
        });

        $('.addItem').on('click', function () {
            var newItem = {
                'id': ++lastId,
                'label': $(this).attr('data-label'),
                'page-id': $(this).attr('data-page-id'),
            };

            ajaxMenuItem(newItem);
        });

        function ajaxMenuItem(newItem) {
            $.ajax({
                method: 'POST',
                url: '{{route('admin.menus.item.build.ajax')}}',
                data: {
                    item: newItem,
                },
                success: function (html) {
                    var itemMenu = buildItem({
                        'id': newItem.id,
                        'label': newItem.label,
                    }, html);

                    $('#menu-canvas .dd-empty').remove();
                    $('#menu-nestable').append(itemMenu);
                }
            });
        }

        $('body').on('click', '._editButton', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-edit-id');
            var was_visible = $('._editBox._' + id).is(':visible');

            $('._editBox').hide();
            if(!was_visible)
                $('._editBox._' + id).show();
            else
                $('._editBox._' + id).hide();
        });

        $('body').on('click', '._deleteButton', function (e) {
            var id = $(this).attr('data-edit-id');

            $('._itemId' + id).remove();
            e.preventDefault();
            e.stopPropagation();
        });

        $('#menu-builder').on('submit', function (e) {
            e.preventDefault();
            var nestableData = $('#menu-canvas').nestable('serialize');

            $('#menu-builder').append("<input type='hidden' name='nestableData' value='" + JSON.stringify(nestableData) + "'>");
            $('#menu-builder').get(0).submit();
        });
    </script>
@stop
