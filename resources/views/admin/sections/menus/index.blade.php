@extends('admin._master')

@section('_title')
    @lang('backend.title_menus') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.menus')}}">@lang('backend.title_menus')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_menus')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.menus.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_menu')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($menus as $menu)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    <div class="text-muted">
                        <span class="m-r-10">
                            <i class="ti-tag m-r-5"></i>{{$menu->reference}}
                        </span>
                    </div>
                    <h3>
                        {{$menu->name}}
                    </h3>
                    <a href="{{route('admin.menus.edit', ['id' => $menu->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                    <a href="{{route('admin.menus.delete', ['id' => $menu->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                </div>
            </div>
        @endforeach
    </div>
@stop
