<div class="row grid">
    @if($medias->count() == 0)
        <div class="col-lg-12">
            <span>No media available.</span>
        </div>
    @endif
    @foreach($medias as $media)
        <div class="col-lg-3 grid-item">
            <div class="white-box _itemSH" style="position: relative;">
                <div class="switcher-active">
                    <a href="{{route('admin.medias.delete', $media->id)}}" class="btn btn-danger btn-circle waves-effect waves-light askDelete"><i class="fa fa-trash"></i> </a>
                    <button class="btn btn-default btn-circle waves-effect waves-light _copyClipboard" data-clipboard-text="/uploads/medias/{{$media->file_name}}"><i class="fa fa-copy"></i> </button>
                </div>

                <h3 class="box-title">
                    <a href="{{$media->getMedia()}}" download>
                        {{$media->file_name}}
                    </a>
                </h3>

                <p>Uploaded at {{$media->created_at}} <br>by {{$media->user ? $media->user->getName() : 'Unknown'}}</p>

                <p style="overflow: hidden;">
                    <code>/uploads/medias/{{$media->file_name}}</code>
                </p>

                <ul class="list-inline two-part _media_list">
                    <li>
                        @if($media->isImage())
                            <a href="{{$media->getMedia()}}" class="mytooltip">
                                <img src="{{$media->getMedia()}}" style="max-width: 80px;" class="br4">

                                <span class="tooltip-content5">
                                    <span class="tooltip-text3">
                                        <span class="tooltip-inner2">
                                            <img src="{{$media->getMedia()}}" style="max-width: 100%;">
                                        </span>
                                    </span>
                                </span>
                            </a>
                        @else
                            <i class="fa fa-{{$media->getIcon_()}} text-info"></i>
                        @endif
                    </li>
                    <li class="text-right"><div>{{$media->getFormattedSize_()}}</div></li>
                </ul>
            </div>
        </div>
    @endforeach
</div>

<div class="row text-center">
    {{$medias->render()}}
</div>
