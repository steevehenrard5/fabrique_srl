<div class="medias_table_container">
    <div class="table-responsive">
        <table class="table table_medias">
            <tbody>
                @foreach($medias as $media)
                    @include('admin.sections.medias.row')
                @endforeach
            </tbody>
        </table>
    </div>
</div>
