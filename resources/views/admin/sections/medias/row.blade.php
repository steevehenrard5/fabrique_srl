<tr>
    <td>
        @if($media->isImage())
            <img src="{{$media->getMedia()}}" style="max-width: 45px;">
        @else
            <i class="fa fa-{{$media->getIcon_()}} text-info" style="font-size: 45px;"></i>
        @endif
    </td>
    <td>
        <a href="{{$media->getMedia()}}" download>{{$media->file_name}}</a>
    </td>
    <td><code>/uploads/medias/{{$media->file_name}}</code></td>
    <td style="text-align: right;">
        <button class="btn btn-default btn-circle waves-effect waves-light _copyClipboard" data-clipboard-text="/uploads/medias/{{$media->file_name}}"><i class="fa fa-copy"></i> </button>
    </td>
</tr>
