<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form id="dropzoneForm" class="form-horizontal dropzone" action="{{route('admin.medias.upload')}}" method="POST">
                        {{csrf_field()}}
                        <div class="fallback">
                            <input name="files" type="file" multiple/>
                        </div>
                    </form>

                    <a href="{{href_none()}}" class="_processDzQueue btn btn-success btn-rounded" style="display: none; margin-top: 30px;">
                        Upload
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body" @if(isset($is_modal)) style="padding: 5px;" @endif>
                    @if(!isset($is_modal))
                        <h3>You are using <strong>{{\App\Helpers\_CMS\FileHelper::getFormattedSize($total_size)}}{{$total_size == 0 ? 'MB' : ''}}</strong></h3>
                        @include('admin.sections.medias.masonry')
                    @else
                        @include('admin.sections.medias.table')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@section('_scripts')
    @parent()

    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script>
        new ClipboardJS('._copyClipboard')
            .on('success', function(e) {
                swal({
                    title: "Copied !",
                    text: e.text,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                e.clearSelection();
            });
    </script>

    <script>
        Dropzone.autoDiscover = false;
        var $_element = $('#dropzoneForm');
        if($_element.length > 0) {
            var dz = new Dropzone('#dropzoneForm', {
                uploadMultiple: true,
                autoProcessQueue: false,
                maxFiles: null,
                parallelUploads: 15,
                maxFilesize: 8,
                dictDefaultMessage: "<strong>Drop your files here, or click to upload.</strong><br>(Max size allowed : {{ini_get('upload_max_filesize')}}, Max files : 15)"
            });
            dz.on('completemultiple', function(response) {
                @if(!isset($is_modal))
                    location.reload();
                @else
                    var xhr = response[0].xhr;
                    var resp = JSON.parse(xhr.response);
                    for(var i = 0; i < resp.length; i++) {
                        $('table.table_medias tbody').prepend(resp[i]);
                    }
                @endif
            });
            dz.on('addedfile', function() {
                $('._processDzQueue').fadeIn(500);
            });
            $('._processDzQueue').click(function (e) {
                e.preventDefault();
                dz.processQueue();
            });
        }
    </script>

    <script>
        $('.grid').masonry({
            itemSelector: '.grid-item',
        });
    </script>
@stop
