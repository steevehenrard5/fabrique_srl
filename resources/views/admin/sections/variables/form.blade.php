@extends('admin._master')

@section('_title')
    @lang('backend.title_variables') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.languages')}}">@lang('backend.title_languages')</a></li>
    <li class="active">@lang('backend.variable_edit')</li>
@stop

@section('_title_section')
    @lang('backend.variable_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.languages')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.variable_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.variable_label_reference')</label>
                    <div class="col-md-12">
                        <input @if(isset($variable->id)) disabled @else required @endif type="text" name="reference" class="form-control" value="{{isset($variable->id) ? $variable->reference : old('reference')}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.variable_label_language')</label>
                    <div class="col-md-12">
                        <select name="lang" class="form-control" @if(isset($variable->id)) disabled @else required @endif>
                            <option value="0">-- @lang('backend.variable_label_language_select')</option>
                            @foreach($languages as $language)
                                <option value="{{$language->code}}" {{isset($variable->id) && $variable->lang == $language->code ? 'selected="selected"' : ''}}>{{$language->name}} ({{$language->code}})</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.variable_label_value')</label>
                    <div class="col-md-12">
                        <input required type="text" name="value" class="form-control" value="{{isset($variable->id) ? $variable->value : old('value')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop
