@extends('admin._master')

@section('_title')
    @lang('backend.title_languages') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.languages')}}">@lang('backend.title_languages')</a></li>
    <li class="active">@lang('backend.language_edit')</li>
@stop

@section('_title_section')
    @lang('backend.language_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.languages')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.language_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.language_label_code')</label>
                    <div class="col-md-12">
                        <input required type="text" name="code" class="form-control" value="{{isset($language->id) ? $language->code : old('code')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.language_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($language->id) ? $language->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="default_backend" id="checkbox_db" type="checkbox" value="1" {{isset($language->id) && $language->default_backend == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_db">@lang('backend.language_label_default_backend')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="default_frontend" id="checkbox_df" type="checkbox" value="1" {{isset($language->id) && $language->default_frontend == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_df">@lang('backend.language_label_default_frontend')</label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop