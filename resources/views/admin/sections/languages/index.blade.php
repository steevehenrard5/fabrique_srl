@extends('admin._master')

@section('_title')
    @lang('backend.title_languages') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.languages')}}">@lang('backend.title_languages')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_languages')
@stop

@section('_content')
    @if($can_operate)
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="{{route('admin.languages.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light m-r-5">@lang('backend.new_language')</a>
                            <a href="{{route('admin.variables.create')}}" class="btn btn-default m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_variable')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        @foreach($languages as $language)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    <a href="{{route('admin.languages.active.switch', ['id' => $language->id])}}" type="button" class="switcher-active btn btn-{{$language->is_used ? 'warning' : 'default'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i> </a>
                    <div class="text-muted">
                        <span class="m-r-10">
                            <i class="ti-comment m-r-5"></i>{{$language->code}}
                        </span>
                    </div>
                    <h3 class="m-t-20 m-b-{{$can_operate ? 20 : 0}}">
                        {{$language->name}}
                    </h3>
                    @if($can_operate)
                        <a href="{{route('admin.languages.edit', ['id' => $language->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                        <a href="{{route('admin.languages.delete', ['id' => $language->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box m-b-10" style="max-height: 450px; overflow-y: auto;">
                <h3 class="box-title">@lang('backend.language_variables') available in <u>{{$languages->where('code', session('switched_language'))->first()->name}}</u></h3>
                <table class="table m-b-0">
                    <thead>
                        <th width="200">
                            @lang('backend.language_variable_reference')
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-search"></i></div>
                                <input type="text" id="searchRef" class="form-control">
                            </div>
                        </th>
                        <th>
                            @lang('backend.language_variable_value')
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-search"></i></div>
                                <input type="text" id="searchVal" class="form-control">
                            </div>
                        </th>
                        <th>Translated in</th>
                        <th width="150" style="text-align: right;">@lang('backend.actions')</th>
                    </thead>
                    <tbody>
                        @if($variables->count() == 0)
                            <tr>
                                <td colspan="4">@lang('backend.language_variable_none')</td>
                            </tr>
                        @endif
                        @foreach($variables as $variable)
                            <tr>
                                <td class="ref">{{$variable->reference}}</td>
                                <td class="val">{{substr($variable->value, 0, 350)}}{{strlen($variable->value) > 350 ? '...' : ''}}</td>
                                <td>
                                    @foreach($languages->where('is_used', 1) as $lang)
                                        <a class="btn btn-{{in_array($lang->code, $variable->siblings->pluck('lang')->toArray()) ? 'info' : 'default'}} btn-circle waves-effect waves-light" href="{{href_none()}}">
                                            {{$lang->code}}
                                        </a>
                                    @endforeach
                                </td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.variables.edit', $variable->id)}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    @if($can_operate)
                                        <a href="{{route('admin.variables.delete', $variable->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <div class="m-b-15">
                    <h3 class="box-title m-b-0">Sync variables</h3>
                    <small>Synchronize currently selected variables to targeted language.</small>
                </div>

                @foreach($languages->where('code', '!=', session('switched_language')) as $language)
                    <div class="col-md-3 col-xs-12 col-sm-6">
                        <div class="well m-b-0">
                            <h5>
                                {{strtoupper($language->code)}} ({{$language->name}})
                                <a href="{{route('admin.variables.sync', $language->code)}}" class="btn-rounded pull-right btn btn-outline btn-success">
                                    Synchronize
                                </a>
                            </h5>
                        </div>
                    </div>
                @endforeach
                <div class="clear"></div>
            </div>
        </div>
    </div>
@stop

@section('_scripts')
    <script>
        $('input#searchRef').quicksearch('table tbody tr', {
            selector: '.ref'
        });
        $('input#searchVal').quicksearch('table tbody tr', {
            selector: '.val'
        });
    </script>
@stop
