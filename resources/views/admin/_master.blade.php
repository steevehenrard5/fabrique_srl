<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('_title'){{config('_CMS._global.name')}}</title>
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/admin/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles(true) !!}
    <link href="{{asset('assets/admin/css/remodal.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/remodal-default-theme.css')}}" rel="stylesheet">
    @yield('_styles')
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey={{tinymce_key()}}"></script>
</head>

<body class="fix-sidebar">
<div id="wrapper">
    @include('admin.blocks.top_nav')
    @include('admin.blocks.sidebar')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">@yield('_title_section')</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        @yield('_breadcrumb')
                    </ol>
                </div>
            </div>
            @yield('_content')
        </div>
        @include('admin.blocks.footer')
    </div>
</div>

<!-- JS Global -->
{!! \App\Helpers\_CMS\TemplateHelper::scripts(true) !!}
<script type="text/javascript" src="{{asset('assets/admin/js/quicksearch.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/remodal.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.askDelete').click(function(e) {
        e.preventDefault();
        var defaultEvent = $(this).attr('href');
        swal({
            title: "@lang('backend.confirm_delete_title')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "@lang('backend.cancel_button')",
            confirmButtonText: "@lang('backend.confirm_delete_button')",
            closeOnConfirm: false
        }, function() {
            document.location.href = defaultEvent;
            return true;
        });
    });
    $('.datepicker-cms').datepicker();
</script>

<script>
    var editor = tinymce.init({
        selector: '.editor-cms',
        height: 350,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code',
        content_css: '//www.tinymce.com/css/codepen.min.css'
    });
    var editor_small = tinymce.init({
        selector: '.editor-cms-small',
        height: 100,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code',
        content_css: '//www.tinymce.com/css/codepen.min.css'
    });
</script>

@if(session('success'))
    <script>
        $.toast({
            heading: 'Success',
            text: "{{session('success')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif
@if(session('error'))
    <script>
        $.toast({
            heading: 'Error',
            text: "{{session('error')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'error',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif
@if(session('warning'))
    <script>
        $.toast({
            heading: 'Warning',
            text: "{{session('warning')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'warning',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif

@yield('_scripts')
</body>
</html>
